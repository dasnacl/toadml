(** * Evaluation of T *)
Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.
Require Import ToadML.Shared.Fresh ToadML.Shared.Sema ToadML.Util.Sets ToadML.Util.HasEquality.
Require Import ToadML.Util.Convenience ToadML.Util.NoDupInv ToadML.Util.Step.

Require Import ToadML.T.syntax.

Open Scope LangNotationsScope.

(** A runtime program is a state with a list of commands or a crashed state. *)
Inductive rtprog : Type :=
| RPTerm (Ψ : stack) (Δ : store) (cmds : list cmd)
| RPCrash
.
#[global]
Notation "'s(' Ψ ';' Δ '▷' c ')'" := ((RPTerm Ψ Δ c) : rtprog) (at level 81, Δ at next level, c at next level).
#[global]
Notation "'▷↯'" := (RPCrash).

(** Evaluation of binary expressions. Note that 0 means `true` in S, so `5 < 42` evals to `0`. *)
Definition eval_binop (b : binopsymb) (n0 n1 : nat) : option value :=
  Some((match b with
       | Bless => (if Nat.ltb n0 n1 then Vbool true else Vbool false)
       | Badd => Vnat(n0 + n1)
       | Bdiv => Vnat(Nat.div n0 n1)
       | Bsub => Vnat(n0 - n1)
       | Bmul => Vnat(n0 * n1)
       end))
.
Variant StepTag : Type :=
| scmd (* evaluation steps *)
.
(* Contextual steps just eat off the top of the list, i.e., the context is the rest of the list. *)
Inductive estep : (Step scmd rtprog event) :=
| e_negb : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (b : bool),
    s(Sval b :: Ψ ; Δ ▷ Cnegb :: cmds) --[]-->(scmd) s(Sval (negb b) :: Ψ ; Δ ▷ cmds)
| e_get : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (x : vart) (v : value),
    Some (x, v) = Δ ->
    s(Ψ ; Δ ▷ Cget x :: cmds) --[]-->(scmd) s(Sval v :: Ψ ; Δ ▷ cmds)
| e_if_true : forall (Ψ : stack) (Δ : store) (cmds thens : list cmd),
    s(Sval true :: Scode thens :: Ψ ; Δ ▷ Cif :: cmds) --[]-->(scmd) s(Ψ ; Δ ▷ thens ++ cmds)
| e_if_false : forall (Ψ : stack) (Δ : store) (cmds thens : list cmd),
    s(Sval false :: Scode thens :: Ψ ; Δ ▷ Cif :: cmds) --[]-->(scmd) s(Ψ ; Δ ▷ cmds)
| e_unquote : forall (Ψ : stack) (Δ : store) (cmds what : list cmd),
    s(Scode what :: Ψ ; Δ ▷ Cunquote :: cmds) --[]-->(scmd) s(Ψ ; Δ ▷ what ++ cmds)
| e_quote : forall (Ψ : stack) (Δ : store) (cmds what : list cmd),
    s(Ψ ; Δ ▷ Cquote what :: cmds) --[]-->(scmd) s(Scode what :: Ψ ; Δ ▷ cmds)
| e_hasnat_true : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (s : stack_el) (n : nat),
    s = Sval n ->
    s(s :: Ψ ; Δ ▷ Chas Tℕ :: cmds) --[]-->(scmd) s(Sval true :: Ψ ; Δ ▷ cmds)
| e_hasnat_false : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (s : stack_el),
    (forall (n : nat), s <> Sval n) ->
    s(s :: Ψ ; Δ ▷ Chas Tℕ :: cmds) --[]-->(scmd) s(Sval false :: Ψ ; Δ ▷ cmds)
| e_hasbool_true : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (s : stack_el) (b : bool),
    s = Sval b ->
    s(s :: Ψ ; Δ ▷ Chas T𝔹 :: cmds) --[]-->(scmd) s(Sval true :: Ψ ; Δ ▷ cmds)
| e_hasbool_false : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (s : stack_el),
    (forall (b : bool), s <> Sval b) ->
    s(s :: Ψ ; Δ ▷ Chas T𝔹 :: cmds) --[]-->(scmd) s(Sval false :: Ψ ; Δ ▷ cmds)
| e_abort : forall (Ψ : stack) (Δ : store) (cmds : list cmd),
    s(Ψ ; Δ ▷ Cabort :: cmds) --[ Scrash ]-->(scmd) ▷↯
| e_bop : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (n1 n2 : nat) (b : binopsymb) (v : value),
    Some v = eval_binop b n1 n2 ->
    s(Sval n2 :: Sval n1 :: Ψ ; Δ ▷ Cbop b :: cmds) --[]-->(scmd) s(Sval v :: Ψ ; Δ ▷ cmds)
| e_push : forall (Ψ : stack) (Δ : store) (cmds : list cmd) (v : value),
    s(Ψ ; Δ ▷ Cpush v :: cmds) --[]-->(scmd) s(Sval v :: Ψ ; Δ ▷ cmds)
. 
#[global]
Existing Instance estep.
#[global]
Hint Constructors estep : core.

(** A runtime expression is classified as value if the associated state is also freed completely. *)
Inductive rtprog_is_final : rtprog -> Prop :=
| CRTempty : forall (Ψ : stack) (Δ : store), rtprog_is_final (s(Ψ ; Δ ▷ nil))
| CRTfail : rtprog_is_final (▷↯) (* this will let star_step terminate with a crash *)
.
#[export]
Instance T__LangParams : @LangParams event eventeq__Instance StepTag := {
  State := rtprog ;
  step := estep ;
  is_value := rtprog_is_final ;
}.
(** Defines top-level program execution. *)
Inductive progstep : prog -> tracepref -> rtprog -> Prop :=
| prog_step_success : forall (x__foo y : vart) (cmds__pre cmds__post cmds__foo : list cmd) (foo : symbol) (v v__pre v__foo : value)
                           (Ψ : stack) (n__pre n__foo n__post : nat),
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    cmds__foo = body_of_symbol foo ->
    (* setup the argument for the function call *)
    s(nil ; None ▷ cmds__pre) -( n__pre )-[ nil ]-->(scmd) (s(Sval v__pre :: Ψ ; None ▷ nil)) ->
    (* do the function call *)
    s(Ψ ; Some (x__foo, v__pre) ▷ cmds__foo) -( n__foo )-[ nil ]-->(scmd) s(Sval v__foo :: Ψ ; Some (x__foo, v__pre) ▷ nil) ->
    (* something may happen after the function call *)
    s(Sval v__foo :: Ψ ; None ▷ cmds__post) -( n__post )-[ nil ]-->(scmd) s(Sval v :: nil ; None ▷ nil) ->
    progstep (Cprog (LContext y cmds__pre cmds__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Sret Qcomptoctx (sv v__foo) :: Send (sv v) :: nil)
             (RPTerm (Sval v :: nil) None nil)
| prog_step_failure0 : forall (y : vart) (cmds__pre cmds__post : list cmd) (foo : symbol) (n__pre : nat),
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    (* setup the argument for the function call ; fails *)
    s(nil ; None ▷ cmds__pre) -( n__pre )-[ Scrash :: nil ]-->(scmd) (▷↯) ->
    progstep (Cprog (LContext y cmds__pre cmds__post) foo)
             (Sstart :: Scrash :: nil)
             (RPCrash)
| prog_step_failure1 : forall (x__foo y : vart) (cmds__pre cmds__post cmds__foo : list cmd) (foo : symbol) (v__pre : value)
                            (Ψ__pre : stack) (n__pre n__foo : nat),
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    cmds__foo = body_of_symbol foo ->
    (* setup the argument for the function call *)
    s(nil ; None ▷ cmds__pre) -( n__pre )-[ nil ]-->(scmd) (s(Sval v__pre :: Ψ__pre ; None ▷ nil)) ->
    (* do the function call ; fails *)
    s(Ψ__pre ; Some(x__foo, v__pre) ▷ cmds__foo) -( n__foo )-[ Scrash :: nil ]-->(scmd) (▷↯) ->
    progstep (Cprog (LContext y cmds__pre cmds__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Scrash :: nil)
             (RPCrash)
| prog_step_failure2 : forall (x__foo y : vart) (cmds__pre cmds__post cmds__foo : list cmd) (foo : symbol) (v__pre v__foo : value)
                            (Ψ__pre : stack) (n__pre n__foo n__post : nat),
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    cmds__foo = body_of_symbol foo ->
    (* setup the argument for the function call *)
    s(nil ; None ▷ cmds__pre) -( n__pre )-[ nil ]-->(scmd) (s(Sval v__pre :: Ψ__pre ; None ▷ nil)) ->
    (* do the function call *)
    s(Ψ__pre ; Some(x__foo, v__pre) ▷ cmds__foo) -( n__foo )-[ nil ]-->(scmd) s(Sval v__foo :: Ψ__pre ; Some(x__foo, v__pre) ▷ nil) ->
    (* something may happen after the function call; fails *)
    s(Sval v__foo :: Ψ__pre ; None ▷ cmds__post) -( n__post )-[ Scrash :: nil ]-->(scmd) (▷↯) ->
    progstep (Cprog (LContext y cmds__pre cmds__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Sret Qcomptoctx (sv v__foo) :: Scrash :: nil)
             (RPCrash)
.

(** Some star_step related lemmas. *)
Lemma star_step_nf_empty_trace (Ψ : stack) (Δ Δ' : store) (cmds : list cmd) (As : tracepref) (v : value) :
  s(Ψ ; Δ ▷ cmds) ==[ As ]==>(scmd)* s(Sval v :: nil ; Δ' ▷ nil) ->
  As = nil
.
Proof.
  intros H; cbv in H; dependent induction H; eauto.
  inv H. inv H0; inv H.
  inv H; eauto.
Qed.
Lemma star_step_nf_some_trace (Ψ : stack) (Δ : store) (cmds : list cmd) (As : tracepref) :
  s(Ψ ; Δ ▷ cmds) ==[ As ]==>(scmd)* ▷↯ ->
  As = Scrash :: nil
.
Proof.
  intros H; cbv in H; dependent induction H; eauto.
  inv H. inv H0; try easy. inv H; eauto.
Qed.

(** For backward simulation, we need uniqueness of normalforms. *)
Lemma determ_step (Ψ : stack) (Δ : store) (cmds : list cmd) (a a' : option event) (r1 r2 : rtprog) :
  s(Ψ ; Δ ▷ cmds) --[, a ]-->(scmd) r1 ->
  s(Ψ ; Δ ▷ cmds) --[, a' ]-->(scmd) r2 ->
  a = a' /\ r1 = r2
.
Proof.
  intros H0 H1.
  inv H0; inv H1; firstorder eauto.
  inv H6; eauto. specialize (H6 n); congruence. 
  specialize (H4 n); congruence. 
  specialize (H6 b); congruence. 
  specialize (H4 b); congruence. 
  inv H4. inv H9. reflexivity.
Qed.
Lemma unique_nf (Ψ : stack) (Δ : store) (cmds : list cmd) (As As' : tracepref) (r1 r2 : rtprog) :
  rtprog_is_final r1 ->
  rtprog_is_final r2 ->
  s(Ψ ; Δ ▷ cmds) ==[ As ]==>(scmd)* r1 ->
  s(Ψ ; Δ ▷ cmds) ==[ As' ]==>(scmd)* r2 ->
  As = As' /\ r1 = r2
.
Proof.
  intros H0 H1 H2 H3. cbv in H2; dependent induction H2; eauto. 
  - inv H0. inv H3; firstorder eauto; inv H.
  - inv H. inv H2. inv H3. inv H1. inv H. inv H2. eauto. 
    inv H. inv H. inv H. inv H. inv H. 
  - dependent destruction H3.
    + inv H1. inv H.
    + eapply determ_step in H; eauto. now destruct H.
    + eapply determ_step in H; eauto. destruct H as [_ H]; subst.
      inv H3; eapply IHstar_step; eauto. 
Qed.
Lemma unique_nf_step_n (Ψ : stack) (Δ : store) (cmds : list cmd) (As1 As2 : tracepref) (r1 r2 : rtprog) (n n0 : nat) :
  rtprog_is_final r1 ->
  rtprog_is_final r2 ->
  s(Ψ ; Δ ▷ cmds) -(n)-[ As1 ]-->(scmd) r1 ->
  s(Ψ ; Δ ▷ cmds) -(n0)-[ As2 ]-->(scmd) r2 ->
  As1 = As2 /\ r1 = r2 /\ n = n0
.
Proof.
  revert Ψ Δ cmds As1 As2 r1 r2 n0; induction n; intros.
  - inv H1. inv H. inv H2. now inv H0. inv H. inv H. 
  - enough (exists n0', n0 = S n0'); deex; subst.
    enough ((As1 = As2 /\ r1 = r2 /\ S n = S n0') <-> (As1 = As2 /\ r1 = r2 /\ n = n0')) as ->.
    {
    inv H1. inv H4. inv H6. inv H2. inv H3. inv H5. eauto. inv H1. inv H1. 
    inv H3. inv H1. inv H1. inv H2.
    eapply determ_step in H4; eauto. now destruct H4.
    eapply determ_step in H4; eauto. destruct H4 as [_ H4]; subst.
    inv H3; eauto.
    }
    clear. split; intros; firstorder eauto.
    inv H2. inv H0. inv H1. inv H2. inv H2. now exists n1. now exists n1. 
Qed.

(** (Robust) Satisfaction *)
Definition sat (pr : prog) (π : tracepref -> Prop) := forall (As : tracepref) (r : rtprog), progstep pr As r -> π As.
Definition rsat (f : symbol) (π : tracepref -> Prop) := forall (C : LinkageContext), sat (plug' C f) π.
