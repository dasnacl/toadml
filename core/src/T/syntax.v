(** * Abstract Syntax of T *)

Set Implicit Arguments.
Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.

Require Import ToadML.Shared.Sema ToadML.Shared.Fresh ToadML.Util.Sets ToadML.Shared.Fresh.
Require Import ToadML.Util.HasEquality ToadML.Util.Convenience ToadML.Util.NoDupInv ToadML.Util.Util.

From RecordUpdate Require Import RecordSet.

Definition label := nat.
Definition register := nat.

Definition string_of_label := string_of_nat.
Definition string_of_reg := string_of_nat.

(** Values are numbers or booleans. *)
Inductive value : Type :=
| Vnat : nat -> value
| Vbool : bool -> value
| Vlabel : label -> value
| Vreg : register -> value
.
Coercion Vnat : nat >-> value.
Coercion Vbool : bool >-> value.
Definition value_eqb (v1 v2 : value) : bool :=
  match v1, v2 with
  | Vnat n1, Vnat n2 => Nat.eqb n1 n2
  | Vbool n1, Vbool n2 => Bool.eqb n1 n2
  | Vlabel l1, Vlabel l2 => Nat.eqb l1 l2
  | Vreg r1, Vreg r2 => Nat.eqb r1 r2
  | _, _ => false
  end
.
Lemma value_eqb_eq v1 v2 :
  value_eqb v1 v2 = true <-> v1 = v2.
Proof.
  revert v2; induction v1; cbn; split; intros.
  - destruct v2; try easy. apply Nat.eqb_eq in H; inv H; reflexivity.
  - destruct v2; inv H; apply Nat.eqb_refl.
  - inv H; cbn. destruct v2; inv H1. Import BoolEqb. change ((b == b0) = true) in H0.
    rewrite eqb_eq in H0. now subst.
  - destruct v2; inv H. Import BoolEqb. change ((b0 == b0) = true). now rewrite eq_refl.
  - inv H; cbn. destruct v2; inv H1. apply Nat.eqb_eq in H0; now subst.
  - destruct v2; inv H. now apply Nat.eqb_refl.
  - destruct v2; inv H. apply Nat.eqb_eq in H1; now subst.
  - destruct v2; inv H. now apply Nat.eqb_refl.
Qed.
#[export]
Instance valueeq__Instance : HasEquality value := {
  eq := value_eqb ;
  eqb_eq := value_eqb_eq ;
}.
Variant event : Type :=
| Sstart : event
| Send : event
| Sjmp (q : comms) (v : value) : event
| Scrash : event
.
Definition event_eqb (e1 e2 : event) : bool :=
  match e1, e2 with
  | Sstart, Sstart => true
  | Scrash, Scrash => true
  | Send, Send => true
  | Sjmp q1 v0, Sjmp q2 v1 => andb (v0 == v1) (q1 == q2)
  | _, _ => false
  end
.
Lemma event_eqb_eq e1 e2 :
  event_eqb e1 e2 = true <-> e1 = e2.
Proof.
  destruct e1, e2; cbn; split; intros; try easy; eq_to_defeq value_eqb; eq_to_defeq loc_eqb.
  - eq_to_defeq comms_eqb.
  - inv H; eq_to_defeq comms_eqb.
Qed.
#[export]
Instance eventeq__Instance : HasEquality event := {
  eq := event_eqb ;
  eqb_eq := event_eqb_eq ;
}.
Definition string_of_event (e : event) :=
  match e with
  | Sstart => "Start"
  | Scrash => "↯"
  | Send => "End"
  | Sjmp q v => "Jmp " ++ (string_of_comms q) ++ " " ++
      (match v with
       | Vnat n => string_of_nat n
       | Vbool true => "true"
       | Vbool false => "false"
       | Vlabel l => string_of_label l
       | Vreg r => string_of_reg r
       end)
  end%string
.
#[export]
Instance eventshow__Instance : Show event := {
  show := string_of_event ;
}.

(** List of instructions available. *)
Inductive instr : Type :=
  (** TODO *)
.


Definition symbol : Type := vart * list instr.
Definition vart_of_symbol (s : symbol) := let '(x, instrs) := s in x.
Definition body_of_symbol (s : symbol) := let '(x, instrs) := s in instrs.

(** Viable contexts that can be linked against a symbol. *)
Inductive LinkageContext : Type :=
  (* basically models `let y = call foo e__pre in e__post` *)
| LContext (y : vart) (instrs__pre instrs__post : list instr) : LinkageContext
.
(** Components are just lists of commands, we don't define a special type for that. *)

(** Top-level programs *)
Inductive prog : Type := Cprog (C : LinkageContext) (foo : symbol) : prog.
Definition plug' (C : LinkageContext) (foo : symbol) : prog := Cprog C foo.
