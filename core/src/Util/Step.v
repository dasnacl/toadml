Require Import Strings.String Lists.List Numbers.Natural.Peano.NPeano Lia Program.Equality.
Require Import ToadML.Util.Util ToadML.Util.HasEquality ToadML.Util.Convenience.

Class Step {StepTag : Type} (s : StepTag) (State : Type) (Event : Type) : Type :=
  Sstep (s' : StepTag) (H : s = s') : State -> option Event -> State -> Prop
.
Declare Scope LangNotationsScope.
Delimit Scope LangNotationsScope with langnotationsscope.
#[global]
Notation "e0 '--[]-->(' s ')' e1" := (Sstep s Logic.eq_refl e0 None e1) (at level 81, s at next level, e1 at next level) : LangNotationsScope.
#[global]
Notation "e0 '--[' a ']-->(' s ')' e1" := (Sstep s Logic.eq_refl e0 (Some a) e1) (at level 81, a at next level, s at next level, e1 at next level) : LangNotationsScope.
#[global]
Notation "e0 '--[,' a ']-->(' s ')' e1" := (Sstep s Logic.eq_refl e0 a e1) (at level 81, a at next level, s at next level, e1 at next level) : LangNotationsScope.

(** Typeclass to define language with star step and all that. *)
Class LangParams (Event : Type) (EventEq__Instance : HasEquality Event) (StepTag : Type) : Type := {
  State : Type ;
  __s : StepTag ;
  step : forall (s' : StepTag) (H: __s = s'), State -> option Event -> State -> Prop ;
  is_value : State -> Prop ;
}.

Section Trace.
  Context {StepTag : Type}.
  Context {Event : Type} {Event__Equality : HasEquality Event} {Event__Show : Show Event}.
  Context {langParams : LangParams Event Event__Equality StepTag}.
  (** A trace is an infinite stream of events.
      Termination is modeled by infinitely many `None`.
   *)
  CoInductive trace :=
  | TCons : option Event -> trace -> trace
  .
  (** It is sufficient to look at trace prefixes for safety properties. *)
  Definition tracepref := list Event.
  Fixpoint tracepref_eqb (τ1 τ2 : tracepref) : bool :=
    match τ1, τ2 with
    | nil, nil => true
    | a :: τ1, b :: τ2 => andb (a == b) (tracepref_eqb τ1 τ2)
    | _, _ => false
    end
  .
  Lemma tracepref_eqb_eq τ1 τ2 :
    tracepref_eqb τ1 τ2 = true <-> τ1 = τ2.
  Proof.
    revert τ2; induction τ1; split; intros.
    - induction τ2; now inv H.
    - induction τ2; now inv H.
    - induction τ2; inv H. rewrite bool_and_equiv_prop in H1; destruct H1 as [H1a H1b].
      eq_to_defeq event_eqb. f_equal. now eapply IHτ1.
    - inv H. cbn. rewrite bool_and_equiv_prop; split. eq_to_defeq event_eqb; apply eq_refl.
      now eapply IHτ1.
  Qed.
  #[export]
  Instance traceprefeq__Instance : HasEquality tracepref := {
    eq := tracepref_eqb ;
    eqb_eq := tracepref_eqb_eq ;
  }.

  Fixpoint Tappend (As Bs : tracepref) : tracepref :=
    match As with
    | nil => Bs
    | a :: As => a :: (Tappend As Bs)
    end
  .
  Definition string_of_tracepref (t : tracepref) : string :=
    let aux := fix string_of_tracepref_aux (t : tracepref) (acc : string) : string :=
      match t with
      | nil => acc
      | a :: nil => String.append acc (show a)
      | a :: As =>
          let acc' := String.append acc (String.append (show a) (" · "%string))
          in string_of_tracepref_aux As acc'
      end in
    aux t (""%string)
  .
  #[export]
  Instance traceprefshow__Instance : Show tracepref := {
    show := string_of_tracepref ;
  }.
  Inductive wherein : Event -> tracepref -> nat -> Prop :=
  | whereinIn : forall a As, wherein a (a :: As) 0
  | whereinLook : forall a As b n, a <> b -> wherein a As n -> wherein a (b :: As) (S n)
  .
  Hint Constructors wherein : core.
  Definition in_t (a : Event) (As : tracepref) := exists (n : nat), wherein a As n.
  Lemma wherein_nil (a : Event) :
    forall n, wherein a nil n -> False.
  Proof. now induction n. Qed.
  Lemma wherein_predecessor (a b : Event) (As : tracepref) :
    forall n, a <> b -> wherein a (b :: As) n -> wherein a As (pred n).
  Proof.
    intros.
    inv H0. congruence. destruct As. inv H6. now cbn.
  Qed.
  Lemma wherein_eq (a : Event) (As : tracepref) n m :
    wherein a As n -> wherein a As m -> n = m.
  Proof.
    intros H; revert m; induction H; intros. inv H; easy.
    destruct m. inv H1. congruence. inv H1; auto.
  Qed.
  Lemma wherein_equiv_wherein_cons (a b : Event) (As : tracepref) n :
    a <> b ->
    wherein a As n <-> wherein a (b :: As) (n + 1).
  Proof.
    split.
    - induction 1; intros. now constructor. specialize (IHwherein H). inv IHwherein; try easy.
      constructor; trivial. rewrite Nat.add_comm. now constructor.
    - intros H0. inv H0. rewrite Nat.add_comm in H5; congruence. rewrite Nat.add_comm in H4. now inv H4.
  Qed.
  Definition before (a0 a1 : Event) (As : tracepref) : Prop :=
    exists n0 n1, wherein a0 As n0 /\ wherein a1 As n1 /\ n0 < n1
  .
  Ltac unfold_before := match goal with
                       | [H: before _ _ _ |- _] =>
                         let H0 := fresh "H__before" in
                         let H1 := fresh "H__before" in
                         unfold before in H; deex; destruct H as [H [H1 H2]]
                       end.
  Lemma before_nothing a0 a1 :
    before a0 a1 nil -> False.
  Proof.
    intros. unfold_before; now induction n0, n1.
  Qed.
  Lemma before_split a As a0 a1 n :
    a <> a0 -> a <> a1 ->
    (before a0 a1 As) \/ (a0 = a /\ wherein a1 As n) ->
    before a0 a1 (a :: As)
  .
  Proof.
    intros Ha Hb [H1 | [H1 H2]]; try unfold_before.
    - exists (S n0); exists (S n1); repeat split; auto. now apply Arith_prebase.lt_n_S_stt.
    - subst; congruence.
  Qed.
  Lemma eat_front_in_t a b As :
    a <> b ->
    in_t (a) (b :: As)%list <->
    in_t (a) (As)%list
  .
  Proof.
    intros H0; split; intros [n H1].
    - destruct n; cbn in H1.
      + inv H1; contradiction.
      + exists n. now inv H1.
    - exists (S n); auto.
  Qed.
  Lemma eat_front_wherein a b As n :
    a <> b ->
    wherein a (b :: As) (S n) <->
    wherein a As n
  .
  Proof.
    intros H0; split; intros H1.
    - now inv H1.
    - now constructor.
  Qed.
  Lemma eat_front_before a b c As :
    a <> c ->
    b <> c ->
    before a b (As)%list <->
    before a b (c :: As)%list
  .
  Proof.
    intros H0 H1; split; intros.
    - unfold_before; exists (S n0). exists (S n1). repeat split.
      now rewrite eat_front_wherein.
      now rewrite eat_front_wherein.
      unfold "_ < _" in *. lia.
    - unfold_before.
      destruct n0, n1.
      inv H2. inv H; congruence. inv H__before0; congruence.
      inv H; inv H__before0. exists n0; exists n1; repeat split; auto; lia.
  Qed.
  Lemma before_from_wherein a b As x x0 :
    before (a) (b) (As)%list ->
    wherein (a) (As)%list (x) ->
    wherein (b) (As)%list (x0) ->
    x < x0
  .
  Proof.
    intros. unfold_before.
    eapply wherein_eq in H, H__before0; eauto; subst; easy.
  Qed.

  Local Set Warnings "-uniform-inheritance".
  (** Use this to define a coercion *)
  Definition ev_to_tracepref (e : Event) : tracepref := e :: nil.
  Coercion ev_to_tracepref : Event >-> tracepref.
End Trace.
#[global]
Ltac unfold_before := match goal with
                      | [H: before _ _ _ |- _] =>
                        let H0 := fresh "H__before" in
                        let H1 := fresh "H__before" in
                        unfold before in H; deex; destruct H as [H [H1 H2]]
                      end
.
Section Lang.
  Context {StepTag : Type}.
  Context {Event : Type} {Event__Equality : HasEquality Event} {Event__Show : Show Event}.
  Context {langParams : LangParams Event Event__Equality StepTag}.

  Inductive star_step (s : StepTag) : State -> tracepref -> State -> Prop :=
  | ES_refl : forall (r1 : State),
      star_step s r1 nil r1
  | ES_trans_important : forall (r1 r2 r3 : State) (a : Event) (As : tracepref),
      step __s Logic.eq_refl r1 (Some a) r2 ->
      star_step s r2 As r3 ->
      star_step s r1 (a :: As) r3
  | ES_trans_unimportant : forall (r1 r2 r3 : State) (As : tracepref),
      step __s Logic.eq_refl r1 None r2 ->
      star_step s r2 As r3 ->
      star_step s r1 As r3
  .
  Hint Constructors star_step : core.

  Lemma must_step_once s (S0 S2 : State) a (As : tracepref) :
    star_step s S0 (a :: As)%list S2 ->
    exists S0' S1, step __s Logic.eq_refl S0' (Some a) S1 /\ star_step s S1 As S2
  .
  Proof.
    intros H; dependent induction H; eauto.
  Qed.

  Lemma star_step_trans s e e' e'' As0 As1 :
    star_step s e As0 e' ->
    star_step s e' As1 e'' ->
    star_step s e (As0 ++ As1) e''
  .
  Proof.
    induction 1; eauto; intros; econstructor; eauto.
  Qed.
  Lemma star_step_trans_nil s e e' e'' :
    star_step s e nil e' ->
    star_step s e' nil e'' ->
    star_step s e nil e''
  .
  Proof.
    intros H; dependent induction H; eauto.
  Qed.

  Inductive n_step (s : StepTag) : State -> nat -> tracepref -> State -> Prop :=
  | ENS_refl : forall (r1 : State),
      n_step s r1 0 nil r1
  | ENS_trans_important : forall (r1 r2 r3 : State) (a : Event) (As : tracepref) (n : nat),
      step __s Logic.eq_refl r1 (Some a) r2 ->
      n_step s r2 n As r3 ->
      n_step s r1 (S n) (a :: As) r3
  | ENS_trans_unimportant : forall (r1 r2 r3 : State) (As : tracepref) (n : nat),
      step __s Logic.eq_refl r1 None r2 ->
      n_step s r2 n As r3 ->
      n_step s r1 (S n) As r3
  .
  Hint Constructors star_step : core.

  Lemma n_step_trans_unimp s (r1 r2 r3 : State) (n0 n1 : nat) :
    n_step s r1 n0 nil r2 ->
    n_step s r2 n1 nil r3 ->
    n_step s r1 (n0 + n1) nil r3
  .
  Proof.
    revert r1 r2 n1 r3; induction n0; intros.
    - inv H. now cbn.
    - inv H. eapply ENS_trans_unimportant; eauto.
  Qed.
  Lemma n_step_trans s (r1 r2 r3 : State) (n0 n1 : nat) (As0 As1 : tracepref) :
    n_step s r1 n0 As0 r2 ->
    n_step s r2 n1 As1 r3 ->
    n_step s r1 (n0 + n1) (As0 ++ As1) r3
  .
  Proof.
    revert r1 As0 r2 n1 As1 r3; induction n0; intros.
    - inv H. now cbn.
    - inv H; eauto using ENS_trans_important, ENS_trans_unimportant.
  Qed.
  Lemma star_step_n_step s (S0 S2 : State) (As : tracepref) :
    star_step s S0 As S2 ->
    exists (n : nat), n_step s S0 n As S2
  .
  Proof.
    intros H; dependent induction H; deex; (exists 0 + exists (S n)); econstructor; eauto.
  Qed.
End Lang.

#[global]
Notation "e0 '==[' a ']==>(' s ')*' e1" := (star_step s e0 a e1) (at level 82, a at next level, s at next level, e1 at next level) : LangNotationsScope.
#[global]
Notation "e0 '==[]==>(' s ')*' e1" := (star_step s e0 nil e1) (at level 82, s at next level, e1 at next level) : LangNotationsScope.

#[global]
Notation "e0 '-(' n ')-[' As ']-->(' s ')' e1" := (n_step s e0 n As e1) (at level 82, e1 at next level) : LangNotationsScope.
#[global]
Notation "e0 '-()-[]-->(' s ')' e1" := (n_step s e0 0 nil e1) (at level 82, e1 at next level) : LangNotationsScope.

