Set Implicit Arguments.
Require Import String.

(** Some generic tactics. *)
#[global]
Ltac inv H := (inversion H; subst; clear H).
#[global]
Ltac deex :=
  repeat match goal with
         | [ H: exists (name:_), _ |- _ ] =>
           let name' := fresh name in
           destruct H as [name' H]
         end
.

(** Useful typeclasses *)
Class Monad (m : Type -> Type) : Type := {
  ret : forall {t : Type}, t -> m t ;
  bind : forall {t u : Type}, m t -> (t -> m u) -> m u
}.
#[global] Notation "x <- c1 ;; c2" := (@bind _ _ _ _ c1 (fun x => c2)) (at level 61, c1 at next level, right associativity).
#[global] Notation "'$' pat '<-' c1 ';;' c2" := (@bind _ _ _ _ c1 (fun x => match x with pat => c2 end)) (at level 61, pat pattern, c1 at next level, right associativity).
#[global] Notation "'let*' p ':=' c1 'in' c2" := (@bind _ _ _ _ c1 (fun p => c2)) (at level 61, p as pattern, c1 at next level, right associativity).

(** Options *)
#[global]
Instance OptionMonad__Instance : Monad option := {
  ret T x := Some x ;
  bind T U m f := match m with
                  | None => None
                  | Some x => f x
                  end
}.
Lemma get_rid_of_letstar {A B:Type} (a : A) (x : A) (f : A -> option B):
  (let* a := Some x in f a) = f x.
Proof. now cbn. Qed.
Definition is_Some {A : Type} (mx : option A) := exists x, mx = Some x.
#[global] Hint Unfold is_Some : core.
Lemma is_Some_alt {A : Type} (mx : option A) :
  is_Some mx <-> match mx with Some _ => True | None => False end.
Proof.
  unfold is_Some; destruct mx; split; try easy; try now exists a.
  intros; now destruct H.
Qed.

Lemma not_eq_None_Some {A : Type} (mx : option A) : mx <> None <-> is_Some mx.
Proof. rewrite is_Some_alt; destruct mx; try easy; congruence. Qed.

Lemma option_dec {A : Type} (mx : option A) : { mx <> None } + { mx = None }.
Proof. destruct mx; now (left + right). Qed.

#[global] Hint Resolve get_rid_of_letstar : core.

(** Pretty Printing *)
Class Show A : Type := {
  show : A -> string
}.
#[export] Instance show__pair {A B : Type} `{Show A} `{Show B} : Show (A * B) := {
  show p := (let (a,b) := p in "(" ++ show a ++ "," ++ show b ++ ")")%string
}.
#[export]
Instance show__bool : Show bool := {
  show := fun (b : bool) => 
    match b with
    | true => "true"
    | false => "false"
    end%string
}.
#[export]
Instance show__unit : Show unit := {
  show := fun (b : unit) => "()"%string
}.
#[export] Instance show__list {A : Type} `{Show A} : Show (list A) := {
  show := fun (xs : list A) =>
    (let show_list_aux := fix show_list_aux {A : Type} (s : A -> string) (l : list A) : string :=
      match l with
      | nil => ""
      | cons x nil => s x
      | cons h t => append (append (s h) ", ") (show_list_aux s t)
      end
    in append "[" (append (show_list_aux show xs) "]"))%string
}.
Require Import Strings.Ascii Numbers.Natural.Peano.NPeano.
Definition ascii_of_nat (n : nat) : ascii :=
  match n with
  | 0 => "0"
  | 1 => "1"
  | 2 => "2"
  | 3 => "3"
  | 4 => "4"
  | 5 => "5"
  | 6 => "6"
  | 7 => "7"
  | 8 => "8"
  | _ => "9"
  end
.
#[export]
Instance show__nat : Show nat := {
  show := fun (n : nat) =>
    let string_of_nat_aux := fix string_of_nat_aux (time n : nat) (acc : string) : string :=
      let acc' := String (ascii_of_nat (n mod 10)) acc in
      match time with
      | 0 => acc'
      | S time' =>
        match n / 10 with
        | 0 => acc'
        | n' => string_of_nat_aux time' n' acc'
        end
      end
    in string_of_nat_aux n n ""%string
}.

