Require Import FunctionalExtensionality Relations RelationClasses.
Require Import ToadML.Util.HasEquality.

Notation "f ∙ g" := (fun a => g(f a)) (at level 11, left associativity).
Definition unique {A : Type} (a : A) (property : A -> Prop) :=
  property a ->
  forall (b : A), property b -> a = b
.

Class Injection {A B : Type} (f : A -> B) := {
  injection : forall (a1 a2 : A), f a1 = f a2 -> a1 = a2
}.
Class Surjection {A B : Type} (f : A -> B) := {
  surjection : forall (b : B), exists (a : A), f a = b
}.
Class Bijection {A B : Type} (f : A -> B) := {
  bijection__inj : Injection f ;
  bijection__surj : Surjection f ;
}.
Inductive Img {A B : Type} (f : A -> B) : Type :=
| ImgC : forall (a : A) (b : B), b = f a -> Img f
.
Lemma canonical_injection {A B : Type} (f : A -> B) :
  exists (g : (Img f) -> A), Injection g
.
Proof.
  exists (fun (b : Img f) => match b with
                             | ImgC _ a _ _ => a
                             end).
  constructor; intros [] [] ->; subst.
  reflexivity.
Qed. 

Class Poset {A : Type} (rel : relation A) := {
  poset__reflexive : reflexive A rel ; 
  poset__transitive : transitive A rel ; 
  poset__antisymmetric : antisymmetric A rel ; 
}.
#[global] Hint Resolve poset__reflexive poset__transitive poset__antisymmetric : core.
#[global] Instance reflexive_poset {A : Type} {rel : relation A} (pos : Poset rel) :
  Reflexive rel
.
Proof. apply poset__reflexive. Qed.
#[global] Instance transitive_poset {A : Type} {rel : relation A} (pos : Poset rel) :
  Transitive rel
.
Proof. apply poset__transitive. Qed.
#[global] Instance antisymmetric_poset {A : Type} {rel : relation A} (pos : Poset rel) :
  Antisymmetric A Logic.eq rel
.
Proof. apply poset__antisymmetric. Qed.
Lemma eq_from_lt_gt {A : Type} (rel : relation A) {Poset_rel : Poset rel} (a b : A) :
  rel a b ->
  rel b a -> 
  a = b
.
Proof.
  intros H0 H1; now specialize (Poset_rel.(poset__antisymmetric) a b H0 H1).
Qed.
#[global] Hint Resolve eq_from_lt_gt : core.

Definition gtrel {A : Type} (rel : relation A) (x y : A) := rel y x /\ x <> y.
Lemma wf_gtrel {A : Type} (rel : relation A) {Poset__rel : Poset rel} (x : A) :
  well_founded rel ->
  well_founded (gtrel rel)
.
Proof.
  intros H. induction x using (well_founded_induction H).
  specialize (H0 x); apply H0. reflexivity.
Qed.

Class Monotone {A B : Type} (rel1 : relation A) (rel2 : relation B)
               {Poset__rel1 : Poset rel1} {Poset__rel2 : Poset rel2}
               (f : A -> B) := {
  monotone : forall (x y : A), rel1 x y -> rel2 (f x) (f y)
}.

Definition order_embedding {A B : Type} {rel1 : relation A} {rel2 : relation B} `{Poset A rel1} `{Poset B rel2} (f : A -> B) :=
  forall (x y : A), rel1 x y <-> rel2 (f x) (f y)
.

#[global]
Instance monotone_trans {A B C : Type} (rel1 : relation A) (rel2 : relation B) (rel3 : relation C)
  {Poset__rel1 : Poset rel1} {Poset__rel2 : Poset rel2} {Poset__rel3 : Poset rel3}
  (f : A -> B) (g : B -> C) {HMon__f: Monotone rel1 rel2 f} {HMon__g: Monotone rel2 rel3 g} :
  Monotone rel1 rel3 (f ∙ g)
.
Proof.
  constructor; intros x y H0; destruct HMon__f, HMon__g; eauto.
Qed.

(** Upper/Lower bounds of a subset X *)
Definition upper_bound {A : Type} {rel : relation A} {Poset__rel : Poset rel} (X : A -> Prop) (a : A) :=
  forall (u : A), X u -> rel u a
.
Definition lower_bound {A : Type} {rel : relation A} {Poset__rel : Poset rel} (X : A -> Prop) (a : A) :=
  forall (u : A), X u -> rel a u
. 
Definition lub {A : Type} {rel : relation A} {Poset__rel : Poset rel} (X : A -> Prop) (a : A) :=
  upper_bound X a /\ forall (b : A), upper_bound X b -> rel a b
. 
Definition glb {A : Type} {rel : relation A} {Poset__rel : Poset rel} (X : A -> Prop) (a : A) :=
  lower_bound X a /\ forall (b : A), lower_bound X b -> rel b a
. 
Lemma lub_is_unique {A : Type} {rel : relation A} {Poset__rel : Poset rel} (X : A -> Prop) (a : A) :
  unique a (lub X)
.
Proof.
  intros [HaUpperBound HaSmallest] b [HbUpperBound HbSmallest].
  specialize (HaSmallest b HbUpperBound).
  specialize (HbSmallest a HaUpperBound).
  eauto.
Qed.
Lemma glb_is_unique {A : Type} {rel : relation A} {Poset__rel : Poset rel} (X : A -> Prop) (a : A) :
  unique a (glb X)
.
Proof.
  intros [HaLowerBound HaGreatest] b [HbLowerBound HbGreatest].
  specialize (HaGreatest b HbLowerBound).
  specialize (HbGreatest a HaLowerBound).
  eauto.
Qed.
Definition lub_op {A : Type} {rel : relation A} {Poset__rel : Poset rel} (a b : A) :=
  exists (c : A), lub (fun (u : A) => u = a \/ u = b) c
.
Definition glb_op {A : Type} {rel : relation A} {Poset__rel : Poset rel} (a b : A) :=
  exists (c : A), lub (fun (u : A) => u = a \/ u = b) c
.
Infix "⊔" := (lub_op) (at level 81).
Infix "⊓" := (glb_op) (at level 81).

Notation "⨆ X" := (exists c, lub X c) (at level 81).
Notation "⨅ X" := (exists c, glb X c) (at level 81).
Notation "[ ⨆ X := c ]" := (lub X c) (at level 81, X at next level, c at next level).
Notation "[ ⨅ X := c ]" := (glb X c) (at level 81, X at next level, c at next level).

(** Lattices *)
Class Lattice {A : Type} (rel : relation A) {Poset__rel : Poset rel} := {
  has_lub : forall (a b : A), a ⊔ b ;
  has_glb : forall (a b : A), a ⊓ b ;
}.
Class CompleteLattice {A : Type} (rel : relation A) {Poset__rel : Poset rel} {IsLattice : Lattice rel} := {
  allsubsets_lub : forall (X : A -> Prop), ⨆ X;
  allsubsets_glb : forall (X : A -> Prop), ⨅ X;
}.
Lemma lattice_has_lub {A : Type} {rel : relation A} {Poset__rel : Poset rel} {IsLattice : Lattice rel} {Complete : CompleteLattice rel} :
  ⨅ (fun _ => True)
.
Proof. eapply (Complete.(allsubsets_glb)). Qed.
Lemma lattice_has_glb {A : Type} {rel : relation A} {Poset__rel : Poset rel} {IsLattice : Lattice rel} {Complete : CompleteLattice rel} :
  ⨆ (fun _ => True)
.
Proof. eapply (Complete.(allsubsets_lub)). Qed.

(** Knaster Tarski/Kleene *)
Definition descending_subset {A : Type} (f : A -> A) (rel : relation A) (a : A) := 
  rel (f a) a
.
Definition ascending_subset {A : Type} (f : A -> A) (rel : relation A) (a : A) := 
  rel a (f a)
.
Definition fixpoint {A : Type} (f : A -> A) (a : A) := f a = a.

Lemma iterate_acc {A : Type} (a : A) (rel : relation A) (f : A -> A) (acc : Acc (gtrel rel) a) (prefix : rel a (f a)) (noteq : a <> f a) :
   Acc (gtrel rel) (f a)
.
Proof. eapply Acc_inv; eauto; split; auto. Defined.

Lemma iterate_le {A : Type} (a : A) (rel : relation A) {Poset__rel : Poset rel} (f : A -> A) {MonotoneF : Monotone rel rel f} :
  rel a (f a) ->
  rel (f a) (f(f a))
.
Proof. destruct MonotoneF; eauto. Defined.

Fixpoint iterate {A : Type} {HasEq : HasEquality A} (f : A -> A) (rel : relation A) {Poset__rel : Poset rel} {MonotoneF : Monotone rel rel f} (a : A) (acc : Acc (gtrel rel) a) (prefix : rel a (f a)) {struct acc} : A :=
  let a' := f a in
  match eq_dec' a a' with
  | left _ => a
  | right ineq => iterate f rel a' (iterate_acc a rel f acc prefix ineq) (iterate_le a rel f prefix)
  end
.

Theorem lfp_exists {A : Type} {HasEq : HasEquality A} {rel : relation A} {wf_rel : well_founded rel} {Poset__rel : Poset rel} {IsLattice : Lattice rel} {Complete : CompleteLattice rel} (f : A -> A) (MonotoneF : Monotone rel rel f) :
  exists (c : A), fixpoint f c /\ ([ ⨅ (descending_subset f rel) := c ])
.
Proof.
  destruct lattice_has_lub as [BOT [BOTlowerBound BOTSmallest]].
  assert (Acc (gtrel rel) BOT) as BOTAcc by now apply (wf_gtrel _ BOT) in wf_rel.
  assert (rel BOT (f BOT)) as BOTrel by now apply BOTlowerBound.
  exists (iterate f rel BOT BOTAcc BOTrel); split.
  - (*is a fixpoint*)
    induction BOT using (well_founded_induction wf_rel); eauto.
  - (*is smallest*)
    split; induction BOT using (well_founded_induction wf_rel); eauto.
Qed.

(** Galois Connections *)
Class GaloisConn {A B : Type} (rel1 : relation A) (rel2 : relation B)
  {Poset__rel1 : Poset rel1} {Poset__rel2 : Poset rel2}
  (γ : A -> B) (α : B -> A) := {
  galois_conn : forall (a : A) (b : B), rel2 (γ a) b <-> rel1 a (α b)
}.
Notation "f '<-[' rel1 ']-[' rel2 ']->' g" := (GaloisConn rel1 rel2 f g) (at level 21, rel1 at next level, rel2 at next level, g at next level).
  
#[global]
Instance galois_conn_trans {A B C : Type} (rel1 : relation A) (rel2 : relation B) (rel3 : relation C)
  {Poset__rel1 : Poset rel1} {Poset__rel2 : Poset rel2} {Poset__rel3 : Poset rel3}
  {f : A -> B} {g : B -> A} {h : B -> C} {i : C -> B}
  (Gfg1 : f <-[ rel1 ]-[ rel2 ]-> g) (Gfg2 : h <-[ rel2 ]-[ rel3 ]-> i) :
  f ∙ h <-[ rel1 ]-[ rel3 ]-> i ∙ g
.
Proof.
  constructor; split; intros H; firstorder.
  - now eapply galois_conn1, galois_conn0.
  - now eapply galois_conn0, galois_conn1.
Qed.

(** Galois Correspondences *)
Class GaloisCorr {A B : Type} (rel1 : relation A) (rel2 : relation B)
  {Poset__rel1 : Poset rel1} {Poset__rel2 : Poset rel2}
  (γ : A -> B) (α : B -> A) := {
  galois_corr : (forall (a : A), a = α(γ a)) /\ (forall (b : B), b = γ(α b)) ;
}.
Notation "f '<-[' rel1 ']=[' rel2 ']->' g" := (GaloisCorr rel1 rel2 f g) (at level 21, rel1 at next level, rel2 at next level, g at next level).

Require Import Classical.

Lemma galois_conn_induces_galois_corr {A B : Type} (rel1 : relation A) (rel2 : relation B) 
  {Poset__rel1 : Poset rel1} {Poset__rel2 : Poset rel2}
  {f : A -> B} {g : B -> A}
  (Gfg : f <-[ rel1 ]-[ rel2 ]-> g) :
  f <-[ rel1 ]=[ rel2 ]-> g
.
Proof.
  destruct (canonical_injection f) as [f__inj Hinj].
  constructor; split; intros a.
  - eapply galois_conn, Poset__rel2.
  - eapply galois_conn, Poset__rel1.
Admitted.
