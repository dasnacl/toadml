(** * Defines top-level program execution. *)

(** The reason why this is not defined in `S/dynamic.v` is that `S/dynamic.v` depends on `S/static.v`,
    while the definition of top-level execution needs both dynamic and static semantics. *)

Set Implicit Arguments.
Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.
Require Import ToadML.Shared.Fresh ToadML.Shared.Sema ToadML.Util.Sets ToadML.Util.HasEquality.
Require Import ToadML.Util.Convenience ToadML.Util.NoDupInv. 

Require Import ToadML.S.syntax ToadML.S.static ToadML.S.dynamic ToadML.Util.Step.
Open Scope LangNotationsScope.

Inductive progstep : prog -> tracepref -> rtexpr -> Prop :=
| prog_step_success : forall (x__foo y : vart) (e__pre e__post e__foo : expr) (foo : symbol) (v v__pre v__foo : value)
                        (n__pre n__foo n__post : nat),
    check_symb foo ->
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    e__foo = expr_of_symbol foo ->
    (* setup the argument for the function call *)
    ▷ e__pre -( n__pre )-[ nil ]-->(ectx) ▷ Xval v__pre ->
    (* do the function call *)
    ▷ (subst x__foo e__foo (Xval v__pre)) -( n__foo )-[ nil ]-->(ectx) ▷ Xval v__foo ->
    (* something may happen after the function call *)
    ▷ subst y e__post (Xval v__foo) -( n__post )-[ nil ]-->(ectx) ▷ Xval v ->
    progstep (Cprog (LContext y e__pre e__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Sret Qcomptoctx (sv v__foo) :: Send (sv v) :: nil)
             (RTerm (Xval v))
| prog_step_failure0 : forall (y : vart) (e__pre e__post : expr) (foo : symbol) (n__pre : nat),
    (* wether component is well-typed or not does not matter if the context yields a crash *)
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    (* setup the argument for the function call ; fails *)
    ▷ e__pre -( n__pre )-[ Scrash :: nil ]-->(ectx) (▷↯) ->
    progstep (Cprog (LContext y e__pre e__post) foo)
             (Sstart :: Scrash :: nil)
             (▷↯)
| prog_step_failure1 : forall (x__foo y : vart) (e__pre e__post e__foo : expr) (foo : symbol) (v__pre : value)
                         (n__pre n__foo : nat),
    check_symb foo ->
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    e__foo = expr_of_symbol foo ->
    (* setup the argument for the function call *)
    ▷ e__pre -( n__pre )-[ nil ]-->(ectx) ▷ Xval v__pre ->
    (* do the function call ; fails *)
    ▷ (subst x__foo e__foo (Xval v__pre)) -( n__foo )-[ Scrash :: nil ]-->(ectx) (▷↯) ->
    progstep (Cprog (LContext y e__pre e__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Scrash :: nil)
             (▷↯)
| prog_step_failure2 : forall (x__foo y : vart) (e__pre e__post e__foo : expr) (foo : symbol) (v__pre v__foo : value)
                         (n__pre n__foo n__post : nat),
    check_symb foo ->
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    e__foo = expr_of_symbol foo ->
    (* setup the argument for the function call *)
    ▷ e__pre -( n__pre )-[ nil ]-->(ectx) ▷ Xval v__pre ->
    (* do the function call *)
    ▷ (subst x__foo e__foo (Xval v__pre)) -( n__foo )-[ nil ]-->(ectx) ▷ Xval v__foo ->
    (* something may happen after the function call; fails *)
    ▷ subst y e__post (Xval v__foo) -( n__post )-[ Scrash :: nil ]-->(ectx) (▷↯) ->
    progstep (Cprog (LContext y e__pre e__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Sret Qcomptoctx (sv v__foo) :: Scrash :: nil)
             (▷↯)
| prog_step_failure3 : forall (x__foo y : vart) (e__pre e__post e__foo : expr) (foo : symbol) (v__pre : value)
                         (n__pre : nat),
    (* this is a dirty hack to sidestep more formalization work. we want ill-typed components to "crash", but it's a hassle
       to extend small step and bigstep accordingly. So, instead, we do it at the top level execution relation of programs *)
    ~(check_symb foo) ->
    (* with traces, we cheat a little. All steps of the language are internal (i.e., they do not produce an action)
       To actually emit traces, we just hardcode them at the top-level... *)
    x__foo = vart_of_symbol foo ->
    e__foo = expr_of_symbol foo ->
    (* setup the argument for the function call *)
    ▷ e__pre -( n__pre )-[ nil ]-->(ectx) ▷ Xval v__pre ->
    (* since the component is ill-typed, just crash immediately *)
    progstep (Cprog (LContext y e__pre e__post) foo)
             (Sstart :: Scall Qctxtocomp (sv v__pre) :: Scrash :: nil)
             (▷↯)
.

(** (Robust) Satisfaction *)
Definition sat (pr : prog) (π : tracepref -> Prop) := forall (As : tracepref) (r : rtexpr), progstep pr As r -> π As.
Definition rsat (f : symbol) (π : tracepref -> Prop) := forall (C : LinkageContext), sat (plug' C f) π.
