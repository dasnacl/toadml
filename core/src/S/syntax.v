(** * Abstract Syntax of S *)

Set Implicit Arguments.
Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.

Require Import ToadML.Shared.Sema ToadML.Shared.Fresh ToadML.Util.Sets ToadML.Shared.Fresh.
Require Import ToadML.Util.Util ToadML.Util.HasEquality ToadML.Util.Convenience ToadML.Util.NoDupInv.

(** Values are numbers or booleans. *)
Inductive value : Type :=
| Vnat : nat -> value
| Vbool : bool -> value
.
Coercion Vnat : nat >-> value.
Coercion Vbool : bool >-> value.
Definition value_eqb (v1 v2 : value) : bool :=
  match v1, v2 with
  | Vnat n1, Vnat n2 => Nat.eqb n1 n2
  | Vbool b1, Vbool b2 => Bool.eqb b1 b2
  | _, _ => false
  end
.
Lemma value_eqb_eq v1 v2 :
  value_eqb v1 v2 = true <-> v1 = v2.
Proof.
  revert v2; induction v1; cbn; split; intros.
  - destruct v2; try easy. apply Nat.eqb_eq in H; inv H; reflexivity.
  - destruct v2; inv H; apply Nat.eqb_refl.
  - destruct v2; inv H; destruct b, b0; inv H1; reflexivity.
  - inv H; now destruct b.
Qed.
#[export]
Instance valueeq__Instance : HasEquality value := {
  eq := value_eqb ;
  eqb_eq := value_eqb_eq ;
}.
(* This little trick is necessary to make Coq prefer 
   varteq__Instance over valueeq__Instance. *)
#[local]
Existing Instance varteq__Instance.
(** The following functions translate from "general" to "language-specific" values and vice-versa. *)
Definition sv (v : value) : nat + bool :=
  match v with
  | Vnat n => inl n
  | Vbool b => inr b
  end
.
Definition vs (v : nat + bool) : value :=
  match v with
  | inl n => Vnat n
  | inr b => Vbool b
  end
.
Variant event : Type :=
| Sstart : event
| Send (v : nat + bool) : event
| Scall (q : comms) (arg : nat + bool) : event
| Sret (q : comms) (v : nat + bool) : event
| Scrash : event
.
Definition event_eqb (e1 e2 : event) : bool :=
  match e1, e2 with
  | Sstart, Sstart => true
  | Scrash, Scrash => true
  | Send nb1, Send nb2 =>
      match nb1, nb2 with
      | inl n1, inl n2 => n1 == n2
      | inr b1, inr b2 => Bool.eqb b1 b2
      | _, _ => false
      end
  | Scall q1 v0, Scall q2 v1 => andb (
      match v0, v1 with
      | inl n1, inl n2 => n1 == n2
      | inr b1, inr b2 => Bool.eqb b1 b2
      | _, _ => false
      end
    ) (q1 == q2)
  | Sret q1 v0, Sret q2 v1 => andb (
      match v0, v1 with
      | inl n1, inl n2 => n1 == n2
      | inr b1, inr b2 => Bool.eqb b1 b2
      | _, _ => false
      end
     ) (q1 == q2)
  | _, _ => false
  end
.
Lemma event_eqb_eq e1 e2 :
  event_eqb e1 e2 = true <-> e1 = e2.
Proof.
  destruct e1, e2; cbn; split; intros; try easy; eq_to_defeq value_eqb; eq_to_defeq loc_eqb; eq_to_defeq comms_eqb.
  - destruct v, v0; try apply Nat.eqb_eq in H; try now inv H.
    destruct b, b0; inv H; easy.
  - destruct v, v0; try (apply Nat.eqb_eq; inv H; reflexivity); try easy.
    now destruct b, b0; inv H. 
  - destruct arg, arg0; inv H. apply Nat.eqb_eq in H1; inv H1; easy.
    destruct b, b0; inv H1; easy.
  - inv H. destruct arg0; repeat split; eq_to_defeq a; try apply Nat.eqb_refl.
    destruct b; easy.
  - destruct v, v0; inv H. apply Nat.eqb_eq in H1; now inv H1.
    destruct b, b0; inv H1; easy.
  - inv H ; split; try easy. destruct v0. apply Nat.eqb_refl. now destruct b.
Qed.
#[export]
Instance eventeq__Instance : HasEquality event := {
  eq := event_eqb ;
  eqb_eq := event_eqb_eq ;
}.
Definition string_of_event (e : event) :=
  match e with
  | Sstart => "Start"
  | Scrash => "↯"
  | (Send v) => "End " ++
      (match v with
       | inl n => string_of_nat n
       | inr true => "true"
       | inr false => "false"
       end)
  | (Scall q v) => "Call " ++ (string_of_comms q) ++ " " ++
      (match v with
       | inl n => string_of_nat n
       | inr true => "true"
       | inr false => "false"
       end)
  | (Sret q v) => "Ret " ++ (string_of_comms q) ++ " " ++
      (match v with
       | inl n => string_of_nat n
       | inr true => "true"
       | inr false => "false"
       end)
  end%string
.
#[export]
Instance eventshow__Instance : Show event := {
  show := string_of_event ;
}.
(** The actual abstract syntax of our expressions. *)
Inductive expr : Type :=
| Xval (v : value) : expr
| Xvar (x : vart) : expr
| Xbinop (symb : binopsymb) (lhs rhs : expr) : expr
| Xif (c e0 e1 : expr) : expr
| Xlet (x : vart) (e0 e1 : expr) : expr
| Xabort : expr
.
Coercion Xval : value >-> expr.
Coercion Xvar : vart >-> expr.
(* Notation trickery *)
Declare Custom Entry S.
Declare Scope S.

Notation "'<(' e ')>'" := e (at level 0, e custom S at level 99) : S.
Notation "'(' x ')'" := x (in custom S, x at level 99) : S.
Notation "'{' x '}'" := (Xvar x) (in custom S, x at level 99) : S.
Notation "x" := x (in custom S at level 0, x constr at level 0) : S.
Notation "a '+' b" := (Xbinop Badd a b) (in custom S at level 50, left associativity) : S.
Notation "a '-' b" := (Xbinop Bsub a b) (in custom S at level 50, left associativity) : S.
Notation "a '*' b" := (Xbinop Bmul a b) (in custom S at level 40, left associativity) : S.
Notation "a '/' b" := (Xbinop Bdiv a b) (in custom S at level 40, left associativity) : S.
Notation "a '<' b" := (Xbinop Bless a b) (in custom S at level 60, left associativity) : S.
Notation "'if' e0 'then' e1 'else' e2" := (Xif e0 e1 e2) (in custom S at level 100, left associativity).
Notation "'let' x ':=' e0 'in' e1" := (Xlet x e0 e1) (in custom S at level 100, left associativity).
Notation "'abort'" := (Xabort) (in custom S at level 0) : S.

Open Scope S. Open Scope string.

(* We can now use the funky notation to build abstract syntax trees. *)
(* Check the output of the following two and see that they are equal.
   The notation makes our life much more happy. *)
Check (<( ({"x"} + 2) * 5 )>).
Check (Xbinop Bmul (Xbinop Badd (Xvar "x") (Xval(Vnat 2))) (Xval(Vnat 5))).

Check (<( if ({"x"} < 2) then 5 else 13 + 2 )>).

(* Anything of value type is actually a value in this language. *)
Inductive is_val : expr -> Prop :=
| Cval : forall (v : value), is_val (Xval v)
.
(** The following is a helper function to easily define functions over the syntax of S, e.g. substitution. *)
(** It is known as a specific instance of a "recursion scheme". *)
Definition exprmap (h : expr -> expr) (e : expr) :=
  match e with
  | Xbinop b lhs rhs => Xbinop b (h lhs) (h rhs)
  | Xif c e0 e1 => Xif (h c) (h e0) (h e1)
  | Xlet x e0 e1 => Xlet x (h e0) (h e1)
  | _ => e
  end
.
(** We proceed to define the dynamic semantics via evaluation contexts/environments. *)
Inductive evalctx : Type :=
| Khole : evalctx
| KbinopL (b : binopsymb) (K : evalctx) (e : expr) : evalctx
| KbinopR (b : binopsymb) (n : nat) (K : evalctx) : evalctx
| Kif (K : evalctx) (e0 e1 : expr) : evalctx
| Klet (x : vart) (K : evalctx) (e : expr) : evalctx
.
(** Types of S *)
Inductive ty : Type :=
| Tnat : ty
| Tbool : ty
| Tfun : ty -> ty -> ty
.
Notation "'Tℕ'" := (Tnat) : S.
Notation "'T𝔹" := (Tbool) : S.
Fixpoint ty_eqb (t1 t2 : ty) : bool :=
  match t1, t2 with
  | Tnat, Tnat => true
  | Tbool, Tbool => true
  | Tfun t1 t2, Tfun t1' t2' => andb (ty_eqb t1 t1') (ty_eqb t2 t2')
  | _, _ => false
  end
.
Lemma ty_eqb_eq t0 t1 :
  ty_eqb t0 t1 = true <-> t0 = t1.
Proof.
  split; revert t1; induction t0; intros.
  - destruct t1; now cbn.
  - destruct t1; now cbn.
  - destruct t1; try now cbn in H. cbn in H.
    rewrite bool_and_equiv_prop in H; destruct H as [H1 H2].
    apply IHt0_1 in H1; apply IHt0_2 in H2; subst; easy.
  - now destruct t1.
  - now destruct t1.
  - destruct t1; try congruence. inv H. cbn; rewrite bool_and_equiv_prop; eauto.
Qed.
#[export]
Instance tyeq__Instance : HasEquality ty := {
  eq := ty_eqb ;
  eqb_eq := ty_eqb_eq ;
}.
(* Again, make sure Coq chooses varteq__Instance when resolving implicits. *)
#[local]
Existing Instance varteq__Instance.

(** Symbols look like `fn foo x : τ := e` *)
Definition symbol : Type := vart * ty * ty * expr.

(** Viable contexts that can be linked against a symbol. *)
Inductive LinkageContext : Type :=
  (* basically models `let y = call foo e__pre in e__post` *)
| LContext (y : vart) (e__pre e__post : expr) : LinkageContext
.
Definition vart_of_symbol (s : symbol) := let '(v, t0, t1, e) := s in v.
Definition expr_of_symbol (s : symbol) := let '(v, t0, t1, e) := s in e.
Definition ty_of_symbol (s : symbol) := let '(v, t0, t1, e) := s in Tfun t0 t1.
#[global]
Hint Unfold vart_of_symbol expr_of_symbol ty_of_symbol : core.

(** Top-level programs *)
Inductive prog : Type := Cprog (C : LinkageContext) (foo : symbol) : prog.
Definition plug' (C : LinkageContext) (foo : symbol) : prog := Cprog C foo.

(** Some utility for substitutions. *)
Definition substitutions : Type := mapind varteq__Instance value.

(** The actual substitution operation. *)
Definition subst (what : vart) (inn forr : expr) : expr :=
  let fix R e :=
    match e with
    | Xval _ => e
    | Xabort => Xabort
    | Xvar x => if vareq what x then forr else e
    | Xbinop b e1 e2 => Xbinop b (R e1) (R e2)
    | Xif c e1 e2 => Xif (R c) (R e1) (R e2)
    | Xlet x e1 e2 => Xlet x (R e1) (if vareq what x then e2 else (R e2))
    end
  in
  R inn
.
(* Applying a list of substitutions is just folding over that list. *)
Fixpoint apply_substitutions (e : expr) (γs : substitutions) : expr :=
  match γs with
  | mapNil _ _ => e
  | x ↦ v ◘ γs' => apply_substitutions (subst x e (Xval v)) γs'
  end
.
#[local]
Notation "e '⦇' γs '⦈'" := (apply_substitutions e γs) (at level 81, γs at next level, left associativity) : S.

(* Now a bunch of commutation properties for lists of substitutions. *)
Lemma value_value_substitution_zap (v v' : value) (γs : substitutions) :
  Xval v = (Xval v' ⦇ γs ⦈) ->
  v = v'
.
Proof.
  revert v v'; induction γs; cbn; intros.
  now inv H.
  specialize (IHγs v v' H); now subst.
Qed.
Lemma same_value_substitution_zap (v : value) (γs : substitutions) :
  (Xval v ⦇ γs ⦈) = Xval v
.
Proof. now induction γs. Qed.
Lemma abort_substitution_zap (γs : substitutions) :
  (Xabort ⦇ γs ⦈) = Xabort
.
Proof. now induction γs. Qed.
Lemma value_substitution_zap (v : value) (e : expr) (γs : substitutions) :
  Xval v = (e ⦇ γs ⦈) ->
  e = Xval v \/ exists x, Min γs x v /\ e = Xvar x
.
Proof.
  revert v e; induction γs; intros v e H; destruct e; inv H.
  - now left.
  - apply value_value_substitution_zap in H1; subst; now left.
  - destruct (eq_dec a x); eq_to_defeq vareq.
    + subst; rewrite eq_refl in H1.
      apply value_value_substitution_zap in H1; subst. right. exists x; split; try easy; now left.
    + apply neqb_neq in H; rewrite H in H1.
      specialize (IHγs v (Xvar x) H1) as [].
      * inv H0.
      * deex; destruct H0 as [Ha Hb]. inv Hb. right. exists x0. split; try easy. now right.
  - specialize (IHγs v (Xbinop symb (subst a e1 (Xval b)) (subst a e2 (Xval b))) H1) as [].
    + inv H.
    + deex; destruct H as [Ha Hb]. inv Hb.
  - specialize (IHγs v (Xif (subst a e1 (Xval b)) (subst a e2 (Xval b)) (subst a e3 (Xval b))) H1) as [].
      * inv H.
      * deex; destruct H as [Ha Hb]. inv Hb.
  - destruct (eq_dec a x); subst.
    + eq_to_defeq vareq; rewrite eq_refl in H1. 
      specialize (IHγs v (Xlet x (subst x e1 b) e2) H1) as [].
      * congruence.
      * deex; destruct H as [Ha Hb]. inv Hb.
    + eq_to_defeq vareq. apply neqb_neq in H; rewrite H in H1; clear H.
      specialize (IHγs v (Xlet x (subst a e1 b) (subst a e2 b)) H1) as [].
      * congruence.
      * deex; destruct H as [Ha Hb]. inv Hb.
  - specialize (IHγs v Xabort H1) as [].
    + inv H.
    + deex; destruct H as [Ha Hb]. inv Hb.
Qed.

Lemma subst_binop_commute (b : binopsymb) (e0 e1 : expr) (γs : substitutions) :
  Xbinop b e0 e1 ⦇ γs ⦈ = Xbinop b (e0 ⦇ γs ⦈) (e1 ⦇ γs ⦈)
.
Proof.
  revert e0 e1; induction γs; intros; cbn; trivial.
Qed.
Lemma subst_if_commute (e0 e1 e2 : expr) (γs : substitutions) :
  Xif e0 e1 e2 ⦇ γs ⦈ = Xif (e0 ⦇ γs ⦈) (e1 ⦇ γs ⦈) (e2 ⦇ γs ⦈)
.
Proof.
  revert e0 e1 e2; induction γs; intros; now cbn.
Qed.
Lemma var_subst_val_dec (x : vart) (γs : substitutions) :
  (exists v, Xvar x ⦇ γs ⦈ = Xval v /\ Min γs x v) \/ Xvar x ⦇ γs ⦈ = Xvar x
.
Proof.
  induction γs; cbn.
  - now right.
  - destruct (eq_dec a x); eq_to_defeq vareq.
    + subst. rewrite eq_refl in *. left; exists b. rewrite same_value_substitution_zap; split; try easy. now left.
    + apply neqb_neq in H; rewrite H in *.
      destruct IHγs; deex; eauto. destruct H0.
      left; exists v. split; try easy. now right.
Qed.
Lemma let_subst_val_dec_one (x x0 : vart) (v : value) (e0 e1 : expr) :
  ((Xlet x0 e0 e1) ⦇ x ↦ v ◘ mapNil _ _ ⦈ = Xlet x0 (e0 ⦇ x ↦ v ◘ mapNil _ _ ⦈) (e1 ⦇ mapNil _ _ ⦈)) \/
  ((Xlet x0 e0 e1) ⦇ x ↦ v ◘ mapNil _ _ ⦈ = Xlet x0 (e0 ⦇ x ↦ v ◘ mapNil _ _ ⦈) (e1 ⦇ x ↦ v ◘ mapNil _ _ ⦈))
.
Proof.
  cbn; destruct (eq_dec x x0); fold (subst x e0 v); fold (subst x e1 v).
  - left; subst. eq_to_defeq vareq; rewrite eq_refl. reflexivity.
  - right; apply neqb_neq in H. eq_to_defeq vareq; rewrite H. 
    reflexivity.
Qed.

Lemma let_subst_val_dec (x : vart) (v : value) (e0 e1 : expr) (γs : substitutions) :
  nodupinv γs ->
  ((Xlet x e0 e1) ⦇ γs ⦈ = Xlet x (e0 ⦇ γs ⦈) (e1 ⦇ delete γs x ⦈) /\ In x (dom γs)) \/
  ((Xlet x e0 e1) ⦇ γs ⦈ = Xlet x (e0 ⦇ γs ⦈) (e1 ⦇ γs ⦈) /\ ~In x (dom γs))
.
Proof.
  intros H; revert H e0 e1; induction 1; cbn; intros; try now firstorder.
  fold (subst a e0 b); fold (subst a e1 b).
  destruct (eq_dec a x); eq_to_defeq vareq.
  - subst; rewrite eq_refl. destruct (IHnodupinv (subst x e0 b) e1).
    left; split; try now left. now rewrite delete_redundant.
    left; split; try now left. now rewrite delete_redundant.
  - apply neqb_neq in H1; rewrite H1. destruct (IHnodupinv (subst a e0 b) (subst a e1 b)) as [[H2a H2b]|[H2a H2b]].
    left; split; (try now right); easy.
    right; split. now rewrite H2a.
    intros []; try (apply neqb_neq in H1; congruence); contradiction.
Qed.
Lemma subst_comm (e : expr) (v : value) (x : vart) (γs : substitutions) :
  nodupinv (x ↦ v ◘ γs) ->
  (e ⦇ x ↦ v ◘ γs ⦈) = subst x (e ⦇ γs ⦈) (Xval v)
.
Proof.
  revert γs; induction e; intros.
  - cbn; rewrite ! same_value_substitution_zap; now cbn.
  - cbn; eq_to_defeq vareq; destruct (eq_dec x x0); subst. rewrite eq_refl.
    rewrite same_value_substitution_zap. inv H.
    destruct (var_subst_val_dec x0 γs); deex. destruct H as [Ha Hb].
    apply Min_in in Hb as []; contradiction. rewrite H; cbn; eq_to_defeq vareq; now rewrite eq_refl.
    apply neqb_neq in H0; rewrite H0.
    destruct (var_subst_val_dec x0 γs); deex. destruct H1 as [Ha Hb].
    rewrite Ha. now cbn. rewrite ! H1; cbn; eq_to_defeq vareq; now rewrite H0.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v)).
    rewrite ! subst_binop_commute. cbn; f_equal; eauto.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v));
    fold (subst x e3 (Xval v)).
    rewrite ! subst_if_commute. cbn; f_equal; eauto.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v)).
    inv H.
    destruct (eq_dec x x0).
    + destruct (let_subst_val_dec x0 v e1 e2 H4) as [[H0a H0b]|[H0a H0b]]; subst; eq_to_defeq vareq.
      rewrite eq_refl in *.
      rewrite H0a; cbn; eq_to_defeq vareq; rewrite eq_refl.
      fold (subst x0 (e1 ⦇ γs ⦈) v).
      rewrite <- IHe1.
      destruct (let_subst_val_dec x0 v (subst x0 e1 v) e2 H4) as [[H1a H1b]|[H1a H1b]]; eq_to_defeq vareq.
      now constructor. 
    + destruct (let_subst_val_dec x0 v e1 e2 H4) as [[H0a H0b]|[H0a H0b]]; eq_to_defeq vareq.
      * apply neqb_neq in H; rewrite H. rewrite H0a.
        cbn; eq_to_defeq vareq; rewrite H.
        fold (subst x (e1 ⦇ γs ⦈) v); fold (subst x (e2 ⦇ delete γs x0 ⦈) v).
        rewrite <- IHe1, <- IHe2.
        destruct (let_subst_val_dec x0 v (subst x e1 v) (subst x e2 v) H4) as [[H1a H1b]|[H1a H1b]]; eq_to_defeq vareq.
        constructor. intros H1. apply delete_indom_pres in H1; eauto.
        now apply neqb_neq.
        now apply delete_nodupinv_pres.
        now constructor. 
      * apply neqb_neq in H; rewrite H. rewrite H0a.
        cbn; eq_to_defeq vareq; rewrite H.
        fold (subst x (e1 ⦇ γs ⦈) v); fold (subst x (e2 ⦇ γs ⦈) v).
        rewrite <- IHe1, <- IHe2.
        destruct (let_subst_val_dec x0 v (subst x e1 v) (subst x e2 v) H4) as [[H1a H1b]|[H1a H1b]]; eq_to_defeq vareq.
        all: now constructor.
  - rewrite ! abort_substitution_zap; cbn. reflexivity.
Qed.
