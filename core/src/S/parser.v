  

Require Import String.
Require Import ToadML.S.syntax ToadML.Shared.Sema.

Definition expr := ToadML.S.syntax.expr.



From Coq.Lists Require List.
From Coq.PArith Require Import BinPos.
From Coq.NArith Require Import BinNat.
From MenhirLib Require Main.
From MenhirLib Require Version.
Import List.ListNotations.

Definition version_check : unit := MenhirLib.Version.require_unreleased.

Unset Elimination Schemes.

Inductive token : Type :=
| WALRUS : unit%type -> token
| THEN : unit%type -> token
| SUB : unit%type -> token
| RPAREN : unit%type -> token
| NUM :       (nat)%type -> token
| MUL : unit%type -> token
| LPAREN : unit%type -> token
| LET : unit%type -> token
| LESS : unit%type -> token
| IN : unit%type -> token
| IF : unit%type -> token
| ID :       (string)%type -> token
| EOF : unit%type -> token
| ELSE : unit%type -> token
| DIV : unit%type -> token
| ADD : unit%type -> token
| ABORT : unit%type -> token.

Module Import Gram <: MenhirLib.Grammar.T.

Local Obligation Tactic := let x := fresh in intro x; case x; reflexivity.

Inductive terminal' : Set :=
| ABORT't
| ADD't
| DIV't
| ELSE't
| EOF't
| ID't
| IF't
| IN't
| LESS't
| LET't
| LPAREN't
| MUL't
| NUM't
| RPAREN't
| SUB't
| THEN't
| WALRUS't.
Definition terminal := terminal'.

Global Program Instance terminalNum : MenhirLib.Alphabet.Numbered terminal :=
  { inj := fun x => match x return _ with
    | ABORT't => 1%positive
    | ADD't => 2%positive
    | DIV't => 3%positive
    | ELSE't => 4%positive
    | EOF't => 5%positive
    | ID't => 6%positive
    | IF't => 7%positive
    | IN't => 8%positive
    | LESS't => 9%positive
    | LET't => 10%positive
    | LPAREN't => 11%positive
    | MUL't => 12%positive
    | NUM't => 13%positive
    | RPAREN't => 14%positive
    | SUB't => 15%positive
    | THEN't => 16%positive
    | WALRUS't => 17%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => ABORT't
    | 2%positive => ADD't
    | 3%positive => DIV't
    | 4%positive => ELSE't
    | 5%positive => EOF't
    | 6%positive => ID't
    | 7%positive => IF't
    | 8%positive => IN't
    | 9%positive => LESS't
    | 10%positive => LET't
    | 11%positive => LPAREN't
    | 12%positive => MUL't
    | 13%positive => NUM't
    | 14%positive => RPAREN't
    | 15%positive => SUB't
    | 16%positive => THEN't
    | 17%positive => WALRUS't
    | _ => ABORT't
    end)%Z;
    inj_bound := 17%positive }.
Global Instance TerminalAlph : MenhirLib.Alphabet.Alphabet terminal := _.

Inductive nonterminal' : Set :=
| expr'nt
| top_expr'nt.
Definition nonterminal := nonterminal'.

Global Program Instance nonterminalNum : MenhirLib.Alphabet.Numbered nonterminal :=
  { inj := fun x => match x return _ with
    | expr'nt => 1%positive
    | top_expr'nt => 2%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => expr'nt
    | 2%positive => top_expr'nt
    | _ => expr'nt
    end)%Z;
    inj_bound := 2%positive }.
Global Instance NonTerminalAlph : MenhirLib.Alphabet.Alphabet nonterminal := _.

Include MenhirLib.Grammar.Symbol.

Definition terminal_semantic_type (t:terminal) : Type:=
  match t with
  | WALRUS't => unit%type
  | THEN't => unit%type
  | SUB't => unit%type
  | RPAREN't => unit%type
  | NUM't =>       (nat)%type
  | MUL't => unit%type
  | LPAREN't => unit%type
  | LET't => unit%type
  | LESS't => unit%type
  | IN't => unit%type
  | IF't => unit%type
  | ID't =>       (string)%type
  | EOF't => unit%type
  | ELSE't => unit%type
  | DIV't => unit%type
  | ADD't => unit%type
  | ABORT't => unit%type
  end.

Definition nonterminal_semantic_type (nt:nonterminal) : Type:=
  match nt with
  | top_expr'nt =>       (expr)%type
  | expr'nt =>      (expr)%type
  end.

Definition symbol_semantic_type (s:symbol) : Type:=
  match s with
  | T t => terminal_semantic_type t
  | NT nt => nonterminal_semantic_type nt
  end.

Definition token := token.

Definition token_term (tok : token) : terminal :=
  match tok with
  | WALRUS _ => WALRUS't
  | THEN _ => THEN't
  | SUB _ => SUB't
  | RPAREN _ => RPAREN't
  | NUM _ => NUM't
  | MUL _ => MUL't
  | LPAREN _ => LPAREN't
  | LET _ => LET't
  | LESS _ => LESS't
  | IN _ => IN't
  | IF _ => IF't
  | ID _ => ID't
  | EOF _ => EOF't
  | ELSE _ => ELSE't
  | DIV _ => DIV't
  | ADD _ => ADD't
  | ABORT _ => ABORT't
  end.

Definition token_sem (tok : token) : symbol_semantic_type (T (token_term tok)) :=
  match tok with
  | WALRUS x => x
  | THEN x => x
  | SUB x => x
  | RPAREN x => x
  | NUM x => x
  | MUL x => x
  | LPAREN x => x
  | LET x => x
  | LESS x => x
  | IN x => x
  | IF x => x
  | ID x => x
  | EOF x => x
  | ELSE x => x
  | DIV x => x
  | ADD x => x
  | ABORT x => x
  end.

Inductive production' : Set :=
| Prod'top_expr'0
| Prod'expr'10
| Prod'expr'9
| Prod'expr'8
| Prod'expr'7
| Prod'expr'6
| Prod'expr'5
| Prod'expr'4
| Prod'expr'3
| Prod'expr'2
| Prod'expr'1
| Prod'expr'0.
Definition production := production'.

Global Program Instance productionNum : MenhirLib.Alphabet.Numbered production :=
  { inj := fun x => match x return _ with
    | Prod'top_expr'0 => 1%positive
    | Prod'expr'10 => 2%positive
    | Prod'expr'9 => 3%positive
    | Prod'expr'8 => 4%positive
    | Prod'expr'7 => 5%positive
    | Prod'expr'6 => 6%positive
    | Prod'expr'5 => 7%positive
    | Prod'expr'4 => 8%positive
    | Prod'expr'3 => 9%positive
    | Prod'expr'2 => 10%positive
    | Prod'expr'1 => 11%positive
    | Prod'expr'0 => 12%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => Prod'top_expr'0
    | 2%positive => Prod'expr'10
    | 3%positive => Prod'expr'9
    | 4%positive => Prod'expr'8
    | 5%positive => Prod'expr'7
    | 6%positive => Prod'expr'6
    | 7%positive => Prod'expr'5
    | 8%positive => Prod'expr'4
    | 9%positive => Prod'expr'3
    | 10%positive => Prod'expr'2
    | 11%positive => Prod'expr'1
    | 12%positive => Prod'expr'0
    | _ => Prod'top_expr'0
    end)%Z;
    inj_bound := 12%positive }.
Global Instance ProductionAlph : MenhirLib.Alphabet.Alphabet production := _.

Definition prod_contents (p:production) :
  { p:nonterminal * list symbol &
    MenhirLib.Grammar.arrows_right
      (symbol_semantic_type (NT (fst p)))
      (List.map symbol_semantic_type (snd p)) }
 :=
  let box := existT (fun p =>
    MenhirLib.Grammar.arrows_right
      (symbol_semantic_type (NT (fst p)))
      (List.map symbol_semantic_type (snd p)) )
  in
  match p with
  | Prod'expr'0 => box
    (expr'nt, [T NUM't]%list)
    (fun i =>
      ( ToadML.S.syntax.Xval(ToadML.S.syntax.Vnat i) )
)
  | Prod'expr'1 => box
    (expr'nt, [T ID't]%list)
    (fun x =>
      ( ToadML.S.syntax.Xvar x )
)
  | Prod'expr'2 => box
    (expr'nt, [T RPAREN't; NT expr'nt; T LPAREN't]%list)
    (fun _3 e _1 =>
      ( e )
)
  | Prod'expr'3 => box
    (expr'nt, [NT expr'nt; T ADD't; NT expr'nt]%list)
    (fun e2 _2 e1 =>
      ( ToadML.S.syntax.Xbinop Badd e1 e2 )
)
  | Prod'expr'4 => box
    (expr'nt, [NT expr'nt; T SUB't; NT expr'nt]%list)
    (fun e2 _2 e1 =>
      ( ToadML.S.syntax.Xbinop Bsub e1 e2 )
)
  | Prod'expr'5 => box
    (expr'nt, [NT expr'nt; T MUL't; NT expr'nt]%list)
    (fun e2 _2 e1 =>
      ( ToadML.S.syntax.Xbinop Bmul e1 e2 )
)
  | Prod'expr'6 => box
    (expr'nt, [NT expr'nt; T DIV't; NT expr'nt]%list)
    (fun e2 _2 e1 =>
      ( ToadML.S.syntax.Xbinop Bdiv e1 e2 )
)
  | Prod'expr'7 => box
    (expr'nt, [NT expr'nt; T LESS't; NT expr'nt]%list)
    (fun e2 _2 e1 =>
      ( ToadML.S.syntax.Xbinop Bless e1 e2 )
)
  | Prod'expr'8 => box
    (expr'nt, [NT expr'nt; T ELSE't; NT expr'nt; T THEN't; NT expr'nt; T IF't]%list)
    (fun e3 _5 e2 _3 e1 _1 =>
      ( ToadML.S.syntax.Xif e1 e2 e3 )
)
  | Prod'expr'9 => box
    (expr'nt, [NT expr'nt; T IN't; NT expr'nt; T WALRUS't; T ID't; T LET't]%list)
    (fun e2 _5 e1 _3 x _1 =>
      ( ToadML.S.syntax.Xlet x e1 e2 )
)
  | Prod'expr'10 => box
    (expr'nt, [T ABORT't]%list)
    (fun _1 =>
      ( ToadML.S.syntax.Xabort )
)
  | Prod'top_expr'0 => box
    (top_expr'nt, [T EOF't; NT expr'nt]%list)
    (fun _2 e =>
        ( e )
)
  end.

Definition prod_lhs (p:production) :=
  fst (projT1 (prod_contents p)).
Definition prod_rhs_rev (p:production) :=
  snd (projT1 (prod_contents p)).
Definition prod_action (p:production) :=
  projT2 (prod_contents p).

Include MenhirLib.Grammar.Defs.

End Gram.

Module Aut <: MenhirLib.Automaton.T.

Local Obligation Tactic := let x := fresh in intro x; case x; reflexivity.

Module Gram := Gram.
Module GramDefs := Gram.

Definition nullable_nterm (nt:nonterminal) : bool :=
  match nt with
  | top_expr'nt => false
  | expr'nt => false
  end.

Definition first_nterm (nt:nonterminal) : list terminal :=
  match nt with
  | top_expr'nt => [NUM't; LPAREN't; LET't; IF't; ID't; ABORT't]%list
  | expr'nt => [NUM't; LPAREN't; LET't; IF't; ID't; ABORT't]%list
  end.

Inductive noninitstate' : Set :=
| Nis'31
| Nis'30
| Nis'28
| Nis'27
| Nis'26
| Nis'25
| Nis'24
| Nis'23
| Nis'22
| Nis'21
| Nis'20
| Nis'19
| Nis'18
| Nis'17
| Nis'16
| Nis'15
| Nis'14
| Nis'13
| Nis'12
| Nis'11
| Nis'10
| Nis'9
| Nis'8
| Nis'7
| Nis'6
| Nis'5
| Nis'4
| Nis'3
| Nis'2
| Nis'1.
Definition noninitstate := noninitstate'.

Global Program Instance noninitstateNum : MenhirLib.Alphabet.Numbered noninitstate :=
  { inj := fun x => match x return _ with
    | Nis'31 => 1%positive
    | Nis'30 => 2%positive
    | Nis'28 => 3%positive
    | Nis'27 => 4%positive
    | Nis'26 => 5%positive
    | Nis'25 => 6%positive
    | Nis'24 => 7%positive
    | Nis'23 => 8%positive
    | Nis'22 => 9%positive
    | Nis'21 => 10%positive
    | Nis'20 => 11%positive
    | Nis'19 => 12%positive
    | Nis'18 => 13%positive
    | Nis'17 => 14%positive
    | Nis'16 => 15%positive
    | Nis'15 => 16%positive
    | Nis'14 => 17%positive
    | Nis'13 => 18%positive
    | Nis'12 => 19%positive
    | Nis'11 => 20%positive
    | Nis'10 => 21%positive
    | Nis'9 => 22%positive
    | Nis'8 => 23%positive
    | Nis'7 => 24%positive
    | Nis'6 => 25%positive
    | Nis'5 => 26%positive
    | Nis'4 => 27%positive
    | Nis'3 => 28%positive
    | Nis'2 => 29%positive
    | Nis'1 => 30%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => Nis'31
    | 2%positive => Nis'30
    | 3%positive => Nis'28
    | 4%positive => Nis'27
    | 5%positive => Nis'26
    | 6%positive => Nis'25
    | 7%positive => Nis'24
    | 8%positive => Nis'23
    | 9%positive => Nis'22
    | 10%positive => Nis'21
    | 11%positive => Nis'20
    | 12%positive => Nis'19
    | 13%positive => Nis'18
    | 14%positive => Nis'17
    | 15%positive => Nis'16
    | 16%positive => Nis'15
    | 17%positive => Nis'14
    | 18%positive => Nis'13
    | 19%positive => Nis'12
    | 20%positive => Nis'11
    | 21%positive => Nis'10
    | 22%positive => Nis'9
    | 23%positive => Nis'8
    | 24%positive => Nis'7
    | 25%positive => Nis'6
    | 26%positive => Nis'5
    | 27%positive => Nis'4
    | 28%positive => Nis'3
    | 29%positive => Nis'2
    | 30%positive => Nis'1
    | _ => Nis'31
    end)%Z;
    inj_bound := 30%positive }.
Global Instance NonInitStateAlph : MenhirLib.Alphabet.Alphabet noninitstate := _.

Definition last_symb_of_non_init_state (noninitstate:noninitstate) : symbol :=
  match noninitstate with
  | Nis'1 => T NUM't
  | Nis'2 => T LPAREN't
  | Nis'3 => T LET't
  | Nis'4 => T ID't
  | Nis'5 => T WALRUS't
  | Nis'6 => T IF't
  | Nis'7 => T ID't
  | Nis'8 => T ABORT't
  | Nis'9 => NT expr'nt
  | Nis'10 => T THEN't
  | Nis'11 => NT expr'nt
  | Nis'12 => T SUB't
  | Nis'13 => NT expr'nt
  | Nis'14 => T MUL't
  | Nis'15 => NT expr'nt
  | Nis'16 => T DIV't
  | Nis'17 => NT expr'nt
  | Nis'18 => T LESS't
  | Nis'19 => NT expr'nt
  | Nis'20 => T ADD't
  | Nis'21 => NT expr'nt
  | Nis'22 => T ELSE't
  | Nis'23 => NT expr'nt
  | Nis'24 => NT expr'nt
  | Nis'25 => T IN't
  | Nis'26 => NT expr'nt
  | Nis'27 => NT expr'nt
  | Nis'28 => T RPAREN't
  | Nis'30 => NT expr'nt
  | Nis'31 => T EOF't
  end.

Inductive initstate' : Set :=
| Init'0.
Definition initstate := initstate'.

Global Program Instance initstateNum : MenhirLib.Alphabet.Numbered initstate :=
  { inj := fun x => match x return _ with
    | Init'0 => 1%positive
    end;
    surj := (fun n => match n return _ with
    | 1%positive => Init'0
    | _ => Init'0
    end)%Z;
    inj_bound := 1%positive }.
Global Instance InitStateAlph : MenhirLib.Alphabet.Alphabet initstate := _.

Include MenhirLib.Automaton.Types.

Definition start_nt (init:initstate) : nonterminal :=
  match init with
  | Init'0 => top_expr'nt
  end.

Definition action_table (state:state) : action :=
  match state with
  | Init Init'0 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'1 => Default_reduce_act Prod'expr'0
  | Ninit Nis'2 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'3 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | ID't => Shift_act Nis'4 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'4 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | WALRUS't => Shift_act Nis'5 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'5 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'6 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'7 => Default_reduce_act Prod'expr'1
  | Ninit Nis'8 => Default_reduce_act Prod'expr'10
  | Ninit Nis'9 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | THEN't => Shift_act Nis'10 (eq_refl _)
    | SUB't => Shift_act Nis'12 (eq_refl _)
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Shift_act Nis'18 (eq_refl _)
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Shift_act Nis'20 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'10 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'11 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | SUB't => Shift_act Nis'12 (eq_refl _)
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Shift_act Nis'18 (eq_refl _)
    | ELSE't => Shift_act Nis'22 (eq_refl _)
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Shift_act Nis'20 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'12 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'13 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | THEN't => Reduce_act Prod'expr'4
    | SUB't => Reduce_act Prod'expr'4
    | RPAREN't => Reduce_act Prod'expr'4
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Reduce_act Prod'expr'4
    | IN't => Reduce_act Prod'expr'4
    | EOF't => Reduce_act Prod'expr'4
    | ELSE't => Reduce_act Prod'expr'4
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Reduce_act Prod'expr'4
    | _ => Fail_act
    end)
  | Ninit Nis'14 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'15 => Default_reduce_act Prod'expr'5
  | Ninit Nis'16 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'17 => Default_reduce_act Prod'expr'6
  | Ninit Nis'18 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'19 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | THEN't => Reduce_act Prod'expr'7
    | SUB't => Shift_act Nis'12 (eq_refl _)
    | RPAREN't => Reduce_act Prod'expr'7
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Reduce_act Prod'expr'7
    | IN't => Reduce_act Prod'expr'7
    | EOF't => Reduce_act Prod'expr'7
    | ELSE't => Reduce_act Prod'expr'7
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Shift_act Nis'20 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'20 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'21 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | THEN't => Reduce_act Prod'expr'3
    | SUB't => Reduce_act Prod'expr'3
    | RPAREN't => Reduce_act Prod'expr'3
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Reduce_act Prod'expr'3
    | IN't => Reduce_act Prod'expr'3
    | EOF't => Reduce_act Prod'expr'3
    | ELSE't => Reduce_act Prod'expr'3
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Reduce_act Prod'expr'3
    | _ => Fail_act
    end)
  | Ninit Nis'22 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'23 => Default_reduce_act Prod'expr'8
  | Ninit Nis'24 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | SUB't => Shift_act Nis'12 (eq_refl _)
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Shift_act Nis'18 (eq_refl _)
    | IN't => Shift_act Nis'25 (eq_refl _)
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Shift_act Nis'20 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'25 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | NUM't => Shift_act Nis'1 (eq_refl _)
    | LPAREN't => Shift_act Nis'2 (eq_refl _)
    | LET't => Shift_act Nis'3 (eq_refl _)
    | IF't => Shift_act Nis'6 (eq_refl _)
    | ID't => Shift_act Nis'7 (eq_refl _)
    | ABORT't => Shift_act Nis'8 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'26 => Default_reduce_act Prod'expr'9
  | Ninit Nis'27 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | SUB't => Shift_act Nis'12 (eq_refl _)
    | RPAREN't => Shift_act Nis'28 (eq_refl _)
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Shift_act Nis'18 (eq_refl _)
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Shift_act Nis'20 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'28 => Default_reduce_act Prod'expr'2
  | Ninit Nis'30 => Lookahead_act (fun terminal:terminal =>
    match terminal return lookahead_action terminal with
    | SUB't => Shift_act Nis'12 (eq_refl _)
    | MUL't => Shift_act Nis'14 (eq_refl _)
    | LESS't => Shift_act Nis'18 (eq_refl _)
    | EOF't => Shift_act Nis'31 (eq_refl _)
    | DIV't => Shift_act Nis'16 (eq_refl _)
    | ADD't => Shift_act Nis'20 (eq_refl _)
    | _ => Fail_act
    end)
  | Ninit Nis'31 => Default_reduce_act Prod'top_expr'0
  end.

Definition goto_table (state:state) (nt:nonterminal) :=
  match state, nt return option { s:noninitstate | NT nt = last_symb_of_non_init_state s } with
  | Init Init'0, top_expr'nt => None  | Init Init'0, expr'nt => Some (exist _ Nis'30 (eq_refl _))
  | Ninit Nis'2, expr'nt => Some (exist _ Nis'27 (eq_refl _))
  | Ninit Nis'5, expr'nt => Some (exist _ Nis'24 (eq_refl _))
  | Ninit Nis'6, expr'nt => Some (exist _ Nis'9 (eq_refl _))
  | Ninit Nis'10, expr'nt => Some (exist _ Nis'11 (eq_refl _))
  | Ninit Nis'12, expr'nt => Some (exist _ Nis'13 (eq_refl _))
  | Ninit Nis'14, expr'nt => Some (exist _ Nis'15 (eq_refl _))
  | Ninit Nis'16, expr'nt => Some (exist _ Nis'17 (eq_refl _))
  | Ninit Nis'18, expr'nt => Some (exist _ Nis'19 (eq_refl _))
  | Ninit Nis'20, expr'nt => Some (exist _ Nis'21 (eq_refl _))
  | Ninit Nis'22, expr'nt => Some (exist _ Nis'23 (eq_refl _))
  | Ninit Nis'25, expr'nt => Some (exist _ Nis'26 (eq_refl _))
  | _, _ => None
  end.

Definition past_symb_of_non_init_state (noninitstate:noninitstate) : list symbol :=
  match noninitstate with
  | Nis'1 => []%list
  | Nis'2 => []%list
  | Nis'3 => []%list
  | Nis'4 => [T LET't]%list
  | Nis'5 => [T ID't; T LET't]%list
  | Nis'6 => []%list
  | Nis'7 => []%list
  | Nis'8 => []%list
  | Nis'9 => [T IF't]%list
  | Nis'10 => [NT expr'nt; T IF't]%list
  | Nis'11 => [T THEN't; NT expr'nt; T IF't]%list
  | Nis'12 => [NT expr'nt]%list
  | Nis'13 => [T SUB't; NT expr'nt]%list
  | Nis'14 => [NT expr'nt]%list
  | Nis'15 => [T MUL't; NT expr'nt]%list
  | Nis'16 => [NT expr'nt]%list
  | Nis'17 => [T DIV't; NT expr'nt]%list
  | Nis'18 => [NT expr'nt]%list
  | Nis'19 => [T LESS't; NT expr'nt]%list
  | Nis'20 => [NT expr'nt]%list
  | Nis'21 => [T ADD't; NT expr'nt]%list
  | Nis'22 => [NT expr'nt; T THEN't; NT expr'nt; T IF't]%list
  | Nis'23 => [T ELSE't; NT expr'nt; T THEN't; NT expr'nt; T IF't]%list
  | Nis'24 => [T WALRUS't; T ID't; T LET't]%list
  | Nis'25 => [NT expr'nt; T WALRUS't; T ID't; T LET't]%list
  | Nis'26 => [T IN't; NT expr'nt; T WALRUS't; T ID't; T LET't]%list
  | Nis'27 => [T LPAREN't]%list
  | Nis'28 => [NT expr'nt; T LPAREN't]%list
  | Nis'30 => []%list
  | Nis'31 => [NT expr'nt]%list
  end.
Extract Constant past_symb_of_non_init_state => "fun _ -> assert false".

Definition state_set_1 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'2 | Ninit Nis'5 | Ninit Nis'6 | Ninit Nis'10 | Ninit Nis'12 | Ninit Nis'14 | Ninit Nis'16 | Ninit Nis'18 | Ninit Nis'20 | Ninit Nis'22 | Ninit Nis'25 => true
  | _ => false
  end.
Extract Inlined Constant state_set_1 => "assert false".

Definition state_set_2 (s:state) : bool :=
  match s with
  | Ninit Nis'3 => true
  | _ => false
  end.
Extract Inlined Constant state_set_2 => "assert false".

Definition state_set_3 (s:state) : bool :=
  match s with
  | Ninit Nis'4 => true
  | _ => false
  end.
Extract Inlined Constant state_set_3 => "assert false".

Definition state_set_4 (s:state) : bool :=
  match s with
  | Ninit Nis'6 => true
  | _ => false
  end.
Extract Inlined Constant state_set_4 => "assert false".

Definition state_set_5 (s:state) : bool :=
  match s with
  | Ninit Nis'9 => true
  | _ => false
  end.
Extract Inlined Constant state_set_5 => "assert false".

Definition state_set_6 (s:state) : bool :=
  match s with
  | Ninit Nis'10 => true
  | _ => false
  end.
Extract Inlined Constant state_set_6 => "assert false".

Definition state_set_7 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'2 | Ninit Nis'5 | Ninit Nis'6 | Ninit Nis'10 | Ninit Nis'18 => true
  | _ => false
  end.
Extract Inlined Constant state_set_7 => "assert false".

Definition state_set_8 (s:state) : bool :=
  match s with
  | Ninit Nis'9 | Ninit Nis'11 | Ninit Nis'19 | Ninit Nis'24 | Ninit Nis'27 | Ninit Nis'30 => true
  | _ => false
  end.
Extract Inlined Constant state_set_8 => "assert false".

Definition state_set_9 (s:state) : bool :=
  match s with
  | Ninit Nis'12 => true
  | _ => false
  end.
Extract Inlined Constant state_set_9 => "assert false".

Definition state_set_10 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'2 | Ninit Nis'5 | Ninit Nis'6 | Ninit Nis'10 | Ninit Nis'12 | Ninit Nis'18 | Ninit Nis'20 => true
  | _ => false
  end.
Extract Inlined Constant state_set_10 => "assert false".

Definition state_set_11 (s:state) : bool :=
  match s with
  | Ninit Nis'9 | Ninit Nis'11 | Ninit Nis'13 | Ninit Nis'19 | Ninit Nis'21 | Ninit Nis'24 | Ninit Nis'27 | Ninit Nis'30 => true
  | _ => false
  end.
Extract Inlined Constant state_set_11 => "assert false".

Definition state_set_12 (s:state) : bool :=
  match s with
  | Ninit Nis'14 => true
  | _ => false
  end.
Extract Inlined Constant state_set_12 => "assert false".

Definition state_set_13 (s:state) : bool :=
  match s with
  | Ninit Nis'16 => true
  | _ => false
  end.
Extract Inlined Constant state_set_13 => "assert false".

Definition state_set_14 (s:state) : bool :=
  match s with
  | Init Init'0 | Ninit Nis'2 | Ninit Nis'5 | Ninit Nis'6 | Ninit Nis'10 => true
  | _ => false
  end.
Extract Inlined Constant state_set_14 => "assert false".

Definition state_set_15 (s:state) : bool :=
  match s with
  | Ninit Nis'9 | Ninit Nis'11 | Ninit Nis'24 | Ninit Nis'27 | Ninit Nis'30 => true
  | _ => false
  end.
Extract Inlined Constant state_set_15 => "assert false".

Definition state_set_16 (s:state) : bool :=
  match s with
  | Ninit Nis'18 => true
  | _ => false
  end.
Extract Inlined Constant state_set_16 => "assert false".

Definition state_set_17 (s:state) : bool :=
  match s with
  | Ninit Nis'20 => true
  | _ => false
  end.
Extract Inlined Constant state_set_17 => "assert false".

Definition state_set_18 (s:state) : bool :=
  match s with
  | Ninit Nis'11 => true
  | _ => false
  end.
Extract Inlined Constant state_set_18 => "assert false".

Definition state_set_19 (s:state) : bool :=
  match s with
  | Ninit Nis'22 => true
  | _ => false
  end.
Extract Inlined Constant state_set_19 => "assert false".

Definition state_set_20 (s:state) : bool :=
  match s with
  | Ninit Nis'5 => true
  | _ => false
  end.
Extract Inlined Constant state_set_20 => "assert false".

Definition state_set_21 (s:state) : bool :=
  match s with
  | Ninit Nis'24 => true
  | _ => false
  end.
Extract Inlined Constant state_set_21 => "assert false".

Definition state_set_22 (s:state) : bool :=
  match s with
  | Ninit Nis'25 => true
  | _ => false
  end.
Extract Inlined Constant state_set_22 => "assert false".

Definition state_set_23 (s:state) : bool :=
  match s with
  | Ninit Nis'2 => true
  | _ => false
  end.
Extract Inlined Constant state_set_23 => "assert false".

Definition state_set_24 (s:state) : bool :=
  match s with
  | Ninit Nis'27 => true
  | _ => false
  end.
Extract Inlined Constant state_set_24 => "assert false".

Definition state_set_25 (s:state) : bool :=
  match s with
  | Init Init'0 => true
  | _ => false
  end.
Extract Inlined Constant state_set_25 => "assert false".

Definition state_set_26 (s:state) : bool :=
  match s with
  | Ninit Nis'30 => true
  | _ => false
  end.
Extract Inlined Constant state_set_26 => "assert false".

Definition past_state_of_non_init_state (s:noninitstate) : list (state -> bool) :=
  match s with
  | Nis'1 => [state_set_1]%list
  | Nis'2 => [state_set_1]%list
  | Nis'3 => [state_set_1]%list
  | Nis'4 => [state_set_2; state_set_1]%list
  | Nis'5 => [state_set_3; state_set_2; state_set_1]%list
  | Nis'6 => [state_set_1]%list
  | Nis'7 => [state_set_1]%list
  | Nis'8 => [state_set_1]%list
  | Nis'9 => [state_set_4; state_set_1]%list
  | Nis'10 => [state_set_5; state_set_4; state_set_1]%list
  | Nis'11 => [state_set_6; state_set_5; state_set_4; state_set_1]%list
  | Nis'12 => [state_set_8; state_set_7]%list
  | Nis'13 => [state_set_9; state_set_8; state_set_7]%list
  | Nis'14 => [state_set_11; state_set_10]%list
  | Nis'15 => [state_set_12; state_set_11; state_set_10]%list
  | Nis'16 => [state_set_11; state_set_10]%list
  | Nis'17 => [state_set_13; state_set_11; state_set_10]%list
  | Nis'18 => [state_set_15; state_set_14]%list
  | Nis'19 => [state_set_16; state_set_15; state_set_14]%list
  | Nis'20 => [state_set_8; state_set_7]%list
  | Nis'21 => [state_set_17; state_set_8; state_set_7]%list
  | Nis'22 => [state_set_18; state_set_6; state_set_5; state_set_4; state_set_1]%list
  | Nis'23 => [state_set_19; state_set_18; state_set_6; state_set_5; state_set_4; state_set_1]%list
  | Nis'24 => [state_set_20; state_set_3; state_set_2; state_set_1]%list
  | Nis'25 => [state_set_21; state_set_20; state_set_3; state_set_2; state_set_1]%list
  | Nis'26 => [state_set_22; state_set_21; state_set_20; state_set_3; state_set_2; state_set_1]%list
  | Nis'27 => [state_set_23; state_set_1]%list
  | Nis'28 => [state_set_24; state_set_23; state_set_1]%list
  | Nis'30 => [state_set_25]%list
  | Nis'31 => [state_set_26; state_set_25]%list
  end.
Extract Constant past_state_of_non_init_state => "fun _ -> assert false".

Definition items_of_state (s:state): list item := []%list.
Extract Constant items_of_state => "fun _ -> assert false".

Definition N_of_state (s:state) : N :=
  match s with
  | Init Init'0 => 0%N
  | Ninit Nis'1 => 1%N
  | Ninit Nis'2 => 2%N
  | Ninit Nis'3 => 3%N
  | Ninit Nis'4 => 4%N
  | Ninit Nis'5 => 5%N
  | Ninit Nis'6 => 6%N
  | Ninit Nis'7 => 7%N
  | Ninit Nis'8 => 8%N
  | Ninit Nis'9 => 9%N
  | Ninit Nis'10 => 10%N
  | Ninit Nis'11 => 11%N
  | Ninit Nis'12 => 12%N
  | Ninit Nis'13 => 13%N
  | Ninit Nis'14 => 14%N
  | Ninit Nis'15 => 15%N
  | Ninit Nis'16 => 16%N
  | Ninit Nis'17 => 17%N
  | Ninit Nis'18 => 18%N
  | Ninit Nis'19 => 19%N
  | Ninit Nis'20 => 20%N
  | Ninit Nis'21 => 21%N
  | Ninit Nis'22 => 22%N
  | Ninit Nis'23 => 23%N
  | Ninit Nis'24 => 24%N
  | Ninit Nis'25 => 25%N
  | Ninit Nis'26 => 26%N
  | Ninit Nis'27 => 27%N
  | Ninit Nis'28 => 28%N
  | Ninit Nis'30 => 30%N
  | Ninit Nis'31 => 31%N
  end.
End Aut.

Module MenhirLibParser := MenhirLib.Main.Make Aut.
Theorem safe:
  MenhirLibParser.safe_validator tt = true.
Proof eq_refl true<:MenhirLibParser.safe_validator tt = true.

Definition top_expr : nat -> MenhirLibParser.Inter.buffer -> MenhirLibParser.Inter.parse_result       (expr) := MenhirLibParser.parse safe Aut.Init'0.

Theorem top_expr_correct (log_fuel : nat) (buffer : MenhirLibParser.Inter.buffer):
  match top_expr log_fuel buffer with
  | MenhirLibParser.Inter.Parsed_pr sem buffer_new =>
      exists word (tree : Gram.parse_tree (NT top_expr'nt) word),
        buffer = MenhirLibParser.Inter.app_buf word buffer_new /\
        Gram.pt_sem tree = sem
  | _ => True
  end.
Proof. apply MenhirLibParser.parse_correct with (init:=Aut.Init'0). Qed.

