%{

Require Import String.
Require Import ToadML.S.syntax ToadML.Shared.Sema.

Definition expr := ToadML.S.syntax.expr.

%}

%token<nat> NUM

(*
    This represents all operators. Giving each
    operator its own token probably would have
    been cleaner, but I wanted to include
    strings in the example.
*)
%token ADD SUB MUL DIV LESS WALRUS

%token LPAREN RPAREN
%token IF THEN ELSE ABORT LET IN
%token EOF

%token<string> ID

%left LESS
%left ADD SUB
%left MUL DIV
%left ELSE
%left IN

%type<expr> expr

%start<expr> top_expr
%%

top_expr:
    | e=expr EOF
        { e }

expr:
    | i = NUM
      { ToadML.S.syntax.Xval(ToadML.S.syntax.Vnat i) }
    | x = ID
      { ToadML.S.syntax.Xvar x }
    | LPAREN e = expr RPAREN
      { e }
    | e1 = expr ADD e2 = expr
      { ToadML.S.syntax.Xbinop Badd e1 e2 }
    | e1 = expr SUB e2 = expr
      { ToadML.S.syntax.Xbinop Bsub e1 e2 }
    | e1 = expr MUL e2 = expr
      { ToadML.S.syntax.Xbinop Bmul e1 e2 }
    | e1 = expr DIV e2 = expr
      { ToadML.S.syntax.Xbinop Bdiv e1 e2 }
    | e1 = expr LESS e2 = expr
      { ToadML.S.syntax.Xbinop Bless e1 e2 }
    | IF e1 = expr THEN e2 = expr ELSE e3 = expr
      { ToadML.S.syntax.Xif e1 e2 e3 }
    | LET x = ID WALRUS e1 = expr IN e2 = expr
      { ToadML.S.syntax.Xlet x e1 e2 }
    | ABORT
      { ToadML.S.syntax.Xabort }


