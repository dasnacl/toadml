Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.
Require Import ToadML.Util.Sets ToadML.Util.Convenience ToadML.Util.HasEquality.

Require Import ToadML.Shared.Fresh ToadML.Shared.Sema ToadML.S.syntax.
Require Import ToadML.S.dynamic ToadML.Util.NoDupInv ToadML.Util.Step.

Open Scope LangNotationsScope.
Open Scope S. Open Scope string.

#[local]
Existing Instance varteq__Instance.

(** Static Environment; tracks for each identifier a corresponding type. *)
Definition Gamma : Type := mapind varteq__Instance ty.
Notation "[⋅]" := (mapNil _ _).

(** Type system spec *)
Reserved Notation "'[' Γ '|-' e ':' τ  ']'" (at level 81, Γ at next level, e at next level, τ at next level).
Inductive check : Gamma -> expr -> ty -> Prop :=
| T_var : forall (Γ : Gamma) (x : vart) (τ : ty),
    nodupinv Γ ->
    Min Γ x τ ->
    [Γ |- Xvar x : τ]
| T_abort : forall (Γ : Gamma) (τ : ty),
    [Γ |- Xabort : τ]
| T_vnat : forall (Γ : Gamma) (n : nat),
    [Γ |- Xval(Vnat n) : Tℕ]
| T_vbool : forall (Γ : Gamma) (b : bool),
    [Γ |- Xval(Vbool b) : Tbool]
| T_let : forall (Γ : Gamma) (x : vart) (e1 e2 : expr) (τ τ' : ty),
    nodupinv (x ↦ τ' ◘ Γ) ->
    [Γ |- e1 : τ'] ->
    [x ↦ τ' ◘ Γ |- e2 : τ] ->
    [Γ |- Xlet x e1 e2 : τ]
| T_binop : forall (Γ : Gamma) (b : binopsymb) (e1 e2 : expr),
    b <> Bless ->
    [Γ |- e1 : Tℕ] ->
    [Γ |- e2 : Tℕ] ->
    [Γ |- Xbinop b e1 e2 : Tℕ]
| T_binop_less : forall (Γ : Gamma) (e1 e2 : expr),
    [Γ |- e1 : Tℕ] ->
    [Γ |- e2 : Tℕ] ->
    [Γ |- Xbinop Bless e1 e2 : Tbool]
| T_if : forall (Γ : Gamma) (e0 e1 e2 : expr) (τ : ty),
    [Γ |- e0 : Tbool] ->
    [Γ |- e1 : τ] ->
    [Γ |- e2 : τ] ->
    [Γ |- Xif e0 e1 e2 : τ]
where "'[' Γ '|-' e ':' τ ']'" := (check Γ e τ)
.
#[global]
Hint Constructors check : core.

(** Checking of symbols. A symbol may use the identifier x, because a symbol
    in this language is basically fn(x) ↦ e. *)
Inductive check_symb : symbol -> Prop :=
| check_symb_c : forall (e : expr) (x : vart),
    check (x ↦ Tℕ ◘ [⋅]) e Tℕ ->
    check_symb (x, Tℕ, Tℕ, e)
.
(** Similarily, contexts are like `let y = call foo e__pre in e__post` *)
Inductive check_ctx : LinkageContext -> Prop :=
| check_ctx_c : forall (y : vart) (e__pre e__post : expr),
    [[⋅] |- e__pre : Tℕ] ->
    [y ↦ Tℕ ◘ [⋅] |- e__post : Tℕ] ->
    check_ctx (LContext y e__pre e__post)
.
(** Checking a top-level program just means checking both context and component *)
Definition prog_check (p : prog) : Prop :=
  let '(Cprog C foo) := p in
  check_symb foo /\ check_ctx C
.

(** In order to show type preservation for estep, i.e, contextual steps, 
    we need to formulate what it means for contexts to be well-typed.
    Intuitively, they are just an expression with a hole, so they accept
    an argument; an expression that can be filled into the hole. *)
Definition ectx_check (Γ : Gamma) (K : evalctx) (τ τ' : ty) :=
  forall (e : expr), [Γ |- e : τ] -> [Γ |- insert K e : τ']
.
(** Standard compose and decompose lemmas, i.e., if an expression is the
    result of plugging an evaluation context, the typing of that can be (de-)composed *)
Lemma check_decompose (K : evalctx) (e : expr) (Γ : Gamma) (τ : ty) :
  [Γ |- insert K e : τ ] ->
  exists τ', ectx_check Γ K τ' τ /\ [Γ |- e : τ']
.
Proof.
  revert Γ τ e; induction K; cbn; intros.
  - exists τ; split; easy.
  - inv H.
    + specialize (IHK Γ Tℕ e0 H6); deex; destruct IHK as [IHK1 IHK2].
      exists τ'; split; try easy. unfold ectx_check; intros; cbn.
      constructor; eauto. 
    + specialize (IHK Γ Tℕ e0 H5); deex; destruct IHK as [IHK1 IHK2].
      exists τ'; split; try easy. unfold ectx_check; intros; cbn.
      constructor; eauto. 
  - inv H.
    + specialize (IHK Γ Tℕ e H7); deex; destruct IHK as [IHK1 IHK2].
      exists τ'; split; try easy. unfold ectx_check; intros; cbn.
      constructor; eauto.
    + specialize (IHK Γ Tℕ e H6); deex; destruct IHK as [IHK1 IHK2].
      exists τ'; split; try easy. unfold ectx_check; intros; cbn.
      constructor; eauto.
  - inv H. specialize (IHK Γ 'T𝔹 e H4); deex; destruct IHK as [IHK1 IHK2].
    exists τ'; split; try easy. unfold ectx_check; intros; cbn.
    constructor; eauto.
  - inv H. specialize (IHK Γ τ' e0 H6); deex; destruct IHK as [IHK1 IHK2].
    exists τ'0; split; try easy. unfold ectx_check; intros; cbn.
    econstructor; eauto.
Qed.
Lemma check_recompose (Γ : Gamma) (K : evalctx) (e : expr) (τ τ' : ty) :
  ectx_check Γ K τ' τ ->
  [Γ |- e : τ'] ->
  [Γ |- insert K e : τ]
.
Proof. unfold ectx_check; intros; now specialize (H e H0). Qed.

Lemma swap_typs' (Γ1 Γ2 : Gamma) (e : expr) (τ : ty) :
  meq Γ1 Γ2 -> 
  [Γ1 |- e : τ] ->
  [Γ2 |- e : τ]
.
Proof.
  intros H%meq_sound; subst; easy.
Qed.
Lemma swap_typs'' (x x0 : vart) (τ1 τ2 τ' : ty) (Γ : Gamma) (e : expr) :
  (x <> x0) ->
  [(x ↦ τ1 ◘ (x0 ↦ τ' ◘ Γ)) |- e : τ2 ] ->
  [(x0 ↦ τ' ◘ (x ↦ τ1 ◘ Γ)) |- e : τ2 ]
.
Proof.
  intros. eapply swap_typs' in H0; try eassumption.
  clear. split; intros y τ H.
  destruct H as [[Ha Hb]|H]; subst. right; now left.
  destruct H as [[Ha Hb]|H]; subst. now left.
  right; now right.
  destruct H as [[Ha Hb]|H]; subst. right; now left.
  destruct H as [[Ha Hb]|H]; subst. now left.
  right; now right.
Qed.
Lemma swap_typs (x x0 : vart) (τ1 τ2 τ' : ty) (Γ : Gamma) (e : expr) :
  (x <> x0) ->
  [(x ↦ τ1 ◘ (x0 ↦ τ' ◘ Γ)) |- e : τ2 ] <->
  [(x0 ↦ τ' ◘ (x ↦ τ1 ◘ Γ)) |- e : τ2 ]
.
Proof.
  split; eapply swap_typs''; eauto.
Qed.

Lemma substitution Γ v τ1 τ2 x e :
  nodupinv Γ ->
  [[⋅] |- Xval v : τ1 ] ->
  [(x ↦ τ1 ◘ Γ) |- e : τ2 ] ->
  [Γ |- subst x e (Xval v) : τ2]
.
Proof.
  revert τ1 τ2 Γ; induction e; intros; eauto.
  - inv H1; constructor.
  - cbn; destruct (eq_dec x x0); eq_to_defeq vareq.
    + subst; rewrite eq_refl. inv H1. inv H3. destruct H5 as [[]|]; try congruence; subst.
      inv H0; constructor.
      now apply Min_in in H1 as [].
    + apply neqb_neq in H2 as H2'; rewrite H2'. inv H1. inv H4. destruct H6 as [[]|]; try congruence.
      now constructor.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v)); inv H1; econstructor; eauto.
  - cbn; inv H1; constructor; eauto.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v)). 
    destruct (eq_dec x x0); eq_to_defeq vareq.
    + subst. rewrite eq_refl.
      inv H1. inv H6. exfalso. apply H3; now left.
    + apply neqb_neq in H2 as H2'; rewrite H2'.
      inv H1.
      change (x0 ↦ τ' ◘ (x ↦ τ1 ◘ Γ)) with ((x0 ↦ τ' ◘ mapNil _ _) ◘ (x ↦ τ1 ◘ Γ)) in H7.
      apply nodupinv_swap in H7; inv H7; apply nodupinv_swap in H6; cbn in H6.
      econstructor; eauto.
      eapply IHe2; eauto. eapply swap_typs; eauto.
Qed.

(** Now let's tackle the usual type safety lemmas, i.e., progress and preservation *)
Require Import ToadML.S.dynamic.
Lemma progress_step e τ :
  [[⋅] |- e : τ] ->
  (is_val e) \/ exists a r', (▷ e --[, a ]-->(ectx) r')
.
Proof.
  intros H; dependent induction H.
  - easy.
  - right. exists (Some Scrash). exists (▷↯). econstructor; eauto. instantiate (1:=Khole); auto.
  - left; constructor.
  - left; constructor.
  - specialize (IHcheck1 JMeq_refl); right; clear IHcheck2.
    destruct IHcheck1 as [IHcheck1 | IHcheck1]; deex.
    inv IHcheck1.
    + exists None, (▷ subst x e2 v).
      change (Xlet x v e2) with (insert Khole (Xlet x v e2)).
      econstructor; eauto; now cbn.
    + destruct r' as [e1'|]. exists a, (▷ Xlet x e1' e2).
      inv IHcheck1. 
      change (Xlet x (insert K e) e2) with (insert (Klet x K e2) (e)).
      change (Xlet x (insert K e') e2) with (insert (Klet x K e2) (e')).
      econstructor; eauto. 
      exists a, (▷↯). inv IHcheck1.
      change (Xlet x (insert K e) e2) with (insert (Klet x K e2) (e)).
      eauto.
  - specialize (IHcheck1 JMeq_refl); specialize (IHcheck2 JMeq_refl); right.
    destruct IHcheck1 as [IHcheck1 | IHcheck1]; deex;
    destruct IHcheck2 as [IHcheck2 | IHcheck2]; deex.
    inv IHcheck1; inv IHcheck2.
    + inv H0; inv H1; exists None.
      assert (exists n3, Some(n3) = eval_binop b n n0) by (destruct b; unfold eval_binop; cbn; eauto).
    deex. exists (▷ Xval n3). econstructor; try instantiate (2:=Khole). 1,2: eauto; now cbn. 
    now constructor.
    + inv IHcheck1. inv H0. exists a. inv IHcheck2.
      * exists (▷ (Xbinop b (Xval n) (insert K e'))). econstructor; try eassumption.
        now instantiate (1:=KbinopR b n K). easy.
      * exists (▷↯). econstructor; try eassumption.
        now instantiate (1:=KbinopR b n K).
    + exists a. inv IHcheck1.
      * exists (▷ (Xbinop b (insert K e') e2)). econstructor; try eassumption.
        now instantiate (1:=KbinopL b K e2). easy.
      * exists (▷↯). econstructor; try eassumption.
        now instantiate (1:=KbinopL b K e2).
    + exists a. inv IHcheck1.
      * exists (▷ (Xbinop b (insert K e') e2)). econstructor; try eassumption.
        now instantiate (1:=KbinopL b K e2). easy.
      * exists (▷↯). econstructor; try eassumption.
        now instantiate (1:=KbinopL b K e2).
  - specialize (IHcheck1 JMeq_refl); specialize (IHcheck2 JMeq_refl); right.
    destruct IHcheck1 as [IHcheck1 | IHcheck1]; deex;
    destruct IHcheck2 as [IHcheck2 | IHcheck2]; deex.
    + inv IHcheck1; inv IHcheck2. inv H; inv H0.
      assert (exists n3, Some(n3) = eval_binop Bless n n0) by (unfold eval_binop; cbn; eauto).
      deex. exists None. exists (▷ Xval n3). econstructor; try instantiate (2:=Khole). 1,2: eauto; now cbn.
      now constructor. 
    + inv IHcheck1. inv H. exists a. destruct r'. exists (▷ Xbinop Bless (Xval n) e).
      inv IHcheck2. econstructor. instantiate (2:=KbinopR Bless n K). now cbn. now cbn. 
      assumption. exists (▷↯). inv IHcheck2. econstructor 2.
      instantiate (2:=KbinopR Bless n K). now cbn. now cbn.  
    + destruct r'; exists a. exists (▷ Xbinop Bless e e2).
      inv IHcheck1. econstructor. instantiate (2:=KbinopL Bless K e2).
      now cbn. now cbn. assumption.
      exists (▷↯). inv IHcheck1. econstructor. instantiate (2:=KbinopL Bless K e2).
      now cbn. now cbn.
    + destruct r'; exists a. exists (▷ Xbinop Bless e e2).
      inv IHcheck1. econstructor. instantiate (2:=KbinopL Bless K e2).
      now cbn. now cbn. assumption.
      exists (▷↯). inv IHcheck1. econstructor. instantiate (2:=KbinopL Bless K e2).
      now cbn. now cbn.
  - clear IHcheck2 IHcheck3. specialize (IHcheck1 JMeq_refl).
    right. inv IHcheck1; deex.
    + inv H2. inv H. destruct b.
      * exists None. exists (▷ e1). econstructor. now instantiate (2:=Khole). easy. constructor.
      * exists None. exists (▷ e2). econstructor. now instantiate (2:=Khole). easy. constructor.
    + inv H2.
      * exists a. exists (▷ (insert (Kif K e1 e2) e')). econstructor; eauto. now cbn.
      * exists (Some Scrash). exists ▷↯. econstructor; eauto. now instantiate (1:=Kif K e1 e2).
Qed.
Lemma progress e τ :
  [[⋅] |- e : τ] ->
  (is_val e) \/ exists As r', (▷ e ==[ As ]==>(ectx)* r')
.
Proof.
  intros H; assert (H' := H); apply progress_step in H' as [H' | H']; deex; try now left.
  right. destruct a as [a|]. exists (a :: nil). exists r'. econstructor. eassumption. constructor.
  exists nil. exists r'. econstructor. eassumption. constructor.
Qed.
Lemma pstep_preservation Γ e τ e' a :
  [Γ |- e : τ] ->
  (▷ e --[, a ]-->(prim) ▷ e') ->
  [Γ |- e' : τ]
.
Proof.
  intros H0 H1; cbv in H1; dependent induction H1.
  - inv H0. destruct v. econstructor; eauto. destruct b; try congruence; unfold eval_binop in H; inv H. unfold eval_binop in H; inv H.
    destruct (n1 <? n2)%nat; constructor. 
  - now inv H0.
  - now inv H0.
  - inv H0. eapply substitution; eauto. now inv H4. now inv H6.
Qed.
Lemma estep_preservation Γ e τ e' a :
  [Γ |- e : τ] ->
  (▷ e --[, a ]-->(ectx) ▷ e') ->
  [Γ |- e' : τ]
.
Proof.
  intros H0 H1; cbv in H1; dependent induction H1.
  eapply check_decompose in H0; deex; destruct H0 as [H0a H0b].
  eapply pstep_preservation in H1; eauto.
Qed.
Theorem preservation Γ e τ e' As :
  [Γ |- e : τ] ->
  ▷ e ==[ As ]==>(ectx)* ▷ e' ->
  [Γ |- e' : τ]
.
Proof.
  intros H0 H1; cbv in H1. revert Γ H0; dependent induction H1; try easy; intros.
  - inv H.
    + eapply IHstar_step; eauto. eapply estep_preservation in H0; try eassumption.
      instantiate (1:=Some a). econstructor; easy.
    + inv H1; inv H.
  - inv H.
    eapply IHstar_step; eauto. eapply estep_preservation in H0; try eassumption.
    instantiate (1:=None). econstructor; easy.
Qed.
Theorem preservation' Γ e τ r As :
  [Γ |- e : τ] ->
  ▷ e ==[ As ]==>(ectx)* r ->
  (exists e', (r = (▷ e') /\ [Γ |- e' : τ])) \/ (r = ▷↯)
.
Proof.
  intros; destruct r as [e'|]; eauto using preservation.
Qed.

Lemma preserve_bstep e τ v :
  [[⋅] |- e : τ] ->
  ▷ e ⇓ ▷ Xval v ->
  [[⋅] |- Xval v : τ]
.
Proof.
  intros H0 H1; revert τ H0; dependent induction H1; intros; eauto.
  - inv H0. destruct symb; try congruence; constructor.
    destruct (n1 <? n2)%nat; constructor.
  - inv H0; eauto.
  - inv H0; eauto.
  - inv H0; eauto.
    inv H4; eapply IHbstep2; eauto. eapply substitution; eauto.
Qed.

Reserved Notation "'[' Γ '|-' e ':' τ ']_' n" (at level 81, Γ at next level, e at next level, τ at next level, n at next level).
Inductive checkn : nat -> Gamma -> expr -> ty -> Prop :=
| Tn_var : forall (n : nat) (Γ : Gamma) (x : vart) (τ : ty),
    nodupinv Γ ->
    Min Γ x τ ->
    [Γ |- Xvar x : τ]_ n
| Tn_abort : forall (n : nat) (Γ : Gamma) (τ : ty),
    [Γ |- Xabort : τ]_ n
| Tn_vnat : forall (n0 : nat) (Γ : Gamma) (n : nat),
    [Γ |- Xval(Vnat n) : Tℕ]_ n0
| Tn_vbool : forall (n : nat) (Γ : Gamma) (b : bool),
    [Γ |- Xval(Vbool b) : Tbool]_ n
| Tn_let : forall (n : nat) (Γ : Gamma) (x : vart) (e1 e2 : expr) (τ τ' : ty),
    nodupinv (x ↦ τ' ◘ Γ) ->
    [Γ |- e1 : τ']_ n ->
    [x ↦ τ' ◘ Γ |- e2 : τ]_ n ->
    [Γ |- Xlet x e1 e2 : τ]_ (1 + n)
| Tn_binop : forall (n : nat) (Γ : Gamma) (b : binopsymb) (e1 e2 : expr),
    b <> Bless ->
    [Γ |- e1 : Tℕ]_ n ->
    [Γ |- e2 : Tℕ]_ n ->
    [Γ |- Xbinop b e1 e2 : Tℕ]_ (1 + n)
| Tn_binop_less : forall (n : nat) (Γ : Gamma) (e1 e2 : expr),
    [Γ |- e1 : Tℕ]_ n ->
    [Γ |- e2 : Tℕ]_ n ->
    [Γ |- Xbinop Bless e1 e2 : Tbool]_ (1 + n)
| Tn_if : forall (n : nat) (Γ : Gamma) (e0 e1 e2 : expr) (τ : ty),
    [Γ |- e0 : Tbool]_ n ->
    [Γ |- e1 : τ]_ n ->
    [Γ |- e2 : τ]_ n ->
    [Γ |- Xif e0 e1 e2 : τ]_ (1 + n)
where "'[' Γ '|-' e ':' τ ']_' n" := (checkn n Γ e τ)
.
#[local] Hint Constructors checkn : core.

Lemma checkn_sound (Γ : Gamma) (e : expr) (τ : ty) (n : nat) :
  [Γ |- e : τ]_ n ->
  [Γ |- e : τ] 
.
Proof.
  intros H; dependent induction H; eauto.
Qed.

Fixpoint check_height (e : expr) : nat :=
  match e with
  | Xvar _ => 1
  | Xval _ => 1
  | Xbinop _ e1 e2 => 1 + check_height e1 + check_height e2
  | Xabort => 1
  | Xlet x e1 e2 => 1 + check_height e1 + check_height e2
  | Xif e1 e2 e3 => 1 + check_height e1 + check_height e2 + check_height e3
  end
.
Lemma checkn_weaken (Γ : Gamma) (e : expr) (τ : ty) (n n0 : nat) :
  [Γ |- e : τ]_ n ->
  [Γ |- e : τ]_ (n + n0)
.
Proof.
  revert n0 Γ e τ; induction n; intros; cbn; inv H; eauto.
Qed.
Local Hint Resolve checkn_weaken : core.
Lemma checkn_correct (Γ : Gamma) (e : expr) (τ : ty) :
  [Γ |- e : τ] ->
  [Γ |- e : τ]_ (check_height e)
.
Proof.
  revert Γ τ; induction e; cbn; intros; eauto.
  - inv H; constructor.
  - inv H; now constructor.
  - inv H. constructor; eauto. rewrite Nat.add_comm.
    eapply checkn_weaken; eauto.
    constructor; eauto. rewrite Nat.add_comm.
    eapply checkn_weaken; eauto.
  - inv H. constructor; eauto. eapply checkn_weaken.
    rewrite Nat.add_comm; eapply checkn_weaken; eauto.
    rewrite Nat.add_comm. eapply checkn_weaken; eauto.
  - inv H. econstructor; eauto. rewrite Nat.add_comm; eauto.
Qed.

Lemma check_equiv_checkn (Γ : Gamma) (e : expr) (τ : ty) :
  [Γ |- e : τ] <->
  exists n, [Γ |- e : τ]_ n
.
Proof.
  split; intros; deex.
  eauto using checkn_correct, checkn_sound.
  eauto using checkn_correct, checkn_sound.
Qed.
Lemma swap_typs'_n (Γ1 Γ2 : Gamma) (e : expr) (τ : ty) (n : nat) :
  meq Γ1 Γ2 -> 
  [Γ1 |- e : τ]_ n ->
  [Γ2 |- e : τ]_ n
.
Proof.
  intros H%meq_sound; subst; easy.
Qed.
Lemma swap_typs''_n (x x0 : vart) (τ1 τ2 τ' : ty) (Γ : Gamma) (e : expr) (n : nat) :
  (x <> x0) ->
  [(x ↦ τ1 ◘ (x0 ↦ τ' ◘ Γ)) |- e : τ2 ]_ n ->
  [(x0 ↦ τ' ◘ (x ↦ τ1 ◘ Γ)) |- e : τ2 ]_ n
.
Proof.
  intros. eapply swap_typs'_n in H0; try eassumption.
  clear. split; intros y τ H.
  destruct H as [[Ha Hb]|H]; subst. right; now left.
  destruct H as [[Ha Hb]|H]; subst. now left.
  right; now right.
  destruct H as [[Ha Hb]|H]; subst. right; now left.
  destruct H as [[Ha Hb]|H]; subst. now left.
  right; now right.
Qed.
Lemma swap_typs_n (x x0 : vart) (τ1 τ2 τ' : ty) (Γ : Gamma) (e : expr) (n : nat) :
  (x <> x0) ->
  [(x ↦ τ1 ◘ (x0 ↦ τ' ◘ Γ)) |- e : τ2 ]_n <->
  [(x0 ↦ τ' ◘ (x ↦ τ1 ◘ Γ)) |- e : τ2 ]_n
.
Proof.
  split; eapply swap_typs''_n; eauto.
Qed.
Lemma substitution_n Γ v τ1 τ2 x e n n':
  nodupinv Γ ->
  [[⋅] |- Xval v : τ1 ]_ n' ->
  [(x ↦ τ1 ◘ Γ) |- e : τ2 ]_ n ->
  [Γ |- subst x e (Xval v) : τ2]_ n
.
Proof.
  revert τ1 τ2 Γ n n'; induction e; intros; eauto.
  - inv H1; constructor.
  - cbn; destruct (eq_dec x x0); eq_to_defeq vareq.
    + subst; rewrite eq_refl. inv H1. inv H3. destruct H6 as [[]|]; try congruence; subst.
      inv H0; constructor.
      now apply Min_in in H1 as [].
    + apply neqb_neq in H2 as H2'; rewrite H2'. inv H1. inv H4. destruct H7 as [[]|]; try congruence.
      now constructor.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v)); inv H1; econstructor; eauto.
  - cbn; inv H1; constructor; eauto.
  - cbn; fold (subst x e1 (Xval v)); fold (subst x e2 (Xval v)). 
    destruct (eq_dec x x0); eq_to_defeq vareq.
    + subst. rewrite eq_refl.
      inv H1. inv H7. exfalso. apply H3; now left.
    + apply neqb_neq in H2 as H2'; rewrite H2'.
      inv H1.
      change (x0 ↦ τ' ◘ (x ↦ τ1 ◘ Γ)) with ((x0 ↦ τ' ◘ mapNil _ _) ◘ (x ↦ τ1 ◘ Γ)) in H8.
      apply nodupinv_swap in H8; inv H8; apply nodupinv_swap in H6; cbn in H6.
      econstructor; eauto.
      eapply IHe2; eauto. eapply swap_typs_n; eauto.
Qed.
Lemma types_bstep' e τ n :
  [[⋅] |- e : τ]_n ->
  exists r, ▷ e ⇓ r
.
Proof.
  revert e τ ; induction n; intros.
  - inv H; eauto; try now inv H0.
    + exists (▷↯); constructor.
    + exists (▷ n); constructor. 
    + exists (▷ b); constructor.
  - inv H; eauto; try now inv H0. Unshelve. 5: exact τ.
    + specialize (IHn e1 τ' H2) as IHe1; deex.
      destruct r as [v1|].
      * apply bstep_yields_nf in IHe1 as IHe1'.
        destruct IHe1' as [IHe1'|[v' IHe1']]; inv IHe1'; rename v' into v1.

        apply checkn_sound in H2 as H2', H3 as H3'.
        eapply preserve_bstep in H2' as H2''; eauto.
        specialize (checkn_correct [⋅] (Xval v1) τ' H2''); intros H2'''. 
        inv H1; eapply substitution_n in H3; eauto.
        specialize (IHn (subst x e2 v1) τ H3) as IHe2; deex.
        destruct r as [v2|].
        -- exists (▷ v2). apply bstep_yields_nf in IHe2 as IHe2'.
           destruct IHe2' as [IHe2'|[v' IHe2']]; inv IHe2'; rename v' into v2.
           econstructor; eauto.
        -- exists (▷↯). eapply BletCrash_asgn; eauto.
      * exists (▷↯). eapply BletCrash_def; eauto.
    + specialize (IHn e1 Tℕ H2) as IHe1; deex;
      specialize (IHn e2 Tℕ H3) as IHe2; deex.
      apply bstep_yields_nf in IHe1 as IHe1'; apply bstep_yields_nf in IHe2 as IHe2'.
      destruct IHe1' as [IHe1'|[v' IHe1']]; inv IHe1'; try rename v' into v1;
      destruct IHe2' as [IHe2'|[v' IHe2']]; inv IHe2'; try rename v' into v2.
      * exists (▷↯); eauto using BopCrashL.
      * exists (▷↯); eauto using BopCrashL.
      * exists (▷↯); 
        eapply checkn_sound in H3 as H3', H2 as H2'.
        eapply preserve_bstep in H2'; eauto.
        inv H2'.
        eapply BopCrashR; eauto.
      * eapply checkn_sound in H2 as H2', H3 as H3'.
        eapply preserve_bstep in H2', H3'; eauto.
        inv H2'; inv H3'.
        assert (exists v, Some v = eval_binop b n0 n1) by (destruct b; cbn; unfold eval_binop; eauto).
        deex. 
        exists (▷ v). econstructor; eauto.
    + specialize (IHn e1 Tℕ H1) as IHe1; deex;
      specialize (IHn e2 Tℕ H2) as IHe2; deex.
      apply bstep_yields_nf in IHe1 as IHe1'; apply bstep_yields_nf in IHe2 as IHe2'.
      destruct IHe1' as [IHe1'|[v' IHe1']]; inv IHe1'; try rename v' into v1;
      destruct IHe2' as [IHe2'|[v' IHe2']]; inv IHe2'; try rename v' into v2.
      * exists (▷↯); eauto using BopCrashL.
      * exists (▷↯); eauto using BopCrashL.
      * exists (▷↯); 
        eapply checkn_sound in H1 as H1'; eauto.
        eapply preserve_bstep in H1'; eauto.
        inv H1'.
        eapply BopCrashR; eauto.
      * eapply checkn_sound in H1 as H1',H2 as H2'. 
        eapply preserve_bstep in H1', H2'; eauto.
        inv H1'; inv H2'.
        assert (exists v, Some v = eval_binop Bless n0 n1) by (cbn; unfold eval_binop; eauto).
        deex. 
        exists (▷ v). econstructor; eauto.
    + specialize (IHn e0 'T𝔹 H1) as IHe0; deex.
      apply bstep_yields_nf in IHe0 as IHe0'.
      destruct IHe0' as [IHe0'|[v' IHe0']]; inv IHe0'; try rename v' into v0.
      * exists (▷↯); eauto using BifCrash.
      * eapply checkn_sound in H1 as H1'.
        eapply preserve_bstep in H1'; eauto.
        inv H1'.
        destruct b.
        -- specialize (IHn e1 τ H2) as IHe1; deex.
           apply bstep_yields_nf in IHe1 as IHe1'.
           destruct IHe1' as [IHe1'|[v' IHe1']]; inv IHe1'; try rename v' into v1.
           ++ exists (▷↯). eauto using BifCrash_true.
           ++ exists (▷ v1). eauto using Bif_true.
        -- specialize (IHn e2 τ H3) as IHe2; deex.
           apply bstep_yields_nf in IHe2 as IHe2'.
           destruct IHe2' as [IHe2'|[v' IHe2']]; inv IHe2'; try rename v' into v2.
           ++ exists (▷↯). eauto using BifCrash_false.
           ++ exists (▷ v2). eauto using Bif_false.
  Unshelve.
Qed.
Lemma types_bstep e τ :
  [[⋅] |- e : τ] ->
  exists r, ▷ e ⇓ r
.
Proof.
  intros H. specialize (checkn_correct [⋅] e τ H); intros.
  eauto using types_bstep'.
Qed.

Lemma types_steps e τ :
  [[⋅] |- e : τ] ->
  exists r As, (▷ e ==[As]==>(ectx)* r) /\ (rtexpr_is_final r)
.
Proof.
  intros H%types_bstep; deex; exists r.
  assert (H0:=H). apply bstep_yields_nf in H as [H' | H']; deex.
  - inv H'; eapply bigstep_equiv_starstep_crash in H0.
    exists (Scrash :: nil). split; auto; constructor.
  - inv H'; clear H. eapply bigstep_equiv_starstep_value in H0. exists nil. split; auto; constructor.
Qed.
(** Main result: well-typed programs do not get stuck, they either zap to a value or crash. *)
Theorem type_safety e τ :
  [[⋅] |- e : τ] ->
  (exists (v : value), (▷ e ==[]==>(ectx)* ▷ Xval v))
\/ (▷ e ==[Scrash :: nil]==>(ectx)* ▷↯)
.
Proof.
  intros H; assert (H':=H); apply progress in H; destruct H; deex.
  - inv H. left. exists v. constructor.
  - assert (H'':=H). eapply types_steps in H' as H'''; deex.
    destruct H''' as [Ha Hb].
    inv Hb.
    + enough (As0 = nil) as ->.
      left; exists v; easy. clear H H'' H'; dependent induction Ha; try easy. inv H. inv H3. inv Ha. inv H. inv H.
      inv H; eauto.
    + enough (As0 = Scrash :: nil) as ->.
      right; easy. clear H H' H''. dependent induction Ha. inv H. inv H3. inv Ha; trivial. inv H. inv H. inv H; eauto.
Qed.
