(** Merely re-exports modules under unified name `S`. *)

Require ToadML.S.syntax ToadML.S.static ToadML.S.dynamic ToadML.S.toplevel.
Include ToadML.S.syntax.
Include ToadML.S.static.
Include ToadML.S.dynamic.
Include ToadML.S.toplevel.


Require Import String.
Require Import ToadML.S.parser ToadML.S.lexer ToadML.Util.Convenience ToadML.Shared.Sema.
Import MenhirLibParser.Inter.

(** read 2^50 chars and try to parse them *)
Definition string2expr s : option ToadML.S.syntax.expr :=
  match option_map (parser.top_expr 50) (lexer.lex_string s) with
  | Some (Parsed_pr f _) => Some f
  | _ => None
  end.

(** Simple pretty printer *)
Fixpoint string_of_expr (e : expr) :=
  match e with
  | Xval(Vnat n) => string_of_nat n 
  | Xval(Vbool b) => if b then "true" else "false"
  | Xvar x => x
  | Xbinop b e0 e1 => "(" ++ string_of_expr e0 ++ ") "
      ++ string_of_symb b ++ " (" ++ string_of_expr e1 ++ ")"
  | Xif e0 e1 e2 => "if " ++ string_of_expr e0 ++ " then "
      ++ string_of_expr e1 ++ " else " ++ string_of_expr e2
  | Xlet x e0 e1 => "let " ++ x ++ " := " ++ string_of_expr e0
      ++ " in " ++ string_of_expr e1
  | Xabort => "abort"
  end%string
.
Definition expr2string (e : ToadML.S.syntax.expr) : String.string := string_of_expr e.

Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.

From Coq Require Extraction.

Extraction Language OCaml.

Extract Inductive unit => "unit" [ "()" ].
Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inductive list => "list" [ "[]" "(::)" ].
Extract Inductive nat => int [ "0" "succ" ] "(fun fO fS n -> if n=0 then fO () else fS (n-1))".
Extract Inductive option => "option" [ "Some" "None" ].

Extraction "src/S/parser.ml" string2expr expr2string.
