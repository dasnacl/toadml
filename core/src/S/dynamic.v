(** * Evaluation of S *)
Set Implicit Arguments.
Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.
Require Import ToadML.Shared.Fresh ToadML.Shared.Sema ToadML.Util.Sets ToadML.Util.HasEquality.
Require Import ToadML.Util.Util ToadML.Util.Step ToadML.Util.Convenience ToadML.Util.NoDupInv.

Require Import ToadML.S.syntax.

Open Scope string. Open Scope S.

(** Fill hole in evaluation context. *)
Fixpoint insert (K : evalctx) (withh : expr) : expr :=
  let R := fun k => insert k withh in
  match K with
  | Khole => withh
  | KbinopL b K' e => Xbinop b (R K') e
  | KbinopR b v K' => Xbinop b (Xval v) (R K')
  | Kif K' e0 e1 => Xif (R K') e0 e1
  | Klet x K' e => Xlet x (R K') e
  end
.
(** Compose two evaluation contexts with each other. *)
Fixpoint glue (K1 K2 : evalctx) :=
  let R := fun k => glue k K2 in
  match K1 with
  | Khole => K2
  | KbinopL b K' e => KbinopL b (R K') e
  | KbinopR b v K' => KbinopR b v (R K')
  | Kif K' e0 e1 => Kif (R K') e0 e1
  | Klet x K' e => Klet x (R K') e
  end
.

(** A runtime program is just an expression or a crashed state. *)
Inductive rtexpr : Type :=
| RTerm (e : expr)
| RCrash
.
#[global]
Notation "'▷' e" := ((RTerm e) : rtexpr) (at level 81, e at next level).
#[global]
Notation "'▷↯'" := (RCrash).

(** Evaluation of binary expressions. Note that 0 means `true` in S, so `5 < 42` evals to `0`. *)
Definition eval_binop (b : binopsymb) (n0 n1 : nat) : option value :=
  Some((match b with
       | Bless => (if Nat.ltb n0 n1 then Vbool true else Vbool false)
       | Badd => Vnat(n0 + n1)
       | Bdiv => Vnat(Nat.div n0 n1)
       | Bsub => Vnat(n0 - n1)
       | Bmul => Vnat(n0 * n1)
       end))
.
Variant StepTag : Type :=
| prim (* primitive steps *)
| ectx (* evaluation steps *)
.
Open Scope LangNotationsScope.
(** Primitive evaluation steps. *)
Inductive pstep : (Step prim rtexpr event) :=
| e_binop : forall (n1 n2 : nat) v (b : binopsymb),
    Some(v) = eval_binop b n1 n2 ->
    ▷ Xbinop b n1 n2 --[]-->(prim) ▷ v
| e_if_true : forall (e1 e2 : expr),
    ▷ <( if true then (e1) else (e2) )> --[]-->(prim) ▷ e1
| e_if_false : forall (e1 e2 : expr),
    ▷ <( if false then (e1) else (e2) )> --[]-->(prim) ▷ e2
| e_let : forall (x : vart) (e : expr) (v : value),
    ▷ <( let (x) := (v) in (e) )> --[]-->(prim) ▷ subst x e v
| e_abort :
    ▷ <( abort )> --[ Scrash ]-->(prim) ▷↯
.
#[global]
Existing Instance pstep.
#[global]
Hint Constructors pstep : core.
(* Contextual steps may either have a subexpression that can do a primstep or
   have a subexpression that crashes. *)
Inductive estep : (Step ectx rtexpr event) :=
| E_ectx : forall (e e' e0 e0' : expr) (o : option event) (K : evalctx),
    e0 = insert K e ->
    e0' = insert K e' ->
    (▷ e --[, o ]-->(prim) ▷ e') ->
    ▷ e0 --[, o ]-->(ectx) ▷ e0'
| E_stuck : forall (e e0 : expr) (K : evalctx),
    e0 = insert K e ->
    ▷ e --[ Scrash ]-->(prim) ▷↯ ->
    ▷ e0 --[ Scrash ]-->(ectx) ▷↯
.
#[global]
Existing Instance estep.
#[global]
Hint Constructors estep : core.

(** Some lemmas for contexts. *)
Lemma insert_glue (K1 K2 : evalctx) (e : expr) :
  insert K1 (insert K2 e) = insert (glue K1 K2) e
.
Proof. induction K1; cbn; congruence. Qed.
Lemma insert_estep (K : evalctx) (e1 e2 : expr) (a : option event) :
  (▷ e1 --[, a ]-->(ectx) ▷ e2) ->
  ▷ (insert K e1) --[, a ]-->(ectx) ▷ (insert K e2)
.
Proof.
  intros H; cbv in H; dependent destruction H.
  rewrite ! insert_glue. econstructor; easy.
Qed.
Lemma insert_estep_crash (K : evalctx) (e1 : expr) (a : option event) :
  (▷ e1 --[, a ]-->(ectx) ▷↯) ->
  (▷ (insert K e1) --[, a ]-->(ectx) ▷↯)
.
Proof.
  intros H; cbv in H; dependent destruction H.
  rewrite ! insert_glue. econstructor; easy.
Qed.

(** A runtime expression is classified as value if the associated state is also freed completely. *)
Inductive rtexpr_is_final : rtexpr -> Prop :=
| CRTval : forall (v : value),
    rtexpr_is_final (▷ (Xval v))
| CRTfail : rtexpr_is_final (▷↯)
.
#[export]
Instance S__LangParams : @LangParams event eventeq__Instance StepTag := {
  State := rtexpr ;
  step := estep ;
  is_value := rtexpr_is_final ;
}.

(** Some star_step related lemmas. *)
Lemma fill_star_step_estep {e1 e2 : expr} {As : tracepref} (K : evalctx) :
  (▷ e1 ==[ As ]==>(ectx)* ▷ e2) ->
  ▷ (insert K e1) ==[ As ]==>(ectx)* ▷ (insert K e2).
Proof.
  intros H; dependent induction H; eauto using ES_refl.
  - destruct r2. eapply ES_trans_important; try apply IHstar_step.
    eapply insert_estep. eassumption. 1,2: reflexivity.
    inv H0; inv H1.
  - destruct r2. eapply ES_trans_unimportant; try apply IHstar_step.
    eapply insert_estep. eassumption. 1,2: reflexivity.
    inv H0; inv H1.
Qed.
Lemma fill_star_step_estep_crash {e1 : expr} {As : tracepref} (K : evalctx) :
  ▷ e1 ==[ As ]==>(ectx)* ▷↯ ->
  ▷ (insert K e1) ==[ As ]==>(ectx)* ▷↯.
Proof.
  intros H; dependent induction H; eauto using ES_refl.
  - destruct r2. eapply ES_trans_important; try apply IHstar_step.
    eapply insert_estep. eassumption. 1,2: reflexivity.
    inv H0. eapply ES_trans_important. eapply insert_estep_crash. easy. constructor.
    all: inv H1.
  - destruct r2. eapply ES_trans_unimportant; try apply IHstar_step.
    eapply insert_estep. eassumption. 1,2: reflexivity.
    inv H0. eapply ES_trans_unimportant. eapply insert_estep_crash. easy. constructor.
    all: inv H1.
Qed.
Lemma star_step_nf_empty_trace (e : expr) (As : tracepref) (v : value) :
  (▷ e ==[ As ]==>(ectx)* ▷ (Xval v)) ->
  As = nil
.
Proof.
  intros H; cbv in H; dependent induction H; eauto.
  inv H. inv H4. inv H4. inv H0. inv H. inv H.
  eauto. destruct r2; eauto. inv H.
Qed.
Lemma star_step_nf_some_trace (e : expr) (As : tracepref) :
  (▷ e ==[ As ]==>(ectx)* ▷↯) ->
  As = Scrash :: nil
.
Proof.
  intros H; cbv in H; dependent induction H; eauto.
  inv H. inv H4. f_equal. inv H0. reflexivity. inv H. inv H.
  destruct r2; eauto. inv H. 
Qed.

(* Unfortunately, the split lemmas are tricky. We define big-step evaluation to make them go through. *)
Reserved Notation "r1 '⇓' r2" (at level 91, r2 at next level).
Inductive bstep : rtexpr -> rtexpr -> Prop :=
| Bcrash : ▷↯ ⇓ ▷↯
| Bval : forall (v : value), ▷ Xval v ⇓ ▷ Xval v
| Bop : forall (e1 e2 : expr) (n1 n2 : nat) (symb : binopsymb) (v : value),
    Some v = eval_binop symb n1 n2 ->
    ▷ e1 ⇓ ▷ Xval n1 ->
    ▷ e2 ⇓ ▷ Xval n2 ->
    ▷ Xbinop symb e1 e2 ⇓ ▷ Xval v
| BopCrashL : forall (e1 e2 : expr) (symb : binopsymb),
    ▷ e1 ⇓ ▷↯ ->
    ▷ Xbinop symb e1 e2 ⇓ ▷↯
| BopCrashR : forall (e1 e2 : expr) (n1 : nat) (symb : binopsymb),
    ▷ e1 ⇓ ▷ Xval n1 ->
    ▷ e2 ⇓ ▷↯ ->
    ▷ Xbinop symb e1 e2 ⇓ ▷↯
| Bif_true : forall (e0 e1 e2 : expr) (v : value),
    ▷ e0 ⇓ ▷ Xval true ->
    ▷ e1 ⇓ ▷ Xval v ->
    ▷ Xif e0 e1 e2 ⇓ ▷ Xval v
| Bif_false : forall (e0 e1 e2 : expr) (v : value),
    ▷ e0 ⇓ ▷ Xval false ->
    ▷ e2 ⇓ ▷ Xval v ->
    ▷ Xif e0 e1 e2 ⇓ ▷ Xval v
| BifCrash : forall (e0 e1 e2 : expr),
    ▷ e0 ⇓ ▷↯ ->
    ▷ Xif e0 e1 e2 ⇓ ▷↯
| BifCrash_true : forall (e0 e1 e2 : expr),
    ▷ e0 ⇓ ▷ Xval true ->
    ▷ e1 ⇓ ▷↯ ->
    ▷ Xif e0 e1 e2 ⇓ ▷↯
| BifCrash_false : forall (e0 e1 e2 : expr),
    ▷ e0 ⇓ ▷ Xval false ->
    ▷ e2 ⇓ ▷↯ ->
    ▷ Xif e0 e1 e2 ⇓ ▷↯
| Blet : forall (x : vart) (e0 e1 : expr) (v0 v1 : value),
    ▷ e0 ⇓ ▷ Xval v0 ->
    ▷ subst x e1 v0 ⇓ ▷ Xval v1 ->
    ▷ Xlet x e0 e1 ⇓ ▷ Xval v1
| BletCrash_def : forall (x : vart) (e0 e1 : expr),
    ▷ e0 ⇓ ▷↯ ->
    ▷ Xlet x e0 e1 ⇓ ▷↯
| BletCrash_asgn : forall (x : vart) (e0 e1 : expr) (v : value),
    ▷ e0 ⇓ ▷ Xval v ->
    ▷ subst x e1 v ⇓ ▷↯ ->
    ▷ Xlet x e0 e1 ⇓ ▷↯
| Babort : ▷ Xabort ⇓ ▷↯
where "r1 '⇓' r2" := (bstep r1 r2)
.
#[local] Hint Constructors bstep : core.
(** Lemmas related to bstep *)
Lemma bstep_yields_nf (e : expr) (r : rtexpr) :
  ▷ e ⇓ r ->
  r = RCrash \/ exists (v : value), r = ▷ Xval v
.
Proof. intros H; dependent induction H; eauto. Qed.
Lemma unique_nf_bstep (e : expr) (r r' : rtexpr) :
  ▷ e ⇓ r ->
  ▷ e ⇓ r' -> 
  r = r'
.
Proof.
  intros H; revert r'; dependent induction H; intros.
  - now inv H. 
  - inv H2.
    + specialize (IHbstep1 e1 Logic.eq_refl (▷ Xval n0) H8).
      specialize (IHbstep2 e2 Logic.eq_refl (▷ Xval n3) H9).
      inv IHbstep1; inv IHbstep2. inv H; inv H6; reflexivity.
    + now specialize (IHbstep1 e1 Logic.eq_refl (▷↯) H7).
    + now specialize (IHbstep2 e2 Logic.eq_refl (▷↯) H8).
  - inv H0; try reflexivity. 
    now specialize (IHbstep e1 Logic.eq_refl (▷ Xval n1) H6).
  - inv H1; try reflexivity. 
    now specialize (IHbstep2 e2 Logic.eq_refl (▷ Xval n2) H8).
  - inv H1.
    + specialize (IHbstep1 e0 Logic.eq_refl (▷ Xval true) H6).
      specialize (IHbstep2 e1 Logic.eq_refl (▷ Xval v0) H7).
      inv IHbstep1; inv IHbstep2; reflexivity.
    + specialize (IHbstep1 e0 Logic.eq_refl (▷ Xval false) H6).
      inv IHbstep1.
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷↯) H6).
    + now specialize (IHbstep2 e1 Logic.eq_refl (▷↯) H7).
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷ (Xval false)) H6).
  - inv H1.
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷ (Xval true)) H6).
    + specialize (IHbstep1 e0 Logic.eq_refl (▷ Xval false) H6).
      specialize (IHbstep2 e2 Logic.eq_refl (▷ Xval v0) H7).
      inv IHbstep1; inv IHbstep2; reflexivity.
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷↯) H6).
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷ (Xval true)) H6).
    + now specialize (IHbstep2 e2 Logic.eq_refl (▷↯) H7).
  - inv H0; try reflexivity.
    + now specialize (IHbstep e0 Logic.eq_refl (▷ (Xval true)) H5).
    + now specialize (IHbstep e0 Logic.eq_refl (▷ (Xval false)) H5).
  - inv H1; try reflexivity.
    + now specialize (IHbstep2 e1 Logic.eq_refl (▷ Xval v) H7).
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷ (Xval false)) H6).
  - inv H1; try reflexivity.
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷ (Xval true)) H6).
    + now specialize (IHbstep2 e2 Logic.eq_refl (▷ Xval v) H7).
  - inv H1; eauto.
    + eapply IHbstep2; eauto.
      specialize (IHbstep1 e0 Logic.eq_refl (▷ Xval v2) H6);
      now inv IHbstep1.
    + now specialize (IHbstep1 e0 Logic.eq_refl (▷↯) H6).
    + eapply IHbstep2; eauto.
      specialize (IHbstep1 e0 Logic.eq_refl (▷ Xval v) H6);
      now inv IHbstep1.
  - inv H0; eauto.
    now specialize (IHbstep e0 Logic.eq_refl (▷ Xval v0) H5).
  - inv H1; eauto.
    eapply IHbstep2; eauto.
    specialize (IHbstep1 e0 Logic.eq_refl (▷ Xval v0) H6);
    now inv IHbstep1.
  - now inv H. 
Qed.

(** We want the following lemmas but for star_step... *)
Lemma big_binop_exec_split (symb : binopsymb) (e1 e2 : expr) (v : value) :
  ▷ Xbinop symb e1 e2 ⇓ ▷ Xval v ->
  exists (n1 n2 : nat), (▷ e1 ⇓ ▷ Xval n1) /\
                 (▷ e2 ⇓ ▷ Xval n2) /\
                 Some v = eval_binop symb n1 n2
.
Proof. intros H; dependent induction H; eauto. Qed.
Lemma big_binop_exec_split_crash (symb : binopsymb) (e1 e2 : expr) :
  ▷ Xbinop symb e1 e2 ⇓ ▷↯ ->
  (▷ e1 ⇓ ▷↯) \/
  exists (n1 : nat), (▷ e1 ⇓ ▷ Xval n1) 
                  /\ (▷ e2 ⇓ ▷↯)
.
Proof. intros H; dependent induction H; eauto. Qed.
Lemma big_if_exec_split (e0 e1 e2 : expr) (v : value) :
  ▷ Xif e0 e1 e2 ⇓ ▷ Xval v ->
  ((▷ e0 ⇓ ▷ Xval true) /\ (▷ e1 ⇓ ▷ Xval v)
) \/ ((▷ e0 ⇓ ▷ Xval false) /\ (▷ e2 ⇓ ▷ Xval v))
.
Proof. intros H; dependent induction H; eauto. Qed.
Lemma big_if_exec_split_crash (e0 e1 e2 : expr) :
  ▷ Xif e0 e1 e2 ⇓ ▷↯ ->
  (▷ e0 ⇓ ▷↯
) \/ ((▷ e0 ⇓ ▷ Xval true) /\ (▷ e1 ⇓ ▷↯)
) \/ ((▷ e0 ⇓ ▷ Xval false) /\ (▷ e2 ⇓ ▷↯))
.
Proof. intros H; dependent induction H; eauto. Qed.
Lemma big_let_exec_split (x : vart) (e1 e2 : expr) (v : value) :
  ▷ Xlet x e1 e2 ⇓ ▷ Xval v ->
  exists (v1 : value), (▷ e1 ⇓ ▷ Xval v1) /\
                 (▷ subst x e2 v1 ⇓ ▷ Xval v)
.
Proof. intros H; dependent induction H; eauto. Qed.
Lemma big_let_exec_split_crash (x : vart) (e1 e2 : expr) :
  ▷ Xlet x e1 e2 ⇓ ▷↯ ->
  (▷ e1 ⇓ ▷↯) \/
  (exists (v : value), (▷ e1 ⇓ ▷ Xval v) /\
                       (▷ subst x e2 v ⇓ ▷↯))
.
Proof. intros H; dependent induction H; eauto. Qed.

#[local] Hint Resolve big_binop_exec_split  big_binop_exec_split_crash big_if_exec_split big_if_exec_split_crash big_let_exec_split big_let_exec_split_crash : core.

(** ... hence, we establish an equivalence between star step and bstep *)
Lemma bigstep_implies_steps e r :
  ▷ e ⇓ r -> exists (As : tracepref), ▷ e ==[As]==>(ectx)* r
.
Proof.
  intros H; dependent induction H.
  - exists nil. eapply ES_refl; constructor.
  - exists nil. eapply @star_step_trans_nil.
    { eapply (fill_star_step_estep (KbinopL _ Khole _)).
      specialize (IHbstep1 e1 Logic.eq_refl); deex.
      apply star_step_nf_empty_trace in IHbstep1 as IHbstep1'; subst.
      exact IHbstep1.
    }
    cbn; eapply @star_step_trans_nil.
    { eapply (fill_star_step_estep (KbinopR _ _ Khole)).
      specialize (IHbstep2 e2 Logic.eq_refl); deex.
      apply star_step_nf_empty_trace in IHbstep2 as IHbstep2'; subst.
      exact IHbstep2.
    }
    cbn; eapply ES_trans_unimportant. cbv. econstructor; cbv. instantiate (2:=Khole). now cbn. instantiate (1:=Xval v).
    now cbn. cbv. eauto. constructor. unfold eval_binop in *; inv H; reflexivity. constructor.
  - exists (Scrash :: nil). specialize (IHbstep e1 Logic.eq_refl); deex.
    apply star_step_nf_some_trace in IHbstep as IHbstep'; subst.
    inv IHbstep. inv H3. inv H4. inv H2.
    eapply ES_trans_important. 2: econstructor. econstructor 2.
    instantiate (2:=KbinopL symb K e2). now cbn. constructor.
    inv H0. econstructor 3. econstructor.
    instantiate (2:=KbinopL symb K e2). now cbn. now cbn. eassumption.
    change (Xbinop symb (insert K e') e2) with (insert (KbinopL symb Khole e2) (insert K e')).
    now eapply fill_star_step_estep_crash.
  - exists (Scrash :: nil). specialize (IHbstep1 e1 Logic.eq_refl); deex.
    specialize (IHbstep2 e2 Logic.eq_refl); deex.
    apply star_step_nf_empty_trace in IHbstep1 as IHbstep1';
    apply star_step_nf_some_trace in IHbstep2 as IHbstep2'; subst.
    change (Scrash :: nil) with (nil ++ Scrash :: nil)%list.
    eapply star_step_trans. instantiate (1:=RTerm(Xbinop symb (Xval n1) e2)).
    change (Xbinop symb e1 e2) with (insert (KbinopL symb Khole e2) e1).
    change (Xbinop symb (Xval n1) e2) with (insert (KbinopL symb Khole e2) (Xval n1)).
    eapply fill_star_step_estep. eassumption.
    change (Xbinop symb (Xval n1) e2) with (insert (KbinopR symb n1 Khole) e2).
    eapply fill_star_step_estep_crash. eassumption.
  - exists nil. eapply @star_step_trans_nil.
    { eapply (fill_star_step_estep (Kif Khole _ _)).
      specialize (IHbstep1 e0 Logic.eq_refl); deex.
      apply star_step_nf_empty_trace in IHbstep1 as IHbstep1'; subst.
      exact IHbstep1.
    }
    cbn; eapply ES_trans_unimportant. econstructor. instantiate (2:=Khole). now cbn. instantiate (1:=e1).
    now cbn. econstructor.
    specialize (IHbstep2 e1 Logic.eq_refl); deex.
    now eapply star_step_nf_empty_trace in IHbstep2 as IHbstep2'; subst.
  - exists nil. eapply @star_step_trans_nil.
    { eapply (fill_star_step_estep (Kif Khole _ _)).
      specialize (IHbstep1 e0 Logic.eq_refl); deex.
      apply star_step_nf_empty_trace in IHbstep1 as IHbstep1'; subst.
      exact IHbstep1.
    }
    cbn; eapply ES_trans_unimportant. econstructor. instantiate (2:=Khole). now cbn. instantiate (1:=e2).
    now cbn. econstructor.
    specialize (IHbstep2 e2 Logic.eq_refl); deex.
    now eapply star_step_nf_empty_trace in IHbstep2 as IHbstep2'; subst.
  - specialize (IHbstep e0 Logic.eq_refl); deex.
    change (Xif e0 e1 e2) with (insert (Kif Khole e1 e2) e0).
    exists As. now eapply fill_star_step_estep_crash.
  - specialize (IHbstep1 e0 Logic.eq_refl); deex.
    specialize (IHbstep2 e1 Logic.eq_refl); deex.
    exists As0. change (As0) with (nil ++ As0)%list.
    eapply star_step_trans. instantiate (1:=▷ Xif (Xval true) e1 e2).
    change (Xif e0 e1 e2) with (insert (Kif Khole e1 e2) e0).
    change (Xif (Xval true) e1 e2) with (insert (Kif Khole e1 e2) (Xval true)).
    eapply fill_star_step_estep. 
    apply star_step_nf_empty_trace in IHbstep1 as IHbstep1'; subst.
    assumption.
    econstructor 3. econstructor. instantiate (2:=Khole). now cbn. now cbn. 
    econstructor. assumption.
  - specialize (IHbstep1 e0 Logic.eq_refl); deex.
    specialize (IHbstep2 e2 Logic.eq_refl); deex.
    exists As0. change (As0) with (nil ++ As0)%list.
    eapply star_step_trans. instantiate (1:=▷ Xif (Xval false) e1 e2).
    change (Xif e0 e1 e2) with (insert (Kif Khole e1 e2) e0).
    change (Xif (Xval false) e1 e2) with (insert (Kif Khole e1 e2) (Xval false)).
    eapply fill_star_step_estep. 
    apply star_step_nf_empty_trace in IHbstep1 as IHbstep1'; subst.
    eassumption.
    econstructor 3. econstructor. instantiate (2:=Khole). now cbn. now cbn. 
    econstructor. assumption.
  - specialize (IHbstep1 e0 Logic.eq_refl); deex.
    specialize (IHbstep2 (subst x e1 v0) Logic.eq_refl); deex.
    exists (As ++ As0)%list.
    eapply star_step_trans. instantiate (1:=▷ Xlet x v0 e1).
    change (Xlet x e0 e1) with (insert (Klet x Khole e1) (e0)).
    change (Xlet x v0 e1) with (insert (Klet x Khole e1) (v0)).
    eauto using fill_star_step_estep.
    change (Xlet x v0 e1) with (insert (Khole) (Xlet x v0 e1)).
    change (Xval v1) with (insert (Khole) (v1)).
    econstructor. repeat econstructor.
    now cbn.
  - specialize (IHbstep e0 Logic.eq_refl); deex.
    exists As.
    change (Xlet x e0 e1) with (insert (Klet x Khole e1) (e0)).
    eauto using fill_star_step_estep_crash.
  - specialize (IHbstep1 e0 Logic.eq_refl); deex.
    specialize (IHbstep2 (subst x e1 v) Logic.eq_refl); deex.
    exists (As ++ nil ++ As0)%list.
    eapply star_step_trans. instantiate (1:=▷ Xlet x v e1).
    change (Xlet x e0 e1) with (insert (Klet x Khole e1) (e0)).
    change (Xlet x v e1) with (insert (Klet x Khole e1) (v)).
    eauto using fill_star_step_estep.
    eapply star_step_trans. instantiate (1:=▷ subst x e1 v).
    change (Xlet x v e1) with (insert (Khole) (Xlet x v e1)).
    change (subst x e1 v) with (insert (Khole) (subst x e1 v)).
    econstructor. repeat econstructor. econstructor.
    assumption.
  - exists (Scrash :: nil). econstructor. econstructor 2. instantiate (2:=Khole). now cbn.
    constructor. econstructor.
Qed.
Lemma step_implies_bigstep (e e' : expr) (v : value) :
  (▷ e --[]-->(ectx) ▷ e') ->
  ▷ e' ⇓ ▷ Xval v ->
  ▷ e ⇓ ▷ Xval v
.
Proof.
  intros H; inv H. cbv in H4; dependent induction H4; intros.
  - revert v0 v H H0; induction K; cbn; intros; try now (inv H0; econstructor; eauto).
  - revert e'0 v H; induction K; cbn; intros; try now (inv H; econstructor; eauto).
  - revert e'0 v H; induction K; cbn; intros; try now (inv H; econstructor; eauto).
  - revert e v0 v H; induction K; cbn; intros; try now (inv H; econstructor; eauto).
    econstructor; eauto.
Qed.
Lemma step_implies_bigstep_fail (e e' : expr) :
  (▷ e --[]-->(ectx) ▷ e') ->
  ▷ e' ⇓ ▷↯ ->
  ▷ e ⇓ ▷↯
.
Proof.
  intros H; inv H. cbv in H4; dependent induction H4; intros; eauto.
  - induction K; cbn in H0; inv H0; cbn; eauto.  
    + eapply BopCrashR; eauto. instantiate (1:=n0).
      eapply step_implies_bigstep; eauto. 
    + eapply BifCrash_true; eauto. eapply step_implies_bigstep; eauto.
    + eapply BifCrash_false; eauto. eapply step_implies_bigstep; eauto.
    + eapply BletCrash_asgn; eauto. eapply step_implies_bigstep; eauto.
  - induction K; cbn in *; inv H; eauto.
    + eapply BopCrashR; eauto.
      instantiate (1:=n1); eapply step_implies_bigstep; eauto.
    + eauto using BifCrash_true, step_implies_bigstep.
    + eauto using BifCrash_false, step_implies_bigstep.
    + eauto using BletCrash_asgn, step_implies_bigstep.
  - induction K; cbn in *; inv H; eauto.
    + eapply BopCrashR; eauto.
      instantiate (1:=n1); eapply step_implies_bigstep; eauto.
    + eauto using BifCrash_true, step_implies_bigstep.
    + eauto using BifCrash_false, step_implies_bigstep.
    + eauto using BletCrash_asgn, step_implies_bigstep.
  - induction K; cbn in *; eauto; inv H; eauto.
    + eauto using BopCrashL, BopCrashR, step_implies_bigstep.
    + eauto using BopCrashL, BopCrashR, step_implies_bigstep.
    + eauto using BifCrash_true, step_implies_bigstep.
    + eauto using BifCrash_false, step_implies_bigstep.
Qed.
Lemma step_implies_bigstep_fail' (e : expr) :
  (▷ e --[ Scrash ]-->(ectx) ▷↯) ->
  ▷ e ⇓ ▷↯
.
Proof.
  intros; cbv in H; dependent induction H; inv H0.
  induction K; cbn; eauto.
Qed.
Lemma steps_implies_bigstep (e : expr) (v : value) :
  (▷ e ==[]==>(ectx)* ▷ Xval v) ->
  ▷ e ⇓ ▷ Xval v
.
Proof.
  intros. dependent induction H.
  - constructor.
  - destruct r2.
    + eapply step_implies_bigstep; eauto.
    + inv H0. inv H1.
Qed.
Lemma steps_implies_bigstep_crash (e : expr) :
  (▷ e ==[Scrash :: nil]==>(ectx)* ▷↯) ->
  ▷ e ⇓ ▷↯
.
Proof.
  intros. dependent induction H; eauto. 
  destruct r2.
  - inv H. inv H5.
  - eapply step_implies_bigstep_fail'; eauto.
  - destruct r2.  
    eapply step_implies_bigstep_fail; eauto.
    inv H. 
Qed.
Lemma bigstep_equiv_starstep (e : expr) (r : rtexpr) :
  rtexpr_is_final r ->
  ▷ e ⇓ r <-> exists As, ▷ e ==[As]==>(ectx)* r
.
Proof.
  split; intros.
  - eapply bigstep_implies_steps in H0; deex; now exists As.
  - deex; inv H.
    + eapply steps_implies_bigstep.
      apply star_step_nf_empty_trace in H0 as H0'; subst.
      assumption. 
    + eapply steps_implies_bigstep_crash.
      apply star_step_nf_some_trace in H0 as H0'; subst. 
      assumption.
Qed.
Lemma bigstep_equiv_starstep_value (e : expr) (v : value) :
  ▷ e ⇓ ▷ Xval v <-> ▷ e ==[]==>(ectx)* ▷ Xval v
.
Proof.
  split; intros.
  - eapply bigstep_implies_steps in H; deex.
    apply star_step_nf_empty_trace in H as H'; subst; assumption.
  - now eapply steps_implies_bigstep in H. 
Qed.
Lemma bigstep_equiv_starstep_crash (e : expr) :
  ▷ e ⇓ ▷↯ <-> ▷ e ==[Scrash :: nil]==>(ectx)* ▷↯
.
Proof.
  split; intros.
  - eapply bigstep_implies_steps in H; deex.
    apply star_step_nf_some_trace in H as H'; subst; assumption.
  - now eapply steps_implies_bigstep_crash in H. 
Qed.
Lemma unique_nf (e : expr) (r r' : rtexpr) (As As' : tracepref) :
  rtexpr_is_final r ->
  rtexpr_is_final r' ->
  ▷ e ==[As]==>(ectx)* r ->
  ▷ e ==[As']==>(ectx)* r' ->
  r = r' /\ As = As'
.
Proof.
  intros H0 H1 H2 H3; inv H0; inv H1. 
  - apply star_step_nf_empty_trace in H2 as H2';
    apply star_step_nf_empty_trace in H3 as H3'; subst. 
    apply steps_implies_bigstep in H2.
    apply steps_implies_bigstep in H3.
    eapply unique_nf_bstep in H2; eauto.
  - apply star_step_nf_empty_trace in H2 as H2';
    apply star_step_nf_some_trace in H3 as H3'; subst. 
    apply steps_implies_bigstep in H2.
    apply steps_implies_bigstep_crash in H3.
    eapply unique_nf_bstep in H2; eauto. inv H2. 
  - apply star_step_nf_some_trace in H2 as H2';
    apply star_step_nf_empty_trace in H3 as H3'; subst. 
    apply steps_implies_bigstep_crash in H2.
    apply steps_implies_bigstep in H3.
    eapply unique_nf_bstep in H2; eauto. inv H2. 
  - apply star_step_nf_some_trace in H2 as H2';
    apply star_step_nf_some_trace in H3 as H3'; subst. 
    apply steps_implies_bigstep_crash in H2.
    apply steps_implies_bigstep_crash in H3.
    eapply unique_nf_bstep in H2; eauto.
Qed.
Lemma bigstep_implies_step_n e r :
  rtexpr_is_final r ->
  ▷ e ⇓ r -> exists As n, ▷ e -(n)-[ As ]-->(ectx) r
.
Proof.
  intros H H0%bigstep_equiv_starstep; inv H; deex; try now constructor.
  all: exists As; now apply star_step_n_step in H0. 
Qed.

(** Finally, prove the split lemmas for starstep. *)
Lemma binop_exec_split (symb : binopsymb) (e1 e2 : expr) (v : value) :
  ▷ Xbinop symb e1 e2 ==[]==>(ectx)* ▷ Xval v ->
  exists (n1 n2 : nat), (▷ e1 ==[]==>(ectx)* ▷ Xval n1) /\
                 (▷ e2 ==[]==>(ectx)* ▷ Xval n2) /\
                 Some v = eval_binop symb n1 n2
.
Proof.
  intros H%bigstep_equiv_starstep_value%big_binop_exec_split; deex.
  destruct H as [Ha [Hb Hc]]. exists n1, n2. 
  apply bigstep_equiv_starstep_value in Ha, Hb. eauto.
Qed.
Lemma binop_exec_split_crash (symb : binopsymb) (e1 e2 : expr) :
  ▷ Xbinop symb e1 e2 ==[Scrash :: nil]==>(ectx)* ▷↯ ->
  (▷ e1 ==[Scrash :: nil]==>(ectx)* ▷↯) \/
  exists (n1 : nat), (▷ e1 ==[]==>(ectx)* ▷ Xval n1) 
                  /\ (▷ e2 ==[Scrash :: nil]==>(ectx)* ▷↯)
.
Proof.
  intros H%bigstep_equiv_starstep_crash%big_binop_exec_split_crash.
  destruct H as [H%bigstep_equiv_starstep_crash
                | [n1 [Ha%bigstep_equiv_starstep_value Hb%bigstep_equiv_starstep_crash]]]; eauto.
Qed.
Lemma if_exec_split (e0 e1 e2 : expr) (v : value) :
  ▷ Xif e0 e1 e2 ==[]==>(ectx)* ▷ Xval v ->
  ((▷ e0 ==[]==>(ectx)* ▷ Xval true) /\ (▷ e1 ==[]==>(ectx)* ▷ Xval v)
) \/ ((▷ e0 ==[]==>(ectx)* ▷ Xval false) /\ (▷ e2 ==[]==>(ectx)* ▷ Xval v))
.
Proof.
  intros H%bigstep_equiv_starstep_value%big_if_exec_split; deex.
  destruct H as [[Ha Hb]|[Ha Hb]];
  apply bigstep_equiv_starstep_value in Ha, Hb; eauto.
Qed.
Lemma if_exec_split_crash (e0 e1 e2 : expr) :
  ▷ Xif e0 e1 e2 ==[Scrash :: nil]==>(ectx)* ▷↯ ->
  (▷ e0 ==[Scrash :: nil]==>(ectx)* ▷↯
) \/ ((▷ e0 ==[]==>(ectx)* ▷ Xval true) /\ (▷ e1 ==[Scrash :: nil]==>(ectx)* ▷↯)
) \/ ((▷ e0 ==[]==>(ectx)* ▷ Xval false) /\ (▷ e2 ==[Scrash :: nil]==>(ectx)* ▷↯))
.
Proof.
  intros H%bigstep_equiv_starstep_crash%big_if_exec_split_crash; deex.
  destruct H as [H%bigstep_equiv_starstep_crash |
                 [[Ha%bigstep_equiv_starstep_value Hb%bigstep_equiv_starstep_crash] |
                 [Ha%bigstep_equiv_starstep_value Hb%bigstep_equiv_starstep_crash]]]; eauto.
Qed.
Lemma let_exec_split (x : vart) (e1 e2 : expr) (v : value) :
  ▷ Xlet x e1 e2 ==[]==>(ectx)* ▷ Xval v ->
  exists (v1 : value), (▷ e1 ==[]==>(ectx)* ▷ Xval v1) /\
                       (▷ subst x e2 v1 ==[]==>(ectx)* ▷ Xval v)
.
Proof.
  intros H%bigstep_equiv_starstep_value%big_let_exec_split; deex.
  destruct H as [Ha Hb];
  apply bigstep_equiv_starstep_value in Ha, Hb; eauto.
Qed.

Lemma let_exec_split_crash (x : vart) (e1 e2 : expr) :
  ▷ Xlet x e1 e2 ==[Scrash :: nil]==>(ectx)* ▷↯ ->
  (▷ e1 ==[Scrash :: nil]==>(ectx)* ▷↯) \/
  (exists (v : value), (▷ e1 ==[]==>(ectx)* ▷ Xval v) /\
                       (▷ subst x e2 v ==[Scrash :: nil]==>(ectx)* ▷↯))
.
Proof.
  intros H%bigstep_equiv_starstep_crash%big_let_exec_split_crash; deex.
  destruct H as [H%bigstep_equiv_starstep_crash |
                 [v [Ha%bigstep_equiv_starstep_value Hb%bigstep_equiv_starstep_crash]]]; eauto.
Qed.
Lemma star_nosteps_value_crash (As : tracepref) (v : value) :
  (▷ Xval v ==[ As ]==>(ectx)* ▷↯) ->
  False
.
Proof.
  intros H. inv H. inv H0. induction K; cbn in H2; inv H2. inv H4.
  induction K; cbn in H3; inv H3. inv H4. inv H0.
  induction K; cbn in H2; inv H2. inv H4.
Qed.
Lemma star_nosteps_value_abort (As : tracepref) (v : value) :
  (▷ Xval v ==[ As ]==>(ectx)* ▷ Xabort) ->
  False
.
Proof.
  intros H. inv H. inv H0. induction K; cbn in H2; inv H2. inv H4.
  induction K; cbn in H3; inv H3. inv H4. inv H0.
  induction K; cbn in H2; inv H2. inv H4.
Qed.
Lemma star_nosteps_value_eq (As : tracepref) (v0 v1 : value) :
  (▷ Xval v0 ==[ As ]==>(ectx)* ▷ Xval v1) ->
  v0 = v1 /\ As = nil
.
Proof.
  intros H; inv H. split; trivial. inv H0. induction K; cbn in H2; inv H2.
  inv H4. inv H4. induction K; cbn in H3; inv H3. inv H0. 
  induction K; cbn in H2; inv H2. inv H4.
Qed.
Lemma star_nosteps_var_fin (As : tracepref) (x : vart) (r : rtexpr) :
  (▷ Xvar x ==[ As ]==>(ectx)* r) ->
  rtexpr_is_final r ->
  False
.
Proof.
  intros H H0; inv H; inv H0. inv H1. induction K; cbn in H0; inv H0. inv H4. 
  induction K; cbn in H3; inv H3; inv H4. inv H1. 
  induction K; cbn in H0; inv H0; inv H4. 
  induction K; cbn in H3; inv H3; inv H4. 
  inv H1. 
  induction K; cbn in H0; inv H0; inv H4. 
  inv H1. 
  induction K; cbn in H0; inv H0; inv H4. 
Qed.
Lemma star_nosteps_abort_nocrash (As : tracepref) (r : rtexpr) :
  (▷ Xabort ==[ As ]==>(ectx)* r) ->
  rtexpr_is_final r ->
  r <> ▷↯ ->
  False
.
Proof.
  intros. inv H0. inv H. inv H0. induction K; cbn in H3; inv H3; inv H5. 
  induction K; cbn in H4; inv H4. inv H2. inv H0. inv H0. inv H0. 
  induction K; cbn in H3; inv H3; inv H5.
  now apply H1.
Qed.
