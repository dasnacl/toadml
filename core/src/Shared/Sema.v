Set Implicit Arguments.
Require Import Strings.String Strings.Ascii Lists.List Lia.

Require Import ToadML.Util.Convenience ToadML.Util.HasEquality ToadML.Util.NoDupInv.

#[export]
Instance nateq__Instance : HasEquality nat := {
  eq := Nat.eqb ;
  eqb_eq := PeanoNat.Nat.eqb_eq ;
}.

(** Possible binary operations. *)
Variant binopsymb : Type :=
| Badd : binopsymb
| Bsub : binopsymb
| Bmul : binopsymb
| Bdiv : binopsymb
| Bless : binopsymb
.
Definition binopsymb_eqb :=
  fun (b1 b2 : binopsymb) =>
    match b1, b2 with
    | Badd, Badd | Bsub, Bsub | Bmul, Bmul | Bdiv, Bdiv | Bless, Bless => true
    | _, _ => false
    end
.
Lemma binopsymb_eqb_eq b1 b2 :
  binopsymb_eqb b1 b2 = true <-> b1 = b2.
Proof. destruct b1, b2; now cbn. Qed.
#[export]
Instance binopsymb__Instance : HasEquality binopsymb := {
  eq := binopsymb_eqb ;
  eqb_eq := binopsymb_eqb_eq ;
}.
Definition string_of_symb s :=
  match s with
  | Badd => "+"
  | Bsub => "-"
  | Bmul => "*"
  | Bdiv => "/"
  | Bless => "<"
  end%string
.

(** Context switch indicators. The paper calls these Transfer Tags *)
Variant comms : Type :=
| Qctxtocomp : comms
| Qcomptoctx : comms
| Qinternal : comms
.
Definition comms_eqb (q1 q2 : comms) :=
  match q1, q2 with
  | Qctxtocomp, Qctxtocomp => true
  | Qcomptoctx, Qcomptoctx => true
  | Qinternal, Qinternal => true
  | _, _ => false
  end
.
Lemma comms_eqb_eq (q1 q2 : comms) :
  comms_eqb q1 q2 = true <-> q1 = q2.
Proof. destruct q1, q2; now cbn. Qed.
#[export]
Instance commseq__Instance : HasEquality comms := {
  eq := comms_eqb ;
  eqb_eq := comms_eqb_eq ;
}.
Definition string_of_comms (q : comms) :=
  match q with
  | Qctxtocomp => "?"%string
  | Qcomptoctx => "!"%string
  | Qinternal => "∅"%string
  end
.

(** The type used for variable names. *)
Definition vart := string.
Definition vareq := fun x y => (x =? y)%string.
Definition dontcare := "_"%string.
#[export]
Instance varteq__Instance : HasEquality vart := {
  eq := vareq ;
  eqb_eq := String.eqb_eq ;
}.

