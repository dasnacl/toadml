(** Definition of Compiler, forward and backward simulations, as well as robust preservation. *)
Require Import Strings.String Strings.Ascii Numbers.Natural.Peano.NPeano Lists.List Program.Equality Recdef Lia.
Require Import ToadML.Shared.Fresh ToadML.Shared.Sema ToadML.Util.Sets ToadML.Util.HasEquality.
Require Import ToadML.Util.Convenience ToadML.Util.NoDupInv ToadML.Util.Util ToadML.Util.Step.

Require ToadML.T.T ToadML.S.S.

Module T := ToadML.T.T.
Module S := ToadML.S.S.

Inductive xevent_eq : S.event -> T.event -> Prop :=
  | x_starteq : xevent_eq S.Sstart T.Sstart
  | x_crasheq : xevent_eq S.Scrash T.Scrash
  | x_endeq : forall v, xevent_eq (S.Send v) (T.Send v)
  | x_call : forall v q, xevent_eq (S.Scall q v) (T.Scall q v)
  | x_ret : forall v q, xevent_eq (S.Sret q v) (T.Sret q v)
.
Inductive xtrace_eq : tracepref -> tracepref -> Prop :=
  | xempty_eq : xtrace_eq nil nil 
  | xcons_eq : forall (a : S.event) (b : T.event) (As : tracepref) (Bs : tracepref),
  xevent_eq a b ->
  xtrace_eq As Bs ->
  xtrace_eq (a :: As) (b :: Bs) 
.

(** Compiling values is just a "recoloring", nothing interesting happening here *)
Definition compile_v (v : S.value) : T.value :=
  match v with
  | S.Vnat n => T.Vnat n
  | S.Vbool b => T.Vbool b
  end
.
Definition compile_r Ψ Δ cmds (r : S.rtexpr) : option (T.rtprog) :=
  match r with
  | S.RTerm (S.Xval v) => Some(T.RPTerm (T.Sval(compile_v v) :: Ψ) Δ cmds)
  | S.RCrash => Some(T.RPCrash)
  | _ => None
  end
.
Lemma compile_v_injective' (v v' : S.value) :
  compile_v v = compile_v v' ->
  v = v'
.
Proof.
  destruct v, v'; cbn in *; congruence.
Qed.
(** Compiling expressions the more or less standard way when compiling to stack-based languages *)
Fixpoint compile_e (e : S.expr) : list T.cmd :=
  match e with
  | S.Xval v => T.Cpush (compile_v v) :: nil
  | S.Xvar x => T.Cget x :: nil
  | S.Xbinop symb e1 e2 => compile_e e1 ++ compile_e e2 ++ T.Cbop symb :: nil
  | S.Xif e1 e2 e3 => T.Cquote (compile_e e2) :: compile_e e1 ++ T.Cif
  :: T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: nil
  | S.Xabort => T.Cabort :: nil
  end
.

(** Assume there exists an implementation of a typechecker for S. *)
(** This is a nice exercise to try and extend S/static.v with a `Fixpoint`
   defining the `check` axiom as well as a lemma proving `check_equiv_Scheck`. *)
Axiom check : S.symbol -> bool.
Axiom check_equiv_Scheck : forall (foo : S.symbol), check foo = true <-> S.check_symb foo.

(** To compile symbols, the compiler has to ensure that the target context cannot pass ill-typed arguments. *)
(** Furthermore, it requires that the component is well-typed. *)
Definition compile_symb' (foo : S.symbol) : T.symbol :=
  let x__foo := S.vart_of_symbol foo in
  let e__foo := S.expr_of_symbol foo in
  let wrapper := T.Cquote (T.Cabort :: nil) :: T.Cget x__foo
    :: T.Chas T.Tnat :: T.Cnegb :: T.Cif :: nil in
    (x__foo, wrapper ++ compile_e e__foo)
.
Definition compile_symb (foo : S.symbol) : T.symbol :=
  if check foo then
  compile_symb' foo
  else
  (S.vart_of_symbol foo, T.Cabort :: nil)
.
Lemma compile_symb_prop (foo : S.symbol) :
  (compile_symb foo = (S.vart_of_symbol foo, T.Cabort :: nil) /\ ~S.check_symb foo) \/
  (S.check_symb foo /\ compile_symb foo = compile_symb' foo)
.
Proof.
  unfold compile_symb; remember (check foo) as b; destruct b; symmetry in Heqb;
  destruct foo as [[[x__foo τ0] τ1] e__body].
- right; apply check_equiv_Scheck in Heqb; split; trivial.
- left; firstorder try easy. intros H. rewrite <- check_equiv_Scheck in H; congruence.
Qed.

#[local] Existing Instance S.S__LangParams.
#[local] Existing Instance T.T__LangParams.

#[local]
Notation "'s(' Ψ ';' Δ '▷' cmds ')'" := (T.RPTerm Ψ Δ cmds) (at level 81, Ψ at next level, Δ at next level, cmds at next level).

(** Little helper tactic to perform one step of evaluation when dealing with multi-step relations *)
Ltac step e := (econstructor; try eapply e).

(** * Robust Preservation *)
(** The following section proves that the compiler is robustly preserving. *)

(** Equivalence between S substitutions and T stores. *)
Inductive substitutions_eq_store : S.substitutions -> T.store -> Prop :=
  | substitutions_eq_store_nil : substitutions_eq_store (mapNil _ _) None
  | substitutions_eq_store_cons : forall (γs : S.substitutions) (x : vart) (v : S.value),
  substitutions_eq_store (x ↦ v ◘ γs) (Some(x, compile_v v))
.
#[local]
Infix "≊" := (substitutions_eq_store) (at level 80, no associativity).
Notation "e '⦇' γs '⦈'" := (S.apply_substitutions e γs) (at level 81, γs at next level, left associativity).

Open Scope LangNotationsScope.

Local Existing Instance S.S__LangParams.
Local Existing Instance S.eventeq__Instance.
Local Existing Instance S.eventshow__Instance.
Local Existing Instance T.T__LangParams.
Local Existing Instance T.eventeq__Instance.
Local Existing Instance T.eventshow__Instance.

Lemma S_reduce_to_crash_impossible e :
  (S.RTerm e ==[]==>(S.ectx)* S.RCrash) ->
  False
.
Proof.
  intros H; dependent induction H; inv H; eauto.
Qed.

(** Some helper tactic to get rid of easy cases that don't reduce. *)
Ltac nostep := (
  repeat match goal with
         | [H: S.rtexpr_is_final (S.RTerm (S.Xabort)) |- _] => inv H
         | [H: (S.RTerm(S.Xabort _) ==[ _ ]==>(S.ectx)* _)%langnotationsscope |- _ ] =>
             inv H
         | [H: S.rtexpr_is_final (S.RTerm (S.Xvar _)) |- _] => inv H
         | [H: (S.RTerm(S.Xvar _) ==[ _ ]==>(S.ectx)* _)%langnotationsscope |- _ ] =>
             inv H
         | [H: (S.RCrash --[, _ ]-->(S.ectx) _)%langnotationsscope |- _ ] =>
             inv H
         | [H: (S.RTerm(S.Xabort) --[, _ ]-->(S.ectx) _)%langnotationsscope |- _ ] =>
             inv H
         | [H: S.Xabort = S.insert ?K ?e,
         H': S.RTerm ?e --[, _ ]-->(S.prim) _ |- _] =>
             induction K; cbn in H; try congruence; inv H; inv H'
         | [H: (S.RTerm(S.Xval _) --[, _ ]-->(S.ectx) _)%langnotationsscope |- _ ] =>
             inv H
         | [H: S.Xval ?v = S.insert ?K ?e,
         H': S.RTerm ?e --[, _ ]-->(S.prim) _ |- _] =>
             induction K; cbn in H; try congruence; inv H; inv H'
         | [H: (S.RTerm(S.Xvar _) --[, _ ]-->(S.ectx) _)%langnotationsscope |- _ ] =>
             inv H
         | [H: S.Xvar ?v = S.insert ?K ?e,
         H': S.RTerm ?e --[, _ ]-->(S.prim) _ |- _] =>
             induction K; cbn in H; try congruence; inv H; inv H'
         end)
.

(** Forward Simulation *)
Lemma forward_sim (e : S.expr) (r : S.rtexpr) (γs : S.substitutions) (Ψ : T.stack) (Δ : T.store) (cmds : list T.cmd) (As0 : tracepref) :
  (S.RTerm(e ⦇ γs ⦈)) ==[As0]==>(S.ectx)* r ->
  S.rtexpr_is_final r ->
  length γs <= 1 ->
  γs ≊ Δ ->
  exists n r' As1, Some r' = compile_r Ψ Δ cmds r /\
  ((s(Ψ ; Δ ▷ (compile_e e) ++ cmds)) -(n)-[As1]-->(T.scmd) r') /\
  xtrace_eq As0 As1
.
Proof.
  revert Ψ Δ cmds r γs As0; induction e; intros; cbn.
  - inv H0; try (now exfalso; eapply S_reduce_to_crash_impossible; eauto).
    rewrite S.same_value_substitution_zap in H. 
    inv H.  
    exists 1; do 2 eexists; repeat split; try easy. econstructor 3. step T.e_push. econstructor. constructor.
    inv H0. induction K; cbn in H4; inv H4. inv H6.
    induction K; cbn in H5; inv H5. inv H6. inv H0. induction K; cbn in H4; inv H4. inv H6. rewrite S.same_value_substitution_zap in H. 
    now apply S.star_nosteps_value_crash in H.
  - destruct (S.var_subst_val_dec x γs); deex.
    + destruct H3 as [Ha Hb]; rewrite Ha in H; inv H0; try now nostep; try now eapply S.star_nosteps_value_crash in H.
      inv H2; try now cbn in Ha. inv H1. apply length_0_inv in H2; subst.
      apply S.star_nosteps_value_eq in H as []; subst. 
      inv Hb; try congruence. destruct H; subst. do 3 eexists; repeat split; try now cbn.
      econstructor 3. step T.e_get. reflexivity. instantiate (2:=0). instantiate (1:=nil).
      econstructor. econstructor.
      inv H. (*spurious*)
      inv H2. (*also spurious*)
    + rewrite H3 in H. now apply S.star_nosteps_var_fin in H; auto.
  - rewrite S.subst_binop_commute in *. assert (H':=H).
    inv H0; try congruence.
    + apply S.star_step_nf_empty_trace in H'; subst.
      apply S.binop_exec_split in H; deex; destruct H as [Ha [Hb Hc]].
      assert (S.rtexpr_is_final (S.RTerm(S.Xval(S.Vnat n1)))) as H0 by constructor.
      specialize (IHe1 Ψ Δ (compile_e e2 ++ T.Cbop symb :: cmds) (S.RTerm(S.Xval(S.Vnat n1))) γs nil Ha H0 H1 H2); deex; clear H0; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1c.
      assert (S.rtexpr_is_final (S.RTerm(S.Xval(S.Vnat n2)))) as H0 by constructor.
      specialize (IHe2 (T.Sval (compile_v (S.Vnat n1)) :: Ψ) Δ (T.Cbop symb :: cmds) (S.RTerm(S.Xval(S.Vnat n2))) γs nil Hb H0 H1 H2); deex; clear H0; destruct IHe2 as [IHe2a [IHe2b IHe2c]]; inv IHe2c.
      assert (exists r', Some r' = compile_r Ψ Δ cmds (S.RTerm(S.Xval v))) as H0 by (cbn; eauto).
      deex; exists (n + (n0 + 1)), r'1, nil; inv H0; repeat split; trivial; try now constructor.
      inv IHe2a. inv IHe1a.
      rewrite <- ! app_assoc; cbn.
      eapply @n_step_trans_unimp; eauto. eapply @n_step_trans_unimp; eauto.
      econstructor. 2: econstructor. cbn.
      step T.e_bop.
      inv Hc. destruct symb; cbn; try easy.
      destruct n2; try easy. cbn. unfold T.eval_binop. cbn. destruct (n1 <=? n2); try easy.
    + apply S.star_step_nf_some_trace in H'; subst.
      apply S.binop_exec_split_crash in H; destruct H as [H|H]; deex.
      * specialize (IHe1 Ψ Δ (compile_e e2 ++ T.Cbop symb :: cmds) (S.RCrash) γs (S.Scrash :: nil) H S.CRTfail H1 H2); deex; cbn in IHe1; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        inv H4; inv H6. exists n, T.RPCrash, (T.Scrash :: nil); repeat split; trivial; try now repeat constructor.
        rewrite <- ! app_assoc; cbn.
        change (T.Scrash :: nil) with ((T.Scrash :: nil) ++ nil).
        rewrite <- (Nat.add_0_r n); eapply @n_step_trans; eauto.
        constructor.
      * destruct H as [Ha Hb].
        assert (S.rtexpr_is_final (S.RTerm(S.Xval(S.Vnat n1)))) as H0 by constructor.
        specialize (IHe1 Ψ Δ (compile_e e2 ++ T.Cbop symb :: cmds) (S.RTerm(S.Xval(S.Vnat n1))) γs nil Ha H0 H1 H2); deex; clear H0; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        specialize (IHe2 (T.Sval (compile_v (S.Vnat n1)) :: Ψ) Δ (T.Cbop symb :: cmds) (S.RCrash) γs (S.Scrash :: nil) Hb S.CRTfail H1 H2); deex; destruct IHe2 as [IHe2a [IHe2b IHe2c]]; inv IHe2a; inv IHe2c.
        inv H3; inv H5.
        exists (n + n0), T.RPCrash, (T.Scrash :: nil); repeat split; trivial; try now repeat constructor.
        rewrite <- ! app_assoc; cbn.
        change (T.Scrash :: nil) with (nil ++ (T.Scrash :: nil)).
        eapply @n_step_trans; eauto. 
  - rewrite S.subst_if_commute in *. assert (H':=H).
    inv H0; try congruence.
    + apply S.star_step_nf_empty_trace in H'; subst.
      apply S.if_exec_split in H; deex; destruct H as [[Ha Hb]|[Ha Hb]].
      * assert (IHe1':=IHe1).
        specialize (IHe1 (T.Scode (compile_e e2) :: Ψ) Δ (T.Cif :: T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool true))) γs nil Ha (S.CRTval (S.Vbool true)) H1 H2); deex; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        specialize (IHe2 Ψ Δ (T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval v)) γs nil Hb (S.CRTval v) H1 H2); deex; destruct IHe2 as [IHe2a [IHe2b IHe2c]]; inv IHe2a; inv IHe2c.
        specialize (IHe1' (T.Scode (compile_e e3) :: T.Sval(compile_v v) :: Ψ) Δ (T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool true))) γs nil Ha (S.CRTval (S.Vbool true)) H1 H2); deex; destruct IHe1' as [IHe1'a [IHe1'b IHe1'c]]; inv IHe1'a; inv IHe1'c.
        assert (exists r', Some r' = (compile_r Ψ Δ cmds (S.RTerm (S.Xval v)))) as H0 by (cbn; eauto).
        deex; exists (1 + n + 1 + n0 + 1 + n1 + 2), r', nil; inv H0.
        repeat split; trivial; try now constructor. 
        step T.e_quote. repeat rewrite <- app_assoc; cbn.
        assert (((compile_e e1 ++ T.Cnegb :: T.Cif :: nil) ++ cmds) = (compile_e e1 ++ T.Cnegb :: T.Cif :: cmds))
        by (rewrite <- app_assoc; now cbn).
        rewrite H; clear H.
        rewrite <- ! Nat.add_assoc.
        eapply @n_step_trans_unimp; eauto.
        step T.e_if_true. eapply @n_step_trans_unimp; eauto.
        step T.e_quote. eapply @n_step_trans_unimp; eauto. step T.e_negb.
        econstructor. 2: econstructor. eapply T.e_if_false.
      * assert (IHe1':=IHe1).
        specialize (IHe1 (T.Scode (compile_e e2) :: Ψ) Δ (T.Cif :: T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool false))) γs nil Ha (S.CRTval (S.Vbool false)) H1 H2); deex; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        specialize (IHe1' (T.Scode (compile_e e3) :: Ψ) Δ (T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool false))) γs nil Ha (S.CRTval (S.Vbool false)) H1 H2); deex; destruct IHe1' as [IHe1'a [IHe1'b IHe1'c]]; inv IHe1'a; inv IHe1'c.
        specialize (IHe3 Ψ Δ cmds (S.RTerm(S.Xval v)) γs nil Hb (S.CRTval v) H1 H2); deex; destruct IHe3 as [IHe3a [IHe3b IHe3c]]; inv IHe3a; inv IHe3c.
        assert (exists r', Some r' = (compile_r Ψ Δ cmds (S.RTerm (S.Xval v)))) as H0 by (cbn; eauto).
        deex; exists (1 + n + 2 + n0 + 2 + n1), r', nil; inv H0.
        repeat split; trivial; try now constructor.
        step T.e_quote. rewrite <- ! app_assoc; cbn.
        assert (((compile_e e1 ++ T.Cnegb :: T.Cif :: nil) ++ cmds) = (compile_e e1 ++ T.Cnegb :: T.Cif :: cmds))
        by (rewrite <- app_assoc; now cbn).
        rewrite H; clear H.
        rewrite <- ! Nat.add_assoc.
        eapply @n_step_trans_unimp; eauto.
        step T.e_if_false. step T.e_quote. eapply @n_step_trans_unimp; eauto.
        step T.e_negb. step T.e_if_true. rewrite <- (Nat.add_0_r n1).
        eapply @n_step_trans_unimp. 2: econstructor.
        easy.
    + apply S.star_step_nf_some_trace in H'; subst.
      apply S.if_exec_split_crash in H; deex. destruct H as [H|[[Ha Hb]|[Ha Hb]]].
      * (* condition crashes *)
        clear IHe2 IHe3.
        specialize (IHe1 (T.Scode (compile_e e2) :: Ψ) Δ (T.Cif :: T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RCrash) γs (S.Scrash :: nil) H S.CRTfail H1 H2); deex; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        inv H4; inv H6.
        exists (1 + n), T.RPCrash, (T.Scrash :: nil); repeat split; trivial; try now repeat constructor.
        econstructor 3. eapply T.e_quote; eauto.
        rewrite <- ! app_assoc; cbn.
        assert (((compile_e e1 ++ T.Cnegb :: T.Cif :: nil) ++ cmds) = (compile_e e1 ++ T.Cnegb :: T.Cif :: cmds))
        by (rewrite <- app_assoc; now cbn).
        rewrite H0; clear H0.
        rewrite <- (Nat.add_0_r n). 
        change (T.Scrash :: nil) with ((T.Scrash :: nil) ++ nil).
        eapply @n_step_trans; eauto.
        econstructor.
      * (* consequence crashes *)
        clear IHe3.
        specialize (IHe1 (T.Scode (compile_e e2) :: Ψ) Δ (T.Cif :: T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool true))) γs nil Ha (S.CRTval(S.Vbool true)) H1 H2); deex; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        specialize (IHe2 Ψ Δ (T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RCrash) γs (S.Scrash :: nil) Hb (S.CRTfail) H1 H2); deex; destruct IHe2 as [IHe2a [IHe2b IHe2c]]; inv IHe2a; inv IHe2c.
        inv H3; inv H5.
        exists (1 + n + 1 + n0), T.RPCrash, (T.Scrash :: nil); repeat split; trivial; try now repeat constructor.
        econstructor 3. eapply T.e_quote.
        rewrite <- ! app_assoc; cbn.
        assert (((compile_e e1 ++ T.Cnegb :: T.Cif :: nil) ++ cmds) = (compile_e e1 ++ T.Cnegb :: T.Cif :: cmds))
        by (rewrite <- app_assoc; now cbn).
        rewrite H; clear H.
        change (T.Scrash :: nil) with (nil ++ (T.Scrash :: nil)).
        eapply @n_step_trans; eauto.
        eapply @n_step_trans_unimp; eauto.
        step T.e_if_true. econstructor.
      * (* alternative crashes *)
        clear IHe2. assert (IHe1':=IHe1).
        specialize (IHe1 (T.Scode (compile_e e2) :: Ψ) Δ (T.Cif :: T.Cquote (compile_e e3) :: compile_e e1 ++ T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool false))) γs nil Ha (S.CRTval(S.Vbool false)) H1 H2); deex; destruct IHe1 as [IHe1a [IHe1b IHe1c]]; inv IHe1a; inv IHe1c.
        specialize (IHe1' (T.Scode (compile_e e3) :: Ψ) Δ (T.Cnegb :: T.Cif :: cmds) (S.RTerm(S.Xval(S.Vbool false))) γs nil Ha (S.CRTval (S.Vbool false)) H1 H2); deex; destruct IHe1' as [IHe1'a [IHe1'b IHe1'c]]; inv IHe1'a; inv IHe1'c.
        specialize (IHe3 Ψ Δ cmds (S.RCrash) γs (S.Scrash :: nil) Hb (S.CRTfail) H1 H2); deex; destruct IHe3 as [IHe3a [IHe3b IHe3c]]; inv IHe3a; inv IHe3c.
        inv H3; inv H5.
        exists (1 + n + 1 + 1 + n0 + 2 + n1), T.RPCrash, (T.Scrash :: nil); repeat split; trivial; try now repeat constructor.
        econstructor 3. eapply T.e_quote.
        rewrite <- ! app_assoc; cbn.
        assert (((compile_e e1 ++ T.Cnegb :: T.Cif :: nil) ++ cmds) = (compile_e e1 ++ T.Cnegb :: T.Cif :: cmds))
        by (rewrite <- app_assoc; now cbn).
        rewrite H; clear H.
        change (T.Scrash :: nil) with (nil ++ (T.Scrash :: nil)).
        rewrite <- ! Nat.add_assoc.
        eapply @n_step_trans; eauto.
        econstructor 3. step T.e_if_false. 
        econstructor 3. step T.e_quote.
        change (T.Scrash :: nil) with (nil ++ (T.Scrash :: nil)).
        eapply @n_step_trans; eauto.
        change (2) with (1 + 1).
        econstructor 3. eapply T.e_negb.
        econstructor 3. eapply T.e_if_true.
        eauto. 
  - rewrite S.abort_substitution_zap in *.
    inv H0.
    + apply S.star_nosteps_abort_nocrash in H; try easy.
      constructor. 
    + exists 1, T.RPCrash, (T.Scrash :: nil); repeat split; try easy; try now repeat constructor.
      repeat econstructor.
      enough (As0 = S.Scrash :: nil); subst.
      repeat constructor. 
      inv H. inv H0. induction K; cbn in H4; inv H4; inv H6. 
      induction K; cbn in H5; inv H5; inv H6.
      now inv H3.
      inv H0. induction K; cbn in H4; inv H4; inv H6.
Qed.

(* Specialized versions of forward_sim *)
Corollary forward_sim_val (e : S.expr) (v : S.value) (γs : S.substitutions) (Ψ : T.stack) (Δ : T.store) (cmds : list T.cmd) :
  S.RTerm(e ⦇ γs ⦈) ==[]==>(S.ectx)* S.RTerm (S.Xval v) ->
  length γs <= 1 ->
  γs ≊ Δ ->
  exists n, (s(Ψ ; Δ ▷ (compile_e e) ++ cmds) -(n)-[nil]-->(T.scmd) s(T.Sval (compile_v v) :: Ψ ; Δ ▷ cmds))
.
Proof.
  intros. eapply forward_sim in H; eauto; try now constructor. 
  deex; destruct H as [Ha [Hb Hc]]. inv Ha. inv Hc. exists n; eauto.  
Qed.
Corollary forward_sim_crash (e : S.expr) (γs : S.substitutions) (Ψ : T.stack) (Δ : T.store) (cmds : list T.cmd) :
  S.RTerm(e ⦇ γs ⦈) ==[S.Scrash :: nil]==>(S.ectx)* (S.RCrash) ->
  length γs <= 1 ->
  γs ≊ Δ ->
  exists n, (s(Ψ ; Δ ▷ (compile_e e) ++ cmds) -(n)-[T.Scrash :: nil]-->(T.scmd) T.RPCrash)
.
Proof.
  intros. eapply forward_sim in H; eauto; try now constructor. 
  deex; destruct H as [Ha [Hb Hc]]. inv Ha. inv Hc. inv H5; inv H3. exists n; eauto.  
Qed.

(* We don't need a generalized version of backtranslation, so just split into two lemmas. *)
Lemma backward_sim_value (e : S.expr) (τ : S.ty) (v : S.value) (Ψ : T.stack) (Δ : T.store) (n : nat) (γs : S.substitutions) :
  S.check (mapNil _ _) (e ⦇ γs ⦈) τ ->
  length γs <= 1 ->
  γs ≊ Δ ->
  (s(Ψ ; Δ ▷ (compile_e e)) -(n)-[nil]-->(T.scmd) s(T.Sval (compile_v v) :: Ψ ; Δ ▷ nil)) ->
  (S.RTerm(e ⦇ γs ⦈) ==[]==>(S.ectx)* S.RTerm (S.Xval v))
.
Proof.
     intros.
     apply S.type_safety in H as [H|H]; deex.
     - assert (H':=H); eapply forward_sim in H; deex; eauto; try now constructor.
     destruct H as [Ha [Hb Hc]].
     assert (T.rtprog_is_final (s(T.Sval(compile_v v) :: Ψ; Δ ▷ nil))) by constructor. 
     assert (T.rtprog_is_final (s(T.Sval(compile_v v0) :: Ψ; Δ ▷ nil))) by constructor. 
     instantiate (2:=Ψ) in Ha; instantiate (1:=nil) in Ha.
     assert (H0':=Ha).
     eapply T.unique_nf_step_n in Hb as [Hb [Hb' Hb'']].
     instantiate (1:=(s( T.Sval (compile_v v) :: Ψ; Δ ▷ nil))) in Hb'.
     inv H0'. inv H5. apply compile_v_injective' in H6; subst.
     instantiate (1:=n) in Hb''; instantiate (1:=nil) in Hb.
     exact H'. 
     econstructor. inv H0'; econstructor.
     rewrite app_nil_r; assumption.
     - assert (H':=H); eapply forward_sim_crash in H; deex; eauto.
     instantiate (1:=nil) in H. instantiate (1:=Ψ) in H.
     rewrite app_nil_r in H.
     assert (H''':=H).
     eapply T.unique_nf_step_n in H2; try exact H'''.
     destruct H2 as [_ [H2 H2']]; congruence.
     1,2: constructor.
Qed.
Lemma backward_sim_crash (e : S.expr) (τ : S.ty) (Ψ : T.stack) (Δ : T.store) (n : nat) (γs : S.substitutions) :
  S.check (mapNil _ _) (e ⦇ γs ⦈) τ ->
  length γs <= 1 ->
  γs ≊ Δ ->
  s(Ψ ; Δ ▷ (compile_e e)) -(n)-[T.Scrash :: nil]-->(T.scmd) T.RPCrash ->
  (S.RTerm(e ⦇ γs ⦈) ==[S.Scrash :: nil]==>(S.ectx)* S.RCrash)
.
Proof.
  intros.
  apply S.type_safety in H as [H|H]; deex.
  - assert (H':=H); eapply forward_sim in H; deex; eauto.
    destruct H as [Ha [Hb Hc]].
    instantiate (1:=nil) in Ha; instantiate (1:=Ψ) in Ha.
    rewrite app_nil_r in Hb.
    inv Ha.
    assert (T.rtprog_is_final(s( T.Sval (compile_v v) :: Ψ; Δ ▷ nil))) by constructor.
    assert (T.rtprog_is_final(T.RPCrash)) by constructor.
    eapply T.unique_nf_step_n in H as [Ha [Ha' Ha'']]; try exact H0; eauto. inv Ha'.
    econstructor; eauto.
  - assumption.
Qed.

(** The backtranslation generates an S level context from a trace. *)
Definition bt (As : tracepref) : option S.LinkageContext :=
  match As with
  | T.Sstart ::
    T.Scrash :: nil => Some(S.LContext "y" (S.Xabort) S.Xabort)
  | T.Sstart ::
    T.Scall Qctxtocomp (inl v__pre) ::
    T.Scrash :: nil => Some(S.LContext "y" (S.Xval(S.Vnat v__pre)) (S.Xval(S.Vnat 5)))
  | T.Sstart ::
    T.Scall Qctxtocomp (inl v__pre) ::
    T.Sret Qcomptoctx _ ::
    T.Scrash :: nil => Some(S.LContext "y"
                          ((S.Xval (S.Vnat v__pre)))
                          S.Xabort)
  | T.Sstart ::
    T.Scall Qctxtocomp (inl v__pre) ::
    T.Sret Qcomptoctx _ ::
    T.Send v__end :: nil => Some(S.LContext "y" (S.Xval(S.Vnat v__pre)) (S.Xval(S.vs v__end)))
  | T.Sstart ::
    T.Scall Qctxtocomp (inr v__pre) ::
    _ => Some(S.LContext "y" (S.Xabort) (S.Xval(S.Vnat 5)))
  | _ => None
  end%string
.
(** Decompilation of values *)
Definition bt_v (v : T.value) : S.value :=
  match v with
  | T.Vnat n => S.Vnat n
  | T.Vbool b => S.Vbool b
  end
.
Lemma compile_v_bt_v v :
  compile_v (bt_v v) = v
.
Proof. destruct v; now cbn. Qed.
Lemma bt_v_compile_v v :
  bt_v (compile_v v) = v
.
Proof. destruct v; now cbn. Qed.
Local Hint Resolve compile_v_bt_v bt_v_compile_v : core.
(** Decompilation of states *)
Definition bt_r (rv : T.rtprog) : option S.rtexpr :=
  match rv with
  | T.RPTerm (T.Sval b :: nil) None nil => Some(S.RTerm(S.Xval(bt_v b)))
  | T.RPCrash => Some(S.RCrash)
  | _ => None
  end
.

(** We need a different kind of trace equality than strict equality.
    The backtranslated context may fail early if the component fails, e.g.,
    a context tries to pass a `bool` and the compiler wrapper fails.
    This, in turn, would mean that the backtranslation cannot generated a well-typed S context,
    so the only option is to abort. However, the abort then happens prior to
    calling into the component, so there is mismatch in the generated traces
    that we "need" to account for by using a different kind of "equality".
*)
Inductive trace_eq : tracepref -> tracepref -> Prop :=
| empty_eq : trace_eq nil nil 
| nocrash_eq : forall (v0 v1 v2 : nat + bool),
    trace_eq (T.Sstart :: T.Scall Qctxtocomp v0 :: T.Sret Qcomptoctx v1 :: T.Send v2 :: nil)
             (S.Sstart :: S.Scall Qctxtocomp v0 :: S.Sret Qcomptoctx v1 :: S.Send v2 :: nil) 
| crashctx1_eq :
    trace_eq (T.Sstart :: T.Scrash :: nil)
             (S.Sstart :: S.Scrash :: nil) 
| crashctx2_eq : forall (v : nat + bool),
    trace_eq (T.Sstart :: T.Scall Qctxtocomp v :: T.Scrash :: nil)
             (S.Sstart :: S.Scrash :: nil) 
| crashcomp_eq : forall (v : nat + bool),
    trace_eq (T.Sstart :: T.Scall Qctxtocomp v :: T.Scrash :: nil)
             (S.Sstart :: S.Scall Qctxtocomp v :: S.Scrash :: nil) 
| crashctx3_eq : forall (v0 v1 : nat + bool),
    trace_eq (T.Sstart :: T.Scall Qctxtocomp v0 :: T.Sret Qcomptoctx v1 :: T.Scrash :: nil)
             (S.Sstart :: S.Scall Qctxtocomp v0 :: S.Sret Qcomptoctx v1 :: S.Scrash :: nil) 
.
#[local]
Infix "≂" := (trace_eq) (at level 81).
Local Hint Constructors trace_eq : core.

(** Main proof that implies robust preservation.
    The backtranslation generates a context that behaves similar to a given target one. *)
Lemma bt_correctness (p : S.symbol) (ctx__T : T.LinkageContext) (r : T.rtprog) (As : tracepref) :
  T.progstep (T.plug' ctx__T (compile_symb p)) As r ->
  exists (ctx__S : S.LinkageContext) (r' : S.rtexpr) (As' : tracepref),
       Some ctx__S = bt As
    /\ Some r' = bt_r r
    /\ trace_eq As As' 
    /\ S.progstep (S.plug' ctx__S p) As' r'
.
Proof.
  intros. destruct (compile_symb_prop p) as [[H0a H0b] | [H0a H0b]].
  (* bad component. need lemma that says that ill-typed components go bad *)
  {
  rewrite H0a in H. dependent destruction H.
  - (* no fail is impossible in this configuration *)
    inv H2.
    (* component aborts, but this contradicts the emitted trace *)
    inv H.
  - (* context fails *) cbn. do 2 eexists; exists (S.Sstart :: S.Scrash :: nil); repeat split; eauto.
    econstructor. instantiate (1:=1). econstructor. econstructor 2.
    instantiate (2:=S.Khole). now cbn. econstructor. constructor.
  - (* component fails, this is "hardcoded" in toplevel semantics of S. *)
    cbn. destruct v__pre; cbn.
    + do 2 eexists; exists (S.Sstart :: S.Scall Qctxtocomp (inl n) :: S.Scrash :: nil); repeat split; eauto.
      change (inl n) with (S.sv (S.Vnat n)).
      econstructor 5; try now cbn. constructor.
    + (* BT gives an aborting context, since it's ill-typed to pass a 𝔹 *)
      do 2 eexists; exists (S.Sstart :: S.Scrash :: nil); repeat split; eauto.
      econstructor. econstructor. econstructor 2.
      instantiate (2:=S.Khole). now cbn.
      all: constructor.
  - (* impossible, the component fails so the emitted trace doesn't fit *)
    inv H2. inv H.
  }
  (* well-typed component *)
  {
  rewrite H0b in *.
  dependent destruction H.
  - (* no fail *)
    cbn; exists (S.LContext "y"%string
                   (S.Xval(bt_v v__pre))
                   (S.Xval(bt_v v))).
    exists (S.RTerm(S.Xval(bt_v v))).
    exists (S.Sstart :: S.Scall Qctxtocomp (T.sv v__pre)
         :: S.Sret Qcomptoctx (T.sv v__foo) :: S.Send (T.sv v) :: nil).
    (* get rid of compiler wrapper *)
    cbn in H2.
    inv H2. inv H. inv H0. inv H. inv H2.
    inv H. 
    { (* has ℕ is true *)
      inv H7. inv H0. inv H. inv H2. inv H. 
      destruct p as [[[x__foo ?] ?] e__foo]; cbn in *.
      inv H6; cbn.
      rewrite <- (compile_v_bt_v v__foo) in H0.
      repeat split; try now cbn. destruct v; now cbn.
      change (T.Vnat n0) with (compile_v (S.Vnat n0)) in H0.
      eapply backward_sim_value in H0.
      eapply star_step_n_step in H0; deex.
      change (inl n0) with (S.sv(S.Vnat n0)).
      assert ((T.sv v__foo) = (S.sv(bt_v v__foo))) as -> by (now destruct v__foo).
      assert ((T.sv v) = (S.sv(bt_v v))) as -> by (now destruct v).
      econstructor 1; eauto. 
      econstructor.
      unfold S.vart_of_symbol; unfold S.expr_of_symbol.
      instantiate (1:=n1).
      change (S.subst x__foo e__foo (S.Xval(S.Vnat n0))) with (e__foo ⦇ x__foo ↦ S.Vnat n0 ◘ mapNil _ _ ⦈).
      eassumption.
      cbn; instantiate (1:=0); constructor.
      instantiate (1:=S.Tnat). inv H0a. eapply S.substitution; eauto. constructor.
      constructor; try easy. constructor; try easy.
    }
    { (* has ℕ is false *)
      (* so this execution fails, which violates assumption H *)
      destruct v0; try congruence. 
      inv H0. inv H. inv H2. inv H. inv H0. inv H. 
    }
  - (* fails right at the beginning of executing the context *)
    cbn. do 3 eexists; repeat split; eauto.
    econstructor. instantiate (1:=1). econstructor. econstructor 2; eauto.
    instantiate (1:=S.Khole). now cbn. econstructor.
  - (* fails in the component *)
    cbn in H2.
    inv H2. inv H5. inv H. inv H0. inv H5. inv H. inv H5. inv H2. inv H5.
    inv H. do 3 eexists; repeat split; eauto; try now cbn.
    { (* has ℕ is true *)
      inv H0; try congruence; inv H5. inv H6. inv H. inv H2. inv H5. inv H.
      now cbn.
    }
    { (* has ℕ is true *)
      inv H5. inv H0. inv H5. inv H. inv H2. inv H5. inv H. 
      destruct p as [[[x0 τ0] τ1] e__foo]; cbn in *.
      change (inl n0) with (S.sv(S.Vnat n0)).
      eapply backward_sim_crash in H0; eauto.
      instantiate (1:=(x0 ↦ S.Vnat n0 ◘ mapNil _ _)) in H0; cbn in H0.
      apply star_step_n_step in H0; deex.
      econstructor; eauto.
      instantiate (1:=0); econstructor. inv H0a. eapply S.substitution; eauto.
      econstructor. now cbn. constructor.
    }
    { (* case where has ℕ is false *)
      (* this is where we have to associate a crash in a component with
         a crash in the context, no chance to do it otherwise *)
      destruct v__pre; try congruence.
      do 2 eexists; exists (S.Sstart :: S.Scrash :: nil); firstorder eauto.
      econstructor; eauto. econstructor. econstructor 2.
      instantiate (2:=S.Khole). now cbn. constructor.
      constructor.
    }
  - (* fails at the end of executing the context *)
    cbn. inv H2. inv H. inv H0. inv H.
    (* What we get with Cget is the same thing that occurs on the trace! *)
    inv H6.
    inv H0a. inv H2. inv H0. inv H7.
    subst; do 3 eexists; repeat split; eauto.
    { (* has ℕ is true *)
      inv H4. inv H0. inv H2. inv H0.
      cbn in H4.
      change (T.Vnat n0) with (compile_v (S.Vnat n0)) in H4.
      rewrite <- (compile_v_bt_v v__foo) in H4.
      change (T.Sval v__foo) with (T.Sval(bt_v v__foo)) in H4.
      eapply backward_sim_value in H4.
      eapply star_step_n_step in H4; deex.
      assert (T.sv(T.Vnat n0) = S.sv(S.Vnat n0)) as -> by (now cbn).
      assert (T.sv v__foo = S.sv(bt_v v__foo)) as -> by (now destruct v__foo).
      econstructor; eauto.
      econstructor; eauto. econstructor. instantiate (1:=n1).
      unfold S.vart_of_symbol; unfold S.expr_of_symbol.
      change (S.subst x e (S.Xval (S.Vnat n0))) with (e ⦇ x ↦ S.Vnat n0 ◘ mapNil _ _ ⦈).
      eassumption.
      econstructor. econstructor 2. instantiate (2:=S.Khole). now cbn. now cbn. econstructor.
      eapply S.substitution; eauto. constructor.
      now cbn. constructor. 
    }
    { (* has ℕ false *)
      destruct v__pre; try congruence.
      inv H4. inv H0. inv H2. inv H0. inv H4. inv H0. 
    }
  }
Qed.

(** We define a "weakened" version of RSC that has a different notion of trace equality. *)
Definition rscwt cc teq :=
  forall (p : S.symbol) (ctx__T : T.LinkageContext) (As : tracepref),
    forall r, T.progstep (T.plug' ctx__T (cc p)) As r ->
    exists r' (ctx__S : S.LinkageContext) As',
      teq As As' /\
      S.progstep (S.plug' ctx__S p) As' r'
.

Notation "'[' '|-' cc ':' 'rscwt~' teq ']'" := (rscwt cc teq) (at level 81, cc at next level).
Theorem compiler_attains_rsp : [|- compile_symb : rscwt~trace_eq].
Proof.
  unfold rscwt; intros.
  eapply bt_correctness in H; deex; destruct H as [Ha [Hb [Hc Hd]]].
  exists r', ctx__S, As'; split; easy.
Qed. 

