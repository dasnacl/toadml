

First milestone, up to the module parts, is to be able to at least interpret this in the unverified interpreter:

```ml
import Naturals, Lists, Logics.Equality with Notations, Tactics.
import Effects.

pose Expr : TypeFamily.

data Ast <: Expr [
  Const Nat,
  Add Expr Expr,
].
fn eval (Ast::Const n) := n
  and   (Ast::Add a1 a2) := eval a1 + eval a2
.

data Command [
  Push Nat,
  Add,
].
data Stack := list Nat.

use Command, Effect {
  fn eval (Push n) s := [n] ++ s
    and   (Add) ([n1, n2] ++ s) := [n1 + n2] ++ s
    else := panic "Inconsistent stack."
}

fn compile (what : Ast) : list Command :=
  match what [
    Const n => Push n,
    Add a1 a2 => compile a2 ++ compile a1 ++ [Add],
  ]
.

Lemma general_correct (what : Ast) (cs : list Command) (s : Stack) :
  eval (compile what ++ cs) s = eval cs ([eval what] ++ s)
 :=
  induction what /* todo */
(admit).
```


