use either::Either;
use std::collections::HashSet;
use std::fmt;

#[derive(Clone, Debug, Hash)]
pub enum EPath {
    Var(String),
    Star(),
    PathCons(String, Box<Path>),
}
#[derive(Clone, Debug, Hash)]
pub struct Path(pub EPath, pub Option<logos::Span>);

impl Path {
    pub fn add(&self, x: String) -> Path {
        match &self.0 {
            EPath::Var(y) => Path(EPath::Var(x + &y), self.1.clone()),
            EPath::Star() => panic!("Cannot append to star"),
            EPath::PathCons(a, b) => Path(
                EPath::PathCons(a.clone(), Box::new(b.add(x))),
                self.1.clone(),
            ),
        }
    }
    pub fn append(&mut self, x: String) -> () {
        *self = self.add(x);
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum EPattern {
    Wildcard,
    Var(Path),
    Unit,
    True,
    False,
    Tuple(Vec<Pattern>),
    App(Box<Pattern>, Box<Pattern>),
    Annot(Box<Pattern>, Preterm),
}
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct Pattern(pub EPattern, pub Option<logos::Span>);

impl TryFrom<Pattern> for Path {
    type Error = ();

    fn try_from(pat: Pattern) -> Result<Self, Self::Error> {
        match pat.0 {
            EPattern::Wildcard => Ok(Path(EPath::Star(), pat.1)),
            EPattern::Var(x) => Ok(x),
            EPattern::Annot(x, _) => Self::try_from(*x),
            _ => Err(()),
        }
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum EPreterm {
    Pi(Box<Pattern>, Box<Preterm>, Box<Preterm>),
    Lambda(Box<Pattern>, Option<Box<Preterm>>, Box<Preterm>),
    App(Box<Preterm>, Box<Preterm>),
    Var(Path),

    Unit,
    Type(u32),

    Let(Path, Option<Box<Preterm>>, Box<Preterm>, Box<Preterm>),

    Match(Box<Preterm>, Vec<(Pattern, Preterm)>),

    True,
    False,
    If(Box<Preterm>, Box<Preterm>, Box<Preterm>),
    Bool,

    TAnnot(Box<Preterm>, Box<Preterm>),
}
#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub struct Preterm(pub EPreterm, pub Option<logos::Span>);

#[derive(Clone, PartialEq, Debug)]
pub enum EStatement {
    ExprStmt(Preterm),
    FnStmt(String, Vec<Pattern>, Option<Preterm>, Preterm),

    Data(Path, Preterm, Vec<(Path, Preterm)>),
    DataAlias(Path, Box<Preterm>),
}
#[derive(Clone, PartialEq, Debug)]
pub struct Statement(pub EStatement, pub Option<logos::Span>);
pub struct Statements(pub Vec<Statement>);

impl std::cmp::PartialEq for EPath {
    fn eq(&self, other: &EPath) -> bool {
        match (&self, &other) {
            (EPath::Var(x), EPath::Var(y)) => x == y,
            _ => false, // TODO: implement lookup of symbols and stuff
        }
    }
}
impl std::cmp::Eq for EPath {}
impl std::cmp::PartialEq for Path {
    fn eq(&self, other: &Path) -> bool {
        self.0 == other.0
    }
}
impl std::cmp::Eq for Path {}

impl Pattern {
    pub fn is_wildcard(&self) -> bool {
        match &self.0 {
            EPattern::Wildcard => true,
            _ => false,
        }
    }
}

impl fmt::Display for Preterm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl fmt::Display for EPreterm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            EPreterm::Type(0) => write!(f, "Type"),
            EPreterm::Type(n) => write!(f, "Type {}", n),
            EPreterm::True => write!(f, "true"),
            EPreterm::False => write!(f, "false"),
            EPreterm::Bool => write!(f, "bool"),
            EPreterm::Match(x, pats) => {
                let patsstr = pats.iter().fold("".to_string(), |acc, (pat, e)| {
                    acc + &format!("{} => {}, ", pat, e)
                });
                write!(f, "match {} with [ {}]", x, patsstr)
            }
            EPreterm::If(a, b, c) => write!(f, "if {} then {} else {}", a, b, c),
            EPreterm::Var(x) => write!(f, "{}", x),
            EPreterm::App(a, b) => match (&(*a).0, &(*b).0) {
                (EPreterm::Var(_), EPreterm::Var(_)) => write!(f, "{} {}", a, b),
                (EPreterm::Var(_), _) => write!(f, "{} ({})", a, b),
                (EPreterm::Lambda(_, _, _), _) => write!(f, "({}) {}", a, b),
                (_, EPreterm::App(_, _)) => write!(f, "{} ({})", a, b),
                (_, _) => write!(f, "({} {})", a, b),
            },
            EPreterm::Let(x, ot, a, b) => {
                if let Some(t) = ot {
                    write!(f, "let {} : {} := {}. {}", x, t, a, b)
                } else {
                    write!(f, "let {} := {}. {}", x, a, b)
                }
            }

            EPreterm::Pi(x, t, b) => {
                let binder: Path = (**x).clone().try_into().unwrap();
                let containsbinder = fv(&(*b).0).contains(&binder.0);
                if !x.is_wildcard() && containsbinder {
                    write!(f, "Π{} : {}. {}", x, t.clone(), b)
                } else {
                    let ut = (&*t).clone().0;
                    match (&ut, &(*b).0) {
                        (EPreterm::Pi(_, _, _), EPreterm::Pi(_, _, _)) => {
                            write!(f, "({}) -> {}", t.clone(), b)
                        }
                        (EPreterm::Pi(_, _, _), EPreterm::Unit) => {
                            write!(f, "({}) -> {}", t.clone(), b)
                        }
                        (EPreterm::Pi(_, _, _), _) => {
                            write!(f, "({}) -> ({})", t.clone(), b)
                        }
                        (EPreterm::Unit | EPreterm::Type(_), EPreterm::Pi(_, _, _)) => {
                            write!(f, "{} -> {}", t.clone(), b)
                        }
                        (EPreterm::Var(_), EPreterm::Pi(_, _, _)) => {
                            write!(f, "{} -> {}", t.clone(), b)
                        }
                        (_, EPreterm::Pi(_, _, _)) => {
                            write!(f, "({}) -> {}", t.clone(), b)
                        }
                        (_, _) => write!(f, "{} -> {}", t.clone(), b),
                    }
                }
            }
            EPreterm::Lambda(x, t, b) => {
                if t.is_none() {
                    write!(f, "λ{}. {}", x, b)
                } else {
                    write!(f, "λ{} : {}. {}", x, t.clone().unwrap(), b)
                }
            }
            EPreterm::Unit => write!(f, "()"),
            EPreterm::TAnnot(a, b) => write!(f, "{} : {}", a, b),
        }
    }
}
impl fmt::Display for EPath {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            EPath::Var(x) => write!(f, "{}", x),
            EPath::Star() => write!(f, "*"),
            EPath::PathCons(x, y) => write!(f, "{}::{}", x, y),
        }
    }
}
impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl fmt::Display for Pattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl fmt::Display for EPattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            EPattern::Unit => write!(f, "()"),
            EPattern::True => write!(f, "true"),
            EPattern::False => write!(f, "false"),
            EPattern::Wildcard => write!(f, "_"),
            EPattern::Var(x) => write!(f, "{}", x),
            EPattern::App(x, y) => write!(f, "{} {}", x, y),
            EPattern::Annot(p, t) => write!(f, "({} : {})", p, t),
            EPattern::Tuple(xs) => write!(
                f,
                "({})",
                xs.iter()
                    .fold("".to_string(), |acc, el| format!("{}{}, ", acc, el))
            ),
        }
    }
}
impl fmt::Display for Statement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl fmt::Display for EStatement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            EStatement::ExprStmt(e) => write!(f, "{}", e),
            EStatement::FnStmt(name, pats, rett, bdy) => {
                // TODO: if bdy is a match on an identifier, print this as patterns
                let mut patsstr = "".to_string();
                let mut patidx: isize = pats.len() as isize - 1;
                if !pats.is_empty() {
                    loop {
                        let bot = &pats[patidx as usize];
                        patidx -= 1;
                        if patidx < 0 {
                            patsstr = format!("{}{}", bot, patsstr);
                            break;
                        } else {
                            patsstr = format!(") ({}{}", bot, patsstr);
                        }
                    }
                }
                let rettstr = match rett {
                    Some(r) => format!(": {} ", r),
                    None => "".to_string(),
                };
                write!(f, "fn {} ({}) {}:= {}", name, patsstr, rettstr, bdy)
            }
            EStatement::DataAlias(x, e) => write!(f, "data {} := {}", x, e),
            EStatement::Data(typector, ty, datactors) => {
                let datactortext = datactors.iter().fold(String::new(), |acc, (name, def)| {
                    acc + &String::from(format!("{} : {},", name, def))
                });
                write!(f, "data {} : {} [{}].", typector, ty, datactortext)
            }
        }
    }
}
impl fmt::Display for Statements {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut res = Ok(());
        let mut first = true;
        for s in &self.0 {
            if !first {
                res = res.and_then(|_| write!(f, "\n"));
            }
            res = res.and_then(|_| write!(f, "{}", s));
            first = false;
        }
        res
    }
}

pub fn fn_body(fun: &Preterm) -> &Preterm {
    let mut rec = match &fun.0 {
        EPreterm::Lambda(_, _, b) => Either::Left(b),
        _ => Either::Right(fun),
    };
    while let Either::Left(r) = rec {
        rec = match &r.0 {
            EPreterm::Lambda(_, _, b) => Either::Left(b),
            _ => Either::Right(r),
        }
    }
    let Either::Right(r) = rec else { panic!() };
    r
}

fn fv_detail_pat(e: &EPattern) -> HashSet<EPath> {
    match e {
        EPattern::Var(x) => [x.0.clone()].iter().cloned().collect(),
        EPattern::Tuple(xs) => {
            let mut set = HashSet::new();
            for x in xs.iter() {
                set.extend(fv_detail_pat(&x.0))
            }
            set
        }
        EPattern::Annot(a, t) => {
            let mut set = HashSet::new();
            set.extend(fv_detail_pat(&a.0));
            set.extend(fv_detail(&mut HashSet::new(), &t.0));
            set
        }
        _ => HashSet::new(),
    }
}

fn fv_detail(bound: &mut HashSet<EPath>, e: &EPreterm) -> HashSet<EPath> {
    match e {
        EPreterm::True | EPreterm::False | EPreterm::Unit | EPreterm::Type(_) | EPreterm::Bool => {
            HashSet::new()
        }
        EPreterm::Match(e, pats) => {
            pats.iter()
                .fold(fv_detail(bound, &(*e).0), |acc, (_pat, el)| {
                    // FIXME: add binders from pattern to `bound`
                    let mut acc = acc;
                    acc.extend(fv_detail(bound, &(*el).0));
                    acc
                })
        }
        EPreterm::If(a, b, c) => {
            let mut set = fv_detail(bound, &(*a).0);
            set.extend(fv_detail(bound, &(*b).0));
            set.extend(fv_detail(bound, &(*c).0));
            set
        }
        EPreterm::TAnnot(a, b) => {
            let mut set = fv_detail(bound, &(*a).0);
            set.extend(fv_detail(bound, &(*b).0));
            set
        }
        EPreterm::Var(x) => {
            if bound.contains(&x.0) {
                HashSet::new()
            } else {
                [x.0.clone()].iter().cloned().collect()
            }
        }
        EPreterm::App(a, b) => {
            let mut set = fv_detail(bound, &(*a).0);
            set.extend(fv_detail(bound, &(*b).0));
            set
        }
        EPreterm::Pi(x, t, b) => {
            let mut fvot = fv_detail(bound, &(*t).0);
            let path: Path = (**x).clone().try_into().unwrap();
            bound.insert(path.0.clone());
            fvot.extend(fv_detail(bound, &(*b).0));
            bound.remove(&path.0);
            fvot
        }
        EPreterm::Lambda(x, ot, b) => {
            let mut fvot = HashSet::new();
            match ot {
                None => (),
                Some(t) => {
                    fvot = fv_detail(bound, &(*t).0);
                    ()
                }
            }
            let path: Path = (**x).clone().try_into().unwrap();
            bound.insert(path.0.clone());
            fvot.extend(fv_detail(bound, &(*b).0));
            bound.remove(&path.0);
            fvot
        }
        EPreterm::Let(x, ot, def, bdy) => {
            let mut fvot = HashSet::new();
            match ot {
                None => (),
                Some(t) => {
                    fvot = fv_detail(bound, &(*t).0);
                    ()
                }
            }
            fvot.extend(fv_detail(bound, &(*def).0));
            bound.insert(x.0.clone());
            fvot.extend(fv_detail(bound, &(*bdy).0));
            bound.remove(&x.0);
            fvot
        }
    }
}

pub fn fv_pat(e: &EPattern) -> HashSet<EPath> {
    fv_detail_pat(e)
}

pub fn fv(e: &EPreterm) -> HashSet<EPath> {
    let mut bound: HashSet<EPath> = HashSet::new();
    fv_detail(&mut bound, e)
}
