use codespan_reporting::diagnostic::Diagnostic;

use super::check::Failable;
use super::names::*;

use std::collections::HashMap;
use std::fmt;

pub mod de_brjn {
    use core::fmt;

    #[derive(Copy, Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
    pub struct Idx(pub usize);

    #[derive(Copy, Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
    pub struct Lvl(pub usize);

    impl Lvl {
        pub fn inc(&self) -> Self {
            Lvl(self.0 + 1)
        }
    }

    impl fmt::Display for Idx {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "I{}", self.0)
        }
    }
    impl fmt::Display for Lvl {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "L{}", self.0)
        }
    }

    pub fn lvl2idx(a: Lvl, b: Lvl) -> Idx {
        let (Lvl(a), Lvl(b)) = (a, b);
        Idx(a - b - 1)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct EnvEl {
    pub el: Value,
}
impl fmt::Display for EnvEl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.el)
    }
}
pub fn envel(_name: Path, el: Value) -> EnvEl {
    EnvEl { el }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Env {
    data: Vec<EnvEl>,
    ctors: HashMap<de_brjn::Lvl, Vec<EnvEl>>,
}
pub fn fresh_env() -> Env {
    Env {
        data: vec![],
        ctors: HashMap::new(),
    }
}

impl fmt::Display for Env {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[data({}); ctors({})]",
            self.data
                .iter()
                .fold("".to_string(), |acc, el| format!("{}{}, ", acc, el)),
            self.ctors
                .iter()
                .fold("".to_string(), |acc, (key, el)| format!(
                    "{}{} : {}, ",
                    acc,
                    key,
                    el.iter()
                        .fold("".to_string(), |acc, ctor| format!("{}{}, ", acc, ctor))
                ))
        )
    }
}

impl Env {
    pub fn len(&self) -> usize {
        self.data.len()
    }
    pub fn get(&self, i: usize) -> Failable<EnvEl> {
        self.data.get(i).cloned().map_or(
            Err(Diagnostic::error().with_message("idx out of range")),
            |v| Ok(v),
        )
    }
    pub fn insert(&mut self, el: EnvEl) -> () {
        self.data.push(el);
    }
    pub fn replace(&self, i: de_brjn::Idx, el: EnvEl) -> Self {
        let Env { data: mut v, ctors } = self.clone();

        let de_brjn::Idx(i) = i;
        v[i] = el;
        Env { data: v, ctors }
    }
    pub fn add_ctors(&mut self, key: de_brjn::Lvl, ctors: Vec<EnvEl>) -> () {
        self.ctors.insert(key, ctors);
    }
    pub fn get_ctors(&self, key: de_brjn::Lvl) -> Vec<EnvEl> {
        self.ctors.get(&key).cloned().unwrap()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum TopLevel {
    Core(Core, Value),
    Data(Path, Vec<(de_brjn::Lvl, Value)>),
}
impl fmt::Display for TopLevel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TopLevel::Core(c, v) => write!(f, "{} : {}.", c, v),
            TopLevel::Data(p, ctors) => {
                let mut ctorstxt = "".to_string();
                let mut first = true;
                for (name, ty) in ctors {
                    if !first {
                        ctorstxt += ", "
                    }
                    first = false;
                    ctorstxt = format!("{}{} : {}", ctorstxt, name, ty);
                }
                write!(f, "data {} [{}].", p, ctorstxt)
            }
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Core {
    Unit,
    Type(u32),
    Bool,
    True,
    False,
    If(Box<Core>, Box<Core>, Box<Core>),

    // FIXME: likely have to store patterns somewhere as well... have patterns in core lang?
    Match(Box<Core>, Vec<(Pattern, Core)>),

    Var(de_brjn::Idx),
    Lam(Path, Box<Core>),
    Pi(Path, Box<Core>, Box<Core>),
    App(Box<Core>, Box<Core>),
    Let(Path, Box<Core>, u32, Box<Core>, Box<Core>),

    Constructor(de_brjn::Lvl),
}
impl fmt::Display for Core {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Core::Unit => write!(f, "()"),
            Core::Type(u) => write!(f, "Type {}", u),
            Core::Bool => write!(f, "Bool"),
            Core::True => write!(f, "true"),
            Core::False => write!(f, "false"),
            Core::If(a, b, c) => write!(f, "if {} then {} else {}", *a, *b, *c),
            Core::Match(a, v) => {
                write!(
                    f,
                    "match {} [{}]",
                    a,
                    v.iter().fold("".to_string(), |acc, (pat, el)| format!(
                        "{}{} => {}, ", // FIXME
                        acc, pat, el
                    ))
                )
            }

            Core::Var(i) => write!(f, "{}", i),
            Core::Pi(x, t, b) => {
                write!(f, "(Π{}:{}.{})", x, t, *b)
            }
            Core::Lam(x, b) => {
                write!(f, "(λ{}.{})", x, *b)
            }
            Core::App(a, b) => write!(f, "({} {})", *a, *b),
            Core::Let(x, _, _, a, b) => write!(f, "let {} := {}. {}", x, *a, *b),

            Core::Constructor(l) => write!(f, "C@{}", l),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Pattern {
    Unit,
    True,
    False,
    Wildcard,
    Constructor(de_brjn::Lvl), // TODO: may need an identifier or something...
    Var(Path),
}
impl fmt::Display for Pattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Pattern::Unit => write!(f, "()"),
            Pattern::True => write!(f, "true"),
            Pattern::False => write!(f, "false"),
            Pattern::Wildcard => write!(f, "_"),
            Pattern::Constructor(l) => write!(f, "C@{}", l),
            Pattern::Var(x) => write!(f, "V@[{}]", x),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Unit,
    Type(u32),
    Bool,
    True,
    False,
    If(Box<Value>, Box<Value>, Box<Value>),

    Match(Box<Value>, Vec<PatClosure>),

    Var(de_brjn::Lvl),
    App(Box<Value>, Box<Value>),
    Lam(Closure),
    Pi(Box<Value>, Closure),

    Constructor(de_brjn::Lvl),
}
impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::Unit => write!(f, "()"),
            Value::Type(u) => write!(f, "Type {}", u),
            Value::Bool => write!(f, "Bool"),
            Value::True => write!(f, "False"),
            Value::False => write!(f, "True"),
            Value::If(a, b, c) => write!(f, "if {} then {} else {}", *a, *b, *c),
            Value::Match(a, v) => write!(
                f,
                "match {} [{}]",
                a,
                v.iter().fold("".to_string(), |acc, el| format!(
                    "{}, {}", // FIXME
                    acc, el
                ))
            ),

            Value::Var(p) => write!(f, "{}", p),
            Value::App(a, b) => write!(f, "{} {}", *a, *b),
            Value::Lam(cls) => write!(f, "λ{}", cls),
            Value::Pi(ty, cls) => write!(f, "Π{}->{}", ty, cls),

            Value::Constructor(l) => write!(f, "C@{}", l),
        }
    }
}
#[derive(Clone, Debug, PartialEq)]
pub struct Closure {
    pub binder: Path,
    pub env: Env,
    pub body: Core,
}
impl fmt::Display for Closure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "λ{}@{}. {}", self.binder, self.env, self.body)
    }
}
#[derive(Clone, Debug, PartialEq)]
pub struct PatClosure {
    pub pat: Pattern,
    pub env: Env,
    pub body: Core,
}
impl fmt::Display for PatClosure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "λ{}@{}. {}", self.pat, self.env, self.body)
    }
}

pub fn appcls(cls: Closure, v: Value, t: Value) -> Failable<Value> {
    let mut env = cls.env;
    env.insert(envel(cls.binder, v));
    eval(env, cls.body, t)
}

pub fn match_with_pattern(cls: PatClosure, v: Value, t: Value) -> Failable<Value> {
    let mut env = cls.env;
    match (cls.pat, v) {
        (Pattern::Unit, Value::Unit)
        | (Pattern::True, Value::True)
        | (Pattern::False, Value::False)
        | (Pattern::Wildcard, _) => eval(env, cls.body, t),
        (Pattern::Constructor(x), Value::Constructor(lvl)) => {
            if x == lvl {
                eval(env, cls.body, t)
            } else {
                return Err(Diagnostic::error().with_message("not matching"));
            }
        }
        (Pattern::Var(x), v) => {
            env.insert(envel(x, v));
            eval(env, cls.body, t)
        }
        _ => return Err(Diagnostic::error().with_message("not matching")),
    }
}

pub fn eval(env: Env, e: Core, t: Value) -> Failable<Value> {
    // eval t first? -> problem: what type does it have: likely should just Kind... or something
    match (e, t) {
        (Core::Unit, Value::Unit) => Ok(Value::Unit),
        (Core::Unit, Value::Type(_)) => Ok(Value::Unit),
        (Core::Type(u), Value::Type(_)) => Ok(Value::Type(u)),
        (Core::Bool, Value::Type(_)) => Ok(Value::Bool),
        (Core::True, Value::Bool) => Ok(Value::True),
        (Core::False, Value::Bool) => Ok(Value::False),
        (Core::If(a, b, c), t) => {
            let a = eval(env.clone(), *a, Value::Bool)?;
            match a {
                Value::True => eval(env.clone(), *b, t),
                Value::False => eval(env.clone(), *c, t),
                _ => {
                    let b = eval(env.clone(), *b, t.clone())?;
                    let c = eval(env.clone(), *c, t)?;
                    Ok(Value::If(Box::new(a), Box::new(b), Box::new(c)))
                }
            }
        }
        (Core::Match(x, vs), t) => {
            let a = eval(env.clone(), *x, t.clone())?;
            match t {
                Value::Constructor(_tctorlvl) => {
                    let mut nv = vec![];
                    for (pat, arm_bdy) in vs.into_iter() {
                        let x = PatClosure {
                            pat,
                            env: env.clone(),
                            body: arm_bdy,
                        };
                        nv.push(x);
                    }
                    // check if it matches any pattern. if yes, bail out, if no, build a full match
                    // as neutral term, since it cannot be reduced further
                    for pc in nv.iter() {
                        if let Ok(matches) = match_with_pattern(pc.clone(), a.clone(), t.clone()) {
                            return Ok(matches);
                        }
                    }
                    Ok(Value::Match(Box::new(a), nv))
                }
                _ => panic!("{} is not a constructor.", t),
            }
        }
        (Core::Constructor(l), t) => Ok(Value::Constructor(l)),

        (Core::Var(de_brjn::Idx(x)), _t) => Ok(env.get(x)?.el),
        (Core::App(a, b), t) => {
            // TODO: decompose t into A -> B
            let a = eval(env.clone(), *a, t.clone())?;
            let b = eval(env, *b, t.clone())?;

            match a {
                Value::Lam(cls) => appcls(cls, b, t),
                _ => Ok(Value::App(Box::new(a), Box::new(b))),
            }
        }
        (Core::Pi(x, t, b), tt) => Ok(Value::Pi(
            Box::new(eval(env.clone(), *t, tt)?),
            Closure {
                binder: x,
                env,
                body: *b,
            },
        )),
        (Core::Lam(x, b), _t) => Ok(Value::Lam(Closure {
            binder: x,
            env,
            body: *b,
        })),
        (Core::Let(x, _, _, a, b), t) => {
            let u = eval(env.clone(), *a, t.clone())?;

            let mut env = env;
            env.insert(envel(x, u));
            eval(env, *b, t)
        }
        _ => panic!(),
    }
}
pub fn quote(lvl: de_brjn::Lvl, v: Value, t: Value) -> Failable<Core> {
    match (v, t) {
        (Value::Unit, Value::Unit) => Ok(Core::Unit),
        (Value::Type(u), Value::Type(_)) => Ok(Core::Type(u)),
        (Value::Bool, Value::Type(_)) => Ok(Core::Bool),
        (Value::True, Value::Bool) => Ok(Core::True),
        (Value::False, Value::Bool) => Ok(Core::False),
        (Value::If(a, b, c), t) => Ok(Core::If(
            Box::new(quote(lvl, *a, Value::Bool)?),
            Box::new(quote(lvl, *b, t.clone())?),
            Box::new(quote(lvl, *c, t)?),
        )),
        (Value::Match(a, v), t) => {
            // TODO: use extra handler for patterns

            let mut nv = vec![];
            for x in v.into_iter() {
                // TODO/FIXME: lvl may need to change depending on the patterns!
                //  `---> closures?
                //nv.push(quote(lvl, x, t)?);
            }
            let v = nv;
            Ok(Core::Match(Box::new(quote(lvl, *a, t)?), v))
        }
        (Value::Constructor(l), t) => Ok(Core::Constructor(l)),

        (Value::Var(x), _t) => Ok(Core::Var(de_brjn::lvl2idx(lvl, x))),
        (Value::App(a, b), t) => Ok(Core::App(
            Box::new(quote(lvl, *a, t.clone())?),
            Box::new(quote(lvl, *b, t)?),
        )),
        (Value::Pi(argt, cls), t) => Ok(Core::Pi(
            cls.binder.clone(),
            // TODO: decompose t
            Box::new(quote(lvl, *argt, t.clone())?),
            Box::new(quote(
                lvl.inc(),
                appcls(cls, Value::Var(lvl), t.clone())?,
                t,
            )?),
        )),
        (Value::Lam(cls), t) => Ok(Core::Lam(
            cls.binder.clone(),
            Box::new(quote(
                lvl.inc(),
                appcls(cls, Value::Var(lvl), t.clone())?,
                t,
            )?),
        )),
        _ => panic!(),
    }
}
pub fn norm(env: Env, e: Core, t: Value) -> Failable<Core> {
    let lvl = env.len();
    let v = eval(env, e, t.clone())?;

    quote(de_brjn::Lvl(lvl), v, t)
}
