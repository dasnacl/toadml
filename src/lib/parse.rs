use std::collections::VecDeque;

use codespan_reporting::diagnostic::{Diagnostic, Label};

use logos::Logos;

use super::lex::Token;
use super::names::*;

macro_rules! rc {
    ( $id0 : expr, $id1 : expr ) => {
        std::cmp::min($id0.start, $id1.start)..std::cmp::max($id0.end, $id1.end)
    };
}

#[derive(Debug, Clone, PartialEq)]
struct ParseState {
    stream: VecDeque<(Token, logos::Span)>,
    prev_tok: Option<(Token, logos::Span)>,
}
fn new_parse_state(deq: VecDeque<(Token, logos::Span)>) -> ParseState {
    ParseState {
        stream: deq,
        prev_tok: None,
    }
}

fn expect(dat: &mut ParseState, what: Token) -> Result<(Token, logos::Span), Diagnostic<()>> {
    match dat.stream.front().cloned() {
        Some((x, loc)) => {
            dat.prev_tok = Some((x.clone(), loc.clone()));
            dat.stream.pop_front();
            if x == what {
                Ok((x, loc))
            } else {
                Err(Diagnostic::error()
                    .with_code("P-EXP")
                    .with_message("unexpected token")
                    .with_labels(vec![Label::primary((), loc)
                        .with_message(format!("Expected {} but got {}!", what, x))]))
            }
        }
        None => Err(Diagnostic::error()
            .with_code("P-EOF")
            .with_message("unexpected end of file")
            .with_labels(vec![Label::primary((), 0..0).with_message(format!(
                "Expected {} but reached end of input!",
                what
            ))])),
    }
}
fn peek(dat: &mut ParseState, what: Token) -> bool {
    match dat.stream.front().cloned() {
        Some((x, _)) => x == what,
        None => false,
    }
}
fn accept(dat: &mut ParseState, what: Token) -> bool {
    match dat.stream.front().cloned() {
        Some((x, l)) => {
            if x == what {
                dat.prev_tok = Some((x, l));
                dat.stream.pop_front();
                true
            } else {
                false
            }
        }
        None => false,
    }
}
fn eat(dat: &mut ParseState) -> Result<(Token, logos::Span), Diagnostic<()>> {
    match dat.stream.front().cloned() {
        Some(x) => {
            dat.prev_tok = Some(x.clone());
            dat.stream.pop_front();
            Ok(x)
        }
        None => Err(Diagnostic::error()
            .with_code("P-EOF")
            .with_message("unexpected end of file")
            .with_labels(vec![Label::primary((), 0..0)
                .with_message(format!("Unexpectedly reached the end of input!"))])),
    }
}

fn eatid(data: &mut ParseState) -> Result<(String, logos::Span), Diagnostic<()>> {
    match data.stream.front().cloned() {
        Some((Token::Identifier(x), y)) => {
            data.prev_tok = Some((Token::Identifier(x.clone()), y.clone()));
            data.stream.pop_front();
            Ok((x.clone(), y))
        }
        None => Err(Diagnostic::error()
            .with_code("P-EOF")
            .with_message("unexpected end of file")
            .with_labels(vec![Label::primary((), 0..0).with_message(format!(
                "Expected an identifier but reached end of input!"
            ))])),
        Some((x, y)) => Err(Diagnostic::error()
            .with_code("P-EXP")
            .with_message("unexpected token")
            .with_labels(vec![Label::primary((), y)
                .with_message(format!("Expected an identifier, but got {}!", x))])),
    }
}

fn delimiting(dat: &ParseState) -> bool {
    match dat.stream.front().cloned() {
        Some((Token::RParen, _))
        | Some((Token::Dot, _))
        | Some((Token::Walrus, _))
        | Some((Token::RBracket, _))
        | Some((Token::Comma, _))
        | Some((Token::Then, _))
        | Some((Token::And, _))
        | Some((Token::With, _))
        | Some((Token::Fun, _))
        | Some((Token::DArrow, _))
        | Some((Token::Else, _)) => true,
        None => true,
        _ => false,
    }
}

fn parse_path(dat: &mut ParseState, last: String) -> Result<Path, Diagnostic<()>> {
    let mut current = vec![(last, dat.prev_tok.clone().unwrap().1)];
    while accept(dat, Token::DoubleColon) {
        if accept(dat, Token::Star) {
            let loc = dat.prev_tok.clone().unwrap().1;
            return Ok(current
                .into_iter()
                .rfold(Path(EPath::Star(), Some(loc)), |acc, (el, elloc)| {
                    Path(EPath::PathCons(el, Box::new(acc)), Some(elloc))
                }));
        } else {
            let eaten = eatid(dat)?;
            let (id, loc) = eaten;

            current.push((id, loc));
        }
    }
    Ok(current
        .into_iter()
        .rfold(None, |acc, (el, elloc)| match acc {
            None => Some(Path(EPath::Var(el), Some(elloc))),
            Some(acc) => Some(Path(EPath::PathCons(el, Box::new(acc)), Some(elloc))),
        })
        .unwrap())
}

fn parse_prefix(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let (tok, loc) = eat(dat)?;
    match tok {
        Token::True => Ok(Preterm(EPreterm::True, Some(loc))),
        Token::False => Ok(Preterm(EPreterm::False, Some(loc))),
        Token::Bool => Ok(Preterm(EPreterm::Bool, Some(loc))),
        Token::Type(lv) => Ok(Preterm(EPreterm::Type(lv), Some(loc))),
        Token::Identifier(x) => {
            if x == "Type" {
                Ok(Preterm(EPreterm::Type(0), Some(loc)))
            } else {
                let x = parse_path(dat, x)?;
                let span = x.1.clone().unwrap();
                Ok(Preterm(EPreterm::Var(x), Some(span)))
            }
        }
        Token::Match => parse_match(dat),
        Token::Forall => parse_pi(dat),
        Token::Lambda => parse_lambda(dat),
        Token::Let => parse_let(dat),
        Token::If => parse_if(dat),
        Token::LParen => {
            if accept(dat, Token::RParen) {
                Ok(Preterm(EPreterm::Unit, Some(loc)))
            } else {
                let result = parse_expr(dat)?;
                expect(dat, Token::RParen)?;
                let pos = result.1.unwrap();
                Ok(Preterm(result.0, Some(rc!(pos, loc))))
            }
        }
        _ => Err(Diagnostic::error()
            .with_code("P-PREF")
            .with_message("prefix expression opener expected")
            .with_labels(vec![Label::primary((), loc).with_message(format!(
                "This is not the beginning of an expression."
            ))])),
    }
}

fn parse_match(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let initloc = dat.prev_tok.clone().unwrap().1;

    let matching_on = parse_expr(dat)?;

    expect(dat, Token::With)?;
    expect(dat, Token::LBracket)?;
    let mut pats = vec![];
    if !accept(dat, Token::RBracket) {
        loop {
            let pat = parse_pattern(dat)?;
            expect(dat, Token::DArrow)?;
            let bdy = parse_expr(dat)?;

            pats.push((pat, bdy));
            if !accept(dat, Token::Comma) {
                break;
            }
        }
        expect(dat, Token::RBracket)?;
    }
    let span = rc!(initloc, dat.prev_tok.clone().unwrap().1);

    Ok(Preterm(
        EPreterm::Match(Box::new(matching_on), pats),
        Some(span),
    ))
}

fn parse_expr(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let mut pref = parse_prefix(dat)?;

    while !dat.stream.is_empty() && !delimiting(dat) {
        let prefpos = pref.1.clone().unwrap();
        if accept(dat, Token::Colon) {
            let typ = parse_expr(dat)?;
            let typpos = typ.1.clone().unwrap();
            pref = Preterm(
                EPreterm::TAnnot(Box::new(pref), Box::new(typ)),
                Some(rc!(prefpos, typpos)),
            );
        } else if accept(dat, Token::Arrow) {
            let typ = parse_expr(dat)?;
            let typpos = typ.1.clone().unwrap();
            pref = Preterm(
                EPreterm::Pi(
                    Box::new(Pattern(EPattern::Wildcard, None)),
                    Box::new(pref),
                    Box::new(typ),
                ),
                Some(rc!(prefpos, typpos)),
            );
        } else {
            let other = parse_prefix(dat)?;
            let otherpos = other.1.clone().unwrap();
            pref = Preterm(
                EPreterm::App(Box::new(pref), Box::new(other)),
                Some(rc!(prefpos, otherpos)),
            );
        }
    }
    Ok(pref)
}

fn parse_if(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let ifpos = dat.prev_tok.clone().unwrap().1;

    let cond = parse_expr(dat)?;
    expect(dat, Token::Then)?;
    let cons = parse_expr(dat)?;
    expect(dat, Token::Else)?;
    let alte = parse_expr(dat)?;

    let condpos = cond.1.clone().unwrap();
    let altepos = alte.1.clone().unwrap();

    Ok(Preterm(
        EPreterm::If(Box::new(cond), Box::new(cons), Box::new(alte)),
        Some(rc!(rc!(ifpos, condpos), altepos)),
    ))
}

fn parse_let(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let letpos = dat.prev_tok.clone().unwrap().1;

    let id = {
        let (x, _loc) = eatid(dat)?;

        parse_path(dat, x)?
    };

    let mut typ: Option<Box<Preterm>> = None;
    if accept(dat, Token::Colon) {
        let pref = parse_expr(dat)?;
        typ = Some(Box::new(pref));
    }
    expect(dat, Token::Walrus)?;
    let def = parse_expr(dat)?;
    expect(dat, Token::Dot)?;

    let bdy = parse_expr(dat)?;
    let bdypos = bdy.1.clone().unwrap();
    Ok(Preterm(
        EPreterm::Let(id, typ, Box::new(def), Box::new(bdy)),
        Some(rc!(letpos, bdypos)),
    ))
}

fn parse_param(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let par_opened = accept(dat, Token::LParen);

    let eaten = eatid(dat)?;
    let (eaten, loc) = eaten;

    let name = {
        let x = parse_path(dat, eaten)?;
        let span = x.1.clone().unwrap();
        Preterm(EPreterm::Var(x), Some(span))
    };
    if !par_opened {
        return Ok(name);
    }
    let loc = rc!(loc, dat.prev_tok.clone().unwrap().1);
    expect(dat, Token::Colon)?;

    let typ = parse_expr(dat)?;

    expect(dat, Token::RParen)?;
    let rloc = dat.prev_tok.clone().unwrap().1;
    Ok(Preterm(
        EPreterm::TAnnot(Box::new(name), Box::new(typ)),
        Some(rc!(loc, rloc)),
    ))
}

fn parse_ctor_def(dat: &mut ParseState, typector: Path) -> Result<(Path, Preterm), Diagnostic<()>> {
    let eaten = eatid(dat)?;
    let (id, loc) = eaten;
    let id = parse_path(dat, id)?;

    // TODO: add ctors to symboltable
    if id == typector {
        return Err(Diagnostic::error()
            .with_code("P-TSH")
            .with_message("type constructor shadowed")
            .with_labels(vec![
                Label::primary((), loc).with_message(format!("{} is known as a type!", id))
            ]));
    }

    let mut v = vec![];
    if accept(dat, Token::Colon) {
        v.push(parse_expr(dat)?);
        accept(dat, Token::Comma);
    } else {
        while !accept(dat, Token::Comma) {
            if dat.stream.is_empty() || peek(dat, Token::Colon) || peek(dat, Token::RBracket) {
                break;
            }
            v.push(parse_param(dat)?);
        }
        if accept(dat, Token::Colon) {
            v.push(parse_expr(dat)?);
            accept(dat, Token::Comma);
        }
    }
    let ret = {
        if v.is_empty() {
            Preterm(EPreterm::Var(typector), Some(loc.clone()))
        } else if v.len() == 1 {
            // FIXME: what if this is a typeconstructor that takes arguments?
            let x = v.pop().unwrap();
            let mut usex = false;
            match &fn_body(&x).0 {
                EPreterm::Var(y) => {
                    // FIXME: use equality check. to split properly for sema,
                    //        maybe add special node representing the return part
                    //        sema would then take care of setting that placeholder
                    if typector == *y {
                        usex = true
                    } else {
                    }
                }
                _ => (),
            };
            if usex {
                x
            } else {
                Preterm(
                    EPreterm::Pi(
                        Box::new(Pattern(EPattern::Wildcard, None)),
                        Box::new(x),
                        Box::new(Preterm(EPreterm::Var(typector), Some(loc))),
                    ),
                    None,
                )
            }
        } else {
            // TODO: general solution with foldl
            panic!()
        }
    };
    Ok((id, ret))
}

fn parse_data(dat: &mut ParseState) -> Result<Statement, Diagnostic<()>> {
    let eaten = eatid(dat)?;
    let (id, loc) = eaten;
    let id = parse_path(dat, id)?;

    if accept(dat, Token::Walrus) {
        // data alias, e.g., `data stack := list A`
        let x = parse_expr(dat)?;
        let loc = rc!(id.1.clone().unwrap(), x.1.clone().unwrap());
        Ok(Statement(EStatement::DataAlias(id, Box::new(x)), Some(loc)))
    } else {
        expect(dat, Token::Colon)?;
        let ty = parse_expr(dat)?;
        expect(dat, Token::Walrus)?;
        expect(dat, Token::LBracket)?;

        let mut v = vec![];
        while !peek(dat, Token::RBracket) {
            v.push(parse_ctor_def(dat, id.clone())?);
        }
        let (_, endpos) = expect(dat, Token::RBracket)?;

        Ok(Statement(
            EStatement::Data(id, ty, v),
            Some(rc!(loc, endpos)),
        ))
    }
}

fn parse_lambda(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let id = parse_pattern(dat)?;
    let idpos = id.1.clone().unwrap();

    let (id, typ) = match id {
        Pattern(EPattern::Annot(a, t), _l) => (Pattern(a.0, a.1), Some(Box::new(t))),
        _ => (id, None),
    };
    expect(dat, Token::Dot)?;
    let bdy = parse_expr(dat)?;

    let bdypos = bdy.1.clone().unwrap();

    Ok(Preterm(
        EPreterm::Lambda(Box::new(id), typ, Box::new(bdy)),
        Some(rc!(idpos, bdypos)),
    ))
}
fn parse_pi(dat: &mut ParseState) -> Result<Preterm, Diagnostic<()>> {
    let id = parse_pattern(dat)?;
    let idpos = id.1.clone().unwrap();

    let (id, typ) = match id {
        Pattern(EPattern::Annot(a, t), _l) => (Pattern(a.0, a.1), Some(Box::new(t))),
        _ => return Err(Diagnostic::error().with_message("type annotation expected")),
    };
    expect(dat, Token::Dot)?;
    let bdy = parse_expr(dat)?;

    let bdypos = bdy.1.clone().unwrap();

    Ok(Preterm(
        EPreterm::Pi(Box::new(id), typ.unwrap(), Box::new(bdy)),
        Some(rc!(idpos, bdypos)),
    ))
}

fn parse_prefix_pattern(dat: &mut ParseState) -> Result<Pattern, Diagnostic<()>> {
    let (tok, loc) = eat(dat)?;
    match tok {
        Token::True => Ok(Pattern(EPattern::True, Some(loc))),
        Token::False => Ok(Pattern(EPattern::False, Some(loc))),
        Token::Identifier(x) => {
            let x = parse_path(dat, x)?;
            let span = x.1.clone().unwrap();
            Ok(Pattern(EPattern::Var(x), Some(span)))
        }
        Token::Wildcard => Ok(Pattern(EPattern::Wildcard, Some(loc))),
        Token::LParen => {
            if accept(dat, Token::RParen) {
                Ok(Pattern(EPattern::Unit, Some(loc)))
            } else {
                let result = parse_pattern(dat)?;
                expect(dat, Token::RParen)?;
                let pos = result.1.unwrap();
                Ok(Pattern(result.0, Some(rc!(pos, loc))))
            }
        }
        _ => Err(Diagnostic::error()
            .with_code("P-PTEF")
            .with_message("prefix pattern opener expected")
            .with_labels(vec![Label::primary((), loc)
                .with_message(format!("This is not the beginning of a pattern."))])),
    }
}

fn parse_pattern(dat: &mut ParseState) -> Result<Pattern, Diagnostic<()>> {
    let mut pref = parse_prefix_pattern(dat)?;

    while !dat.stream.is_empty() && !delimiting(dat) {
        let prefpos = pref.1.clone().unwrap();
        if accept(dat, Token::Colon) {
            let typ = parse_expr(dat)?;
            let typpos = typ.1.clone().unwrap();
            pref = Pattern(
                EPattern::Annot(Box::new(pref), typ),
                Some(rc!(prefpos, typpos)),
            );
        } else {
            let other = parse_prefix_pattern(dat)?;
            let otherpos = other.1.clone().unwrap();
            pref = Pattern(
                EPattern::App(Box::new(pref), Box::new(other)),
                Some(rc!(prefpos, otherpos)),
            );
        }
    }
    Ok(pref)
}

fn parse_fun(dat: &mut ParseState) -> Result<Statement, Diagnostic<()>> {
    let loc = dat.prev_tok.clone().unwrap().1;

    let eaten = eatid(dat)?;
    let (id, _) = eaten;

    let mut repeat = true;
    let mut heads_and_bodies = vec![];

    while repeat {
        repeat = false;

        let mut patterns = vec![];
        expect(dat, Token::LParen)?;
        if accept(dat, Token::RParen) {
            patterns.push(Pattern(EPattern::Unit, None));
        } else {
            loop {
                let pat = parse_pattern(dat)?;
                if let EPattern::Unit = pat.0 {
                } else {
                    patterns.push(pat);
                }
                expect(dat, Token::RParen)?;
                if !accept(dat, Token::LParen) {
                    break;
                }
            }
        }
        expect(dat, Token::Walrus)?;

        heads_and_bodies.push((patterns, parse_expr(dat)?));

        // this is a hack to get rid of a warning
        repeat = repeat || accept(dat, Token::And);
    }
    let mut otyp: Option<Preterm> = None;
    if accept(dat, Token::Colon) {
        otyp = Some(parse_expr(dat)?);
    }
    let span = rc!(loc, dat.prev_tok.clone().unwrap().1);

    if !heads_and_bodies.is_empty() {
        let (params, expr) = if heads_and_bodies.len() == 1 {
            heads_and_bodies[0].clone()
        } else {
            // FIXME: add typeannots to $
            let param_name = Path(EPath::Var("$".to_string()), None);
            (
                vec![Pattern(EPattern::Var(param_name.clone()), None)],
                Preterm(
                    EPreterm::Match(
                        Box::new(Preterm(EPreterm::Var(param_name), None)),
                        heads_and_bodies
                            .into_iter()
                            .map(|(v, e)| (Pattern(EPattern::Tuple(v), None), e))
                            .collect(),
                    ),
                    None,
                ),
            )
        };
        Ok(Statement(
            EStatement::FnStmt(id, params, otyp, expr),
            Some(span),
        ))
    } else {
        Err(Diagnostic::error()
            .with_code("P-FNBDY")
            .with_message("function body expected")
            .with_labels(vec![
                Label::primary((), loc).with_message(format!("Function body is missing."))
            ]))
    }
}

fn parse_stmt(dat: &mut ParseState) -> Result<Statement, Diagnostic<()>> {
    if accept(dat, Token::Fun) {
        let x = parse_fun(dat)?;
        expect(dat, Token::Dot)?;
        return Ok(x);
    }
    if accept(dat, Token::Data) {
        let x = parse_data(dat)?;
        expect(dat, Token::Dot)?;
        return Ok(x);
    }
    let ex = parse_expr(dat)?;
    let span = ex.1.clone();

    expect(dat, Token::Dot)?;
    Ok(Statement(EStatement::ExprStmt(ex), span))
}

pub fn parse(text: String) -> Result<Statements, Vec<Diagnostic<()>>> {
    let lex = Token::lexer(text.as_str());
    let mut lexer_errs = vec![];
    let deque: VecDeque<(Token, logos::Span)> = lex
        .spanned()
        .flat_map(|(rt, r)| match rt {
            Ok(rt) => Some((rt, r)),
            Err(_) => {
                lexer_errs.push(
                    Diagnostic::error()
                        .with_code("LX-ERR")
                        .with_message("Failed to lex token.") // TODO: modify logos errors to be more useful
                        .with_labels(vec![
                            Label::primary((), r).with_message("Failed to lex this token.")
                        ]),
                );
                None
            }
        })
        .collect();
    if !lexer_errs.is_empty() {
        return Err(lexer_errs);
    }

    let mut ps = new_parse_state(deque);
    let mut stmts = vec![];
    loop {
        if ps.stream.is_empty() {
            break;
        }
        stmts.push(parse_stmt(&mut ps).map_err(|r| vec![r])?);
    }
    Ok(Statements(stmts))
}
