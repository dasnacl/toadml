use codespan_reporting::files::SimpleFile;
use codespan_reporting::term;
use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};

use colored::Colorize;
use rustyline::error::ReadlineError;
use rustyline::history::FileHistory;
use rustyline::validate::*;
use rustyline::{Completer, CompletionType, Config, EditMode, Helper, Highlighter, Hinter};

use home::home_dir;

use super::check::{empty_gamma, stmts_check, Gamma};
use super::names::EStatement;
use super::parse::parse;

use super::nbe;

use std::path::PathBuf;

#[derive(Helper, Completer, Hinter, Highlighter)]
struct InputValidator {}

// TODO: Try to add completion https://github.com/kkawakam/rustyline/blob/master/examples/diy_hints.rs

impl Validator for InputValidator {
    fn validate(&self, ctx: &mut ValidationContext) -> rustyline::Result<ValidationResult> {
        use ValidationResult::{Incomplete, Invalid, Valid};

        let input = ctx.input();
        let result = if !input.ends_with(".") && !input.ends_with("\n\n") {
            Incomplete
        } else {
            match parse(input.to_string()) {
                Ok(_) => {
                    //self.editor.borrow_mut().add_history_entry(input).unwrap();
                    Valid(None)
                }
                Err(msgs) => {
                    //self.editor.borrow_mut().add_history_entry(input).unwrap();

                    let writer = StandardStream::stderr(ColorChoice::Always);
                    let config = codespan_reporting::term::Config::default();
                    let file = SimpleFile::new("<repl>", ctx.input());
                    for msg in msgs.into_iter() {
                        term::emit(&mut writer.lock(), &config, &file, &msg).unwrap()
                    }
                    Invalid(None)
                }
            }
        };
        Ok(result)
    }
}

pub struct REPL {
    editor: rustyline::Editor<InputValidator, FileHistory>,
    history_path: String,
    writer: StandardStream,
    config: codespan_reporting::term::Config,

    gamma: Gamma,
}

pub fn mk_repl() -> REPL {
    let config = Config::builder()
        .history_ignore_space(true)
        .completion_type(CompletionType::List)
        .edit_mode(EditMode::Emacs)
        .auto_add_history(true)
        .build();
    let history_path_detail = home_dir().unwrap().as_path().join(".toadml_history");
    let history_path = history_path_detail.as_os_str().to_str().unwrap();

    let mut r = REPL {
        editor: rustyline::Editor::<InputValidator, FileHistory>::with_config(config).unwrap(),
        history_path: history_path.to_string(),
        writer: StandardStream::stderr(ColorChoice::Always),
        config: codespan_reporting::term::Config::default(),

        gamma: empty_gamma(),
    };

    let v = InputValidator {};
    r.editor.set_helper(Some(v));
    r.editor.load_history(history_path).unwrap_or(());
    r
}

impl REPL {
    pub fn load(&mut self, args: Vec<PathBuf>) -> () {
        for p in args.into_iter() {
            let content = std::fs::read_to_string(p).expect("File not found.");
            for line in content.split(".") {
                if !line.contains(|c: char| !c.is_whitespace()) {
                    continue;
                }
                self.eval(line.to_string() + ".");
            }
        }
    }

    pub fn run(&mut self) {
        println!(
            r#"
           ,,
        ,/ .\-,     _____               __    ,------------,
        .-(Oo /    |_   _|             | |    | Welcome to |
      /_  __\        | | ___   __ _  __| |    |  Tadpole,  |
     |  )/  /        | |/ _ \ / _` |/ _` |    |  the Toad  |
      }} /|__/        | | (_) | (_| | (_| |    |    REPL    |
      '--'  '        \_/\___/ \__,_|\__,_\    '------------'
    ===========================================================
    "#
        );
        loop {
            let readline = self.editor.readline("~~o ");
            match readline {
                Ok(line) => {
                    if line.is_empty() {
                        continue;
                    }
                    self.eval(line);
                }
                Err(ReadlineError::Interrupted) => {
                    println!("Quit");
                    break;
                }
                Err(ReadlineError::Eof) => {
                    println!("Quit");
                    break;
                }
                Err(err) => {
                    println!("Error: {:?}", err);
                    break;
                }
            }
        }
        self.editor.save_history(&self.history_path).unwrap_or(());
    }

    fn eval(&mut self, text: String) {
        let file = SimpleFile::new("<repl>", text.clone());
        match parse(text) {
            Ok(parsed) => {
                if parsed.0.is_empty() {
                    return;
                }
                let is_exprstmt = match &parsed.0.last().unwrap().0 {
                    EStatement::ExprStmt(_x) => true,
                    _ => false,
                };
                let checkd = stmts_check(&parsed, &mut self.gamma);
                let (term, typ) = match checkd {
                    Ok(v) => {
                        let x = v.first().unwrap();
                        match x {
                            nbe::TopLevel::Core(term, typ) => match (
                                nbe::norm(self.gamma.env(), term.clone(), typ.clone()),
                                nbe::quote(self.gamma.lvl(), typ.clone(), nbe::Value::Type(0)),
                            ) {
                                (Ok(term), Ok(typ)) => (term, typ),
                                (a,b) => panic!(
                                    "gamma is {} and term is {} and typ is {}; norm is {:?} and quote is {:?}",
                                    self.gamma, term, typ, a, b
                                ),
                            },
                            nbe::TopLevel::Data(_, _) => return,
                            _ => panic!(),
                        }
                    }
                    Err(msgs) => {
                        for msg in msgs.into_iter() {
                            term::emit(&mut self.writer.lock(), &self.config, &file, &msg).unwrap()
                        }
                        return;
                    }
                };

                if is_exprstmt {
                    println!(
                        "• {} {} {} {}",
                        "⊢".bold(),
                        format!("{}", term).bright_black(),
                        "==>".bold(),
                        typ
                    );
                } else {
                    println!("OK");
                }
            }
            Err(msgs) => {
                for msg in msgs.into_iter() {
                    term::emit(&mut self.writer.lock(), &self.config, &file, &msg).unwrap()
                }
            }
        }
    }
}
