use logos::Logos;
use std::fmt;

#[derive(Logos, Debug, PartialEq, Clone)]
#[logos(skip r"[ \t\n\f]+")]
pub enum Token {
    // Tokens can be literal strings, of any length.
    #[token("λ")]
    #[token("\\")]
    Lambda,
    #[token(".")]
    Dot,
    #[token("->")]
    Arrow,
    #[token("forall")]
    #[token("Π")]
    Forall,

    #[token("=>")]
    DArrow,
    #[token(",")]
    Comma,
    #[token("(")]
    LParen,
    #[token(")")]
    RParen,
    #[token("[")]
    LBracket,
    #[token("]")]
    RBracket,
    #[token("::")]
    DoubleColon,
    #[token(":")]
    Colon,
    #[token(":=")]
    Walrus,

    #[token("*")]
    Star,
    #[regex("Type", |_lex| Some(0))]
    #[regex("Type [0-9][1-9]*", |lex| { let slice = lex.slice(); slice[5..slice.len()].parse::<u32>().unwrap() })]
    Type(u32),
    #[regex("let")]
    Let,

    #[regex("true")]
    True,
    #[regex("false")]
    False,
    #[regex("if")]
    If,
    #[regex("then")]
    Then,
    #[regex("else")]
    Else,
    #[regex("bool")]
    Bool,
    #[regex("and")]
    And,
    #[regex("match")]
    Match,
    #[regex("with")]
    With,

    #[regex("fn")]
    Fun,

    #[regex("data")]
    Data,

    #[regex("_")]
    Wildcard,

    #[regex("[a-zA-Z][_a-zA-Z]*", |lex| lex.slice().parse::<String>().unwrap())]
    Identifier(String),
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Token::Lambda => write!(f, "λ"),
            Token::Dot => write!(f, "."),
            Token::Comma => write!(f, ","),
            Token::LBracket => write!(f, "["),
            Token::RBracket => write!(f, "]"),
            Token::Identifier(x) => write!(f, "{}", x),
            Token::LParen => write!(f, "("),
            Token::RParen => write!(f, ")"),
            Token::Forall => write!(f, "forall"),
            Token::Colon => write!(f, ":"),
            Token::Arrow => write!(f, "->"),
            Token::DArrow => write!(f, "->"),
            Token::Type(0) => write!(f, "Type"),
            Token::Type(n) => write!(f, "Type {}", n),
            Token::Walrus => write!(f, ":="),
            Token::Let => write!(f, "let"),
            Token::Data => write!(f, "data"),
            Token::True => write!(f, "true"),
            Token::False => write!(f, "false"),
            Token::If => write!(f, "if"),
            Token::Then => write!(f, "then"),
            Token::Else => write!(f, "else"),
            Token::Bool => write!(f, "bool"),
            Token::And => write!(f, "and"),
            Token::Fun => write!(f, "fn"),
            Token::Match => write!(f, "match"),
            Token::With => write!(f, "with"),
            Token::Star => write!(f, "*"),
            Token::DoubleColon => write!(f, "::"),
            Token::Wildcard => write!(f, "_"),
        }
    }
}
