use codespan_reporting::diagnostic::Diagnostic;

use std::fmt;

use super::names::*;
use super::nbe::de_brjn;
use super::nbe::{self, envel, fresh_env};

pub type Var = EPath;
pub type SVar = Path;

pub type Failable<T> = Result<T, Diagnostic<()>>;
pub type VFailable<T> = Result<T, Vec<Diagnostic<()>>>;

#[derive(Debug, Clone)]
pub struct Gamma {
    types: Vec<(SVar, nbe::Value)>,
    env: nbe::Env,
    lvl: de_brjn::Lvl,
}

impl fmt::Display for Gamma {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut texttyps = "".to_string();
        let mut first = true;
        for (x, t) in self.types.iter() {
            if !first {
                texttyps += ", ";
            }
            texttyps = texttyps + &format!("{} : {}", x, t).to_string();
            first = false;
        }
        write!(f, "[{}; {}; lvl={}]", texttyps, self.env, self.lvl)
    }
}

pub fn empty_gamma() -> Gamma {
    Gamma {
        types: vec![],
        env: fresh_env(),
        lvl: de_brjn::Lvl(0),
    }
}
fn bind(x: SVar, v: nbe::Value, gamma: Gamma) -> Gamma {
    let mut typs = gamma.types;
    typs.push((x.clone(), v));

    let mut env = gamma.env;
    env.insert(envel(x, nbe::Value::Var(gamma.lvl)));
    Gamma {
        types: typs,
        env,
        lvl: gamma.lvl.inc(),
    }
}
fn replace(i: nbe::de_brjn::Idx, v: nbe::Value, gamma: Gamma) -> Gamma {
    Gamma {
        types: gamma.types,
        env: gamma.env.replace(i, envel(Path(EPath::Star(), None), v)),
        lvl: gamma.lvl,
    }
}
fn define(x: SVar, t: nbe::Value, v: nbe::Value, gamma: Gamma) -> Gamma {
    let mut typs = gamma.types;
    typs.push((x.clone(), v));

    let mut env = gamma.env;
    env.insert(envel(x, t.clone()));
    Gamma {
        types: typs,
        env,
        lvl: gamma.lvl.inc(),
    }
}
impl Gamma {
    fn lookup(&self, x: &SVar) -> Failable<(nbe::Core, nbe::Value)> {
        let mut ix = 0;
        for (y, t) in self.types.iter() {
            if x.0 == y.0 {
                return Ok((nbe::Core::Var(de_brjn::Idx(ix)), t.clone()));
            }
            ix += 1;
        }
        Err(Diagnostic::error().with_message(format!("failed to lookup {} in {}", x, self)))
    }
    pub fn lvl(&self) -> de_brjn::Lvl {
        self.lvl
    }
    pub fn env(&self) -> nbe::Env {
        self.env.clone()
    }
    fn ctor_of(&self, x: &SVar, tlvl: de_brjn::Lvl) -> Failable<de_brjn::Lvl> {
        let mut ix = 0;
        for (y, t) in self.types.iter() {
            if x.0 == y.0 {
                match t {
                    nbe::Value::Constructor(l) => {
                        return if tlvl == *l {
                            match self.env.get(ix)?.el {
                                nbe::Value::Constructor(x) => Ok(x),
                                _ => panic!(),
                            }
                        } else {
                            Err(Diagnostic::error().with_message("not part of this datatype"))
                        }
                    }
                    _ => {
                        return Err(Diagnostic::error().with_message("not a type with constructors"))
                    }
                }
            }
            ix += 1;
        }
        Err(Diagnostic::error().with_message("unbound identifier"))
    }
}

fn convertible(lvl: de_brjn::Lvl, t0: &nbe::Value, t1: &nbe::Value) -> Failable<()> {
    match (&t0, &t1) {
        (nbe::Value::Unit, nbe::Value::Unit) => Ok(()),
        (nbe::Value::Bool, nbe::Value::Bool) => Ok(()),
        (nbe::Value::Type(i0), nbe::Value::Type(i1)) => {
            if i0 == i1 {
                Ok(())
            } else {
                panic!("universe mismatch {} != {}", i0, i1)
            }
        }
        (nbe::Value::Constructor(t0), nbe::Value::Constructor(t1)) => {
            if t0 == t1 {
                Ok(())
            } else {
                panic!("different constructors: {} <> {}", t0, t1)
            }
        }
        (nbe::Value::Var(x), nbe::Value::Var(y)) => {
            if x == y {
                Ok(())
            } else {
                panic!("different binders: {} <> {}", x, y)
            }
        }
        (nbe::Value::App(a0, b0), nbe::Value::App(a1, b1)) => {
            convertible(lvl, &*a0, &*a1).and(convertible(lvl, &*b0, &*b1))
        }
        (nbe::Value::Pi(t0, cls0), nbe::Value::Pi(t1, cls1)) => {
            let trg0 = nbe::appcls(cls0.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?; // FIXME: correct type?
            let trg1 = nbe::appcls(cls1.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            convertible(lvl, &*t0, &*t1).and(convertible(lvl.inc(), &trg0, &trg1))
        }
        (nbe::Value::Lam(cls0), nbe::Value::Lam(cls1)) => {
            let t0 = nbe::appcls(cls0.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            let t1 = nbe::appcls(cls1.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            convertible(lvl.inc(), &t0, &t1)
        }
        (nbe::Value::Lam(cls), u) => {
            let t0 = nbe::appcls(cls.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            let t1 = nbe::Value::App(Box::new((*u).clone()), Box::new(nbe::Value::Var(lvl)));
            convertible(lvl.inc(), &t0, &t1)
        }
        (u, nbe::Value::Lam(cls)) => {
            let t0 = nbe::Value::App(Box::new((*u).clone()), Box::new(nbe::Value::Var(lvl)));
            let t1 = nbe::appcls(cls.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            convertible(lvl.inc(), &t0, &t1)
        }
        _ => Err(Diagnostic::error().with_message(format!("inconvertible {} <> {}", t0, t1))),
    }
}
fn subty(lvl: de_brjn::Lvl, t0: &nbe::Value, t1: &nbe::Value) -> Failable<()> {
    match (t0, t1) {
        (nbe::Value::Type(i), nbe::Value::Type(j)) => {
            if i <= j {
                Ok(())
            } else {
                Err(Diagnostic::error().with_message("universe inconsistency"))
            }
        }

        (nbe::Value::Pi(a0, cls0), nbe::Value::Pi(a1, cls1)) => {
            let b0 = nbe::appcls(cls0.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            let b1 = nbe::appcls(cls1.clone(), nbe::Value::Var(lvl), nbe::Value::Type(0))?;
            subty(lvl, &*a0, &*a1).and(subty(lvl.inc(), &b0, &b1))
        }
        _ => convertible(lvl, t0, t1),
    }
}

pub fn infer_univ(gamma: Gamma, e0: &Preterm) -> Failable<(nbe::Core, u32)> {
    let (e, t) = infer(gamma.clone(), e0)?;
    match t {
        nbe::Value::Type(i) => Ok((e, i)),
        nbe::Value::Bool => Ok((e, 0)),
        nbe::Value::Unit => Ok((e, 0)),
        nbe::Value::Var(_x) => match nbe::norm(gamma.env, e.clone(), nbe::Value::Type(0))? {
            // FIXME: may need Kind instead of Type(0)?
            nbe::Core::Type(i) => Ok((e, i)),
            _ => panic!("not a type"),
        },
        _ => Err(Diagnostic::error()
            .with_message(format!("Expected a type! {} |- {} :> {}", gamma, e, t))),
    }
}

// TODO: remove this. this will be an artifact as soon as Lambdas are migrated to accept patterns
pub fn check_pat(gamma: Gamma, p: &Pattern, t: &nbe::Value) -> Failable<(Gamma, Path)> {
    let wildcard_var = Path(EPath::Var("$_".to_string()), None);
    match (&p.0, t) {
        (EPattern::Unit, nbe::Value::Unit) => Ok((gamma, wildcard_var)),
        (EPattern::True, nbe::Value::Bool) => Ok((gamma, wildcard_var)),
        (EPattern::False, nbe::Value::Bool) => Ok((gamma, wildcard_var)),
        (EPattern::Wildcard, _) => Ok((gamma, wildcard_var)),
        (EPattern::Annot(p, ty), vt) => {
            let (ty, _) = infer_univ(gamma.clone(), ty)?;
            let vty = nbe::eval(gamma.env.clone(), ty, nbe::Value::Type(0))?;
            subty(gamma.lvl, &vt, &vty)?;
            check_pat(gamma, p, &vty)
        }
        (EPattern::Var(x), _) => Ok((bind(x.clone(), t.clone(), gamma), x.clone())),
        _ => panic!(),
    }
}
pub fn check_pat2(gamma: Gamma, p: &Pattern, t: &nbe::Value) -> Failable<nbe::Pattern> {
    let wildcard_var = Path(EPath::Var("$_".to_string()), None);
    match (&p.0, t) {
        (EPattern::Unit, nbe::Value::Unit) => Ok(nbe::Pattern::Unit),
        (EPattern::True, nbe::Value::Bool) => Ok(nbe::Pattern::True),
        (EPattern::False, nbe::Value::Bool) => Ok(nbe::Pattern::False),
        (EPattern::Wildcard, _) => Ok(nbe::Pattern::Wildcard),
        (EPattern::Annot(p, ty), vt) => {
            let (ty, _) = infer_univ(gamma.clone(), ty)?;
            let vty = nbe::eval(gamma.env.clone(), ty, nbe::Value::Type(0))?;
            subty(gamma.lvl, &vt, &vty)?;
            check_pat2(gamma, p, &vty)
        }
        (EPattern::Var(x), nbe::Value::Constructor(tlvl)) => {
            // FIXME: ctor_of needs to return different error type that we inspect here, we don't
            // want to create a binding for every error it returns, e.g., if the type mismatches
            // from what we feed in: tlvl. This means we have an ill-formed pattern!!
            if let Ok(clvl) = gamma.ctor_of(x, *tlvl) {
                Ok(nbe::Pattern::Constructor(clvl))
            } else {
                Ok(nbe::Pattern::Var(x.clone()))
            }
        }
        _ => panic!(),
    }
}

pub fn check(gamma: Gamma, e: &Preterm, t: &nbe::Value) -> Failable<nbe::Core> {
    match (&e.0, t) {
        (EPreterm::Lambda(x, ot, e), nbe::Value::Pi(argty, cls)) => {
            let check_against =
                nbe::appcls(cls.clone(), nbe::Value::Var(gamma.lvl), nbe::Value::Type(0))?;

            if let Some(t) = ot {
                let (t, _i) = infer_univ(gamma.clone(), &t.clone())?;
                let vt = nbe::eval(gamma.env.clone(), t, nbe::Value::Type(0))?;
                subty(gamma.lvl, &vt, argty)?;
            }
            let (gamma, binder) = check_pat(gamma, x, argty)?;
            Ok(nbe::Core::Lam(
                binder,
                Box::new(check(gamma, e, &check_against)?),
            ))
        }
        (EPreterm::Let(x, ot, edef, euse), ty) => {
            let (a, i) = infer_univ(gamma.clone(), &ot.clone().unwrap())?;
            let va = nbe::eval(gamma.env.clone(), a.clone(), nbe::Value::Type(0))?;
            let t = check(gamma.clone(), edef, &va)?;
            let vt = nbe::eval(gamma.env.clone(), t.clone(), va.clone())?;

            let gamma = define(x.clone(), vt, va, gamma);
            let u = check(gamma, euse, ty)?;
            Ok(nbe::Core::Let(
                x.clone(),
                Box::new(a),
                i,
                Box::new(t),
                Box::new(u),
            ))
        }
        _ => {
            let (u, ty) = infer(gamma.clone(), e)?;
            subty(gamma.lvl, t, &ty)?;
            Ok(u)
        }
    }
}

pub fn infer(gamma: Gamma, e: &Preterm) -> Failable<(nbe::Core, nbe::Value)> {
    match &e.0 {
        EPreterm::Unit => Ok((nbe::Core::Unit, nbe::Value::Unit)),
        EPreterm::True => Ok((nbe::Core::True, nbe::Value::Bool)),
        EPreterm::False => Ok((nbe::Core::False, nbe::Value::Bool)),
        EPreterm::Bool => Ok((nbe::Core::Bool, nbe::Value::Type(0))),
        EPreterm::Type(i) => Ok((nbe::Core::Type(*i), nbe::Value::Type(i + 1))),

        EPreterm::Var(x) => gamma.lookup(x),
        EPreterm::Pi(x, t, e) => {
            let (a, i) = infer_univ(gamma.clone(), &t.clone())?;
            let va = nbe::eval(gamma.env.clone(), a.clone(), nbe::Value::Type(0))?;

            let (gamma, binder) = check_pat(gamma, x, &va)?;
            let (b, j) = infer_univ(gamma, e)?;
            Ok((
                nbe::Core::Pi(binder, Box::new(a), Box::new(b)),
                nbe::Value::Type(if i > j { i } else { j }),
            ))
        }
        EPreterm::Let(x, ot, edef, euse) => {
            let (a, i) = infer_univ(gamma.clone(), &ot.clone().unwrap())?;
            let va = nbe::eval(gamma.env.clone(), a.clone(), nbe::Value::Type(0))?;
            let t = check(gamma.clone(), edef, &va)?;
            let vt = nbe::eval(gamma.env.clone(), t.clone(), va.clone())?;

            let gamma = define(x.clone(), vt, va, gamma);
            let (u, ty) = infer(gamma, euse)?;
            Ok((
                nbe::Core::Let(x.clone(), Box::new(a), i, Box::new(t), Box::new(u)),
                ty,
            ))
        }
        EPreterm::App(f, arg) => {
            let (t, tty) = infer(gamma.clone(), &*f)?;
            match tty {
                nbe::Value::Pi(argty, cls) => {
                    let u = check(gamma.clone(), arg, &argty.clone())?;
                    Ok((
                        nbe::Core::App(Box::new(t), Box::new(u.clone())),
                        nbe::appcls(
                            cls,
                            nbe::eval(gamma.env.clone(), u, *argty.clone())?,
                            nbe::Value::Type(0),
                        )?,
                    ))
                }
                // FIXME: if tty is Type, then u should be a pi type
                _ => Err(Diagnostic::error().with_message(format!(
                    "function type expected, instead got {} with context {}",
                    tty, gamma
                ))),
            }
        }
        EPreterm::Match(on, cases) => {
            let (on, ont) = infer(gamma.clone(), &*on)?;
            let mut typs = vec![];
            let mut terms = vec![];
            for (pat, term) in cases.into_iter() {
                let patt = check_pat2(gamma.clone(), pat, &ont)?;
                let lv = gamma.lvl();
                let (cc, ct) = infer(gamma.clone(), term)?;
                let ct = nbe::quote(lv, ct, nbe::Value::Type(0))?;

                terms.push((patt.clone(), cc));
                typs.push((patt, ct));
            }
            let ty = nbe::Core::Match(Box::new(on.clone()), typs.clone());
            let ty = nbe::eval(gamma.env, ty, ont)?;

            Ok((nbe::Core::Match(Box::new(on), terms.clone()), ty.clone()))
        }
        EPreterm::If(a, b, c) => {
            let (ac, at) = infer(gamma.clone(), a)?;
            match at {
                nbe::Value::Bool => match ac {
                    nbe::Core::Var(x) => {
                        let gamma_true = replace(x.clone(), nbe::Value::True, gamma.clone());
                        let gamma_false = replace(x.clone(), nbe::Value::False, gamma.clone());
                        let (bc, bt) = infer(gamma_true.clone(), b)?;
                        let (cc, ct) = infer(gamma_false.clone(), c)?;

                        let btt = nbe::quote(gamma_true.lvl, bt, nbe::Value::Type(0))?;
                        let ctt = nbe::quote(gamma_false.lvl, ct, nbe::Value::Type(0))?;
                        let ty = nbe::Core::If(Box::new(ac.clone()), Box::new(btt), Box::new(ctt));
                        let ty = nbe::eval(gamma.env, ty, nbe::Value::Type(0))?;

                        Ok((
                            nbe::Core::If(Box::new(ac), Box::new(bc), Box::new(cc)),
                            ty.clone(),
                        ))
                    }
                    _ => {
                        let (bc, bt) = infer(gamma.clone(), b)?;
                        let cc = check(gamma, c, &bt)?;
                        Ok((
                            nbe::Core::If(Box::new(ac), Box::new(bc), Box::new(cc)),
                            bt.clone(),
                        ))
                    }
                },
                _ => panic!("Boolean elimination expects a boolean"),
            }
        }
        EPreterm::TAnnot(e, t) => {
            let (tt, _i) = infer_univ(gamma.clone(), t)?;
            let tt = nbe::eval(gamma.env.clone(), tt, nbe::Value::Type(0))?;
            Ok((check(gamma, e, &tt.clone())?, tt.clone()))
        }
        _ => todo!("{:?}", e),
    }
}

fn stmt_check(gamma: &mut Gamma, stmt: &Statement) -> Failable<nbe::TopLevel> {
    match &stmt.0 {
        EStatement::ExprStmt(e) => {
            let (c, v) = infer(gamma.clone(), e)?;
            Ok(nbe::TopLevel::Core(c, v))
        }
        EStatement::FnStmt(_name, _params, _rett, _bdy) => {
            // TODO: check well-formedness of params
            todo!()
        }
        EStatement::Data(x, ty, ctors) => {
            let mut new_ctors = vec![];
            let (ty, i) = infer_univ(gamma.clone(), ty)?;
            let ty = nbe::eval(gamma.env.clone(), ty.clone(), nbe::Value::Type(0))?;
            let ngamma = bind(x.clone(), ty.clone(), gamma.clone());

            let mut toadd = vec![];
            let de_brjn::Lvl(mut lv) = gamma.lvl;
            for (name, e) in ctors.into_iter() {
                let (c, j) = infer(ngamma.clone(), e)?;
                let cv = nbe::eval(ngamma.env.clone(), c.clone(), nbe::Value::Type(0))?;

                // FIXME: likely need some checks here, e.g., that universes agree
                //        use cv for that!
                toadd.push((
                    name.clone(),
                    nbe::Value::Constructor(de_brjn::Lvl(lv + 1)), // <- value constructor
                    nbe::Value::Constructor(gamma.lvl()),          // <- type constructor
                ));
                new_ctors.push((gamma.lvl, nbe::Value::Constructor(de_brjn::Lvl(lv + 1)))); // <- FIXME?: use lv instead of gamma.lvl
                lv = lv + 1;
            }
            let tlvl = gamma.lvl();
            *gamma = define(
                x.clone(),
                nbe::Value::Constructor(tlvl),
                ty.clone(),
                gamma.clone(),
            );
            for (n, a, b) in toadd.into_iter() {
                *gamma = define(n, a, b, gamma.clone());
            }
            gamma.env.add_ctors(
                tlvl,
                new_ctors
                    .iter()
                    .cloned()
                    .map(|(_, v)| nbe::envel(Path(EPath::Star(), None), v))
                    .collect(),
            );

            let ctors = new_ctors;
            Ok(nbe::TopLevel::Data(x.clone(), ctors))
        }
        EStatement::DataAlias(name, ty) => {
            // TODO:
            todo!()
        }
    }
}

pub fn stmts_check(stmts: &Statements, gamma: &mut Gamma) -> VFailable<Vec<nbe::TopLevel>> {
    let mut errs = vec![];
    let mut toplvl = vec![];
    for s in stmts.0.iter() {
        match stmt_check(gamma, s) {
            Ok(t) => toplvl.push(t),
            Err(x) => errs.push(x),
        }
    }
    if errs.is_empty() {
        if toplvl.is_empty() {
            return Err(vec![Diagnostic::error()
                .with_code("T-EMPTY")
                .with_message("Empty module!")]);
        }
        Ok(toplvl)
    } else {
        Err(errs)
    }
}
