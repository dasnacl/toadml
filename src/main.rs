mod lib;
use lib::repl::mk_repl;

use std::path::PathBuf;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Preload files into the repl
    #[arg(short, long, num_args = 1.., value_delimiter = ' ')]
    load: Vec<PathBuf>,
}

fn main() {
    let args = Cli::parse();
    println!("{:?}", args);

    let mut repl = mk_repl();
    repl.load(args.load);
    repl.run();
}
