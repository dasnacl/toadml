<a name="user-content-readme-top"></a>

[![Apache 2.0 License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://codeberg.org/dasnacl/toadml">
    <img src="assets/real_toad.png" alt="Logo" width="256" height="256">
  </a>

  <h3 align="center">ToadML</h3>

  <p align="center">
    A multi-language programming language for security that enables verification-driven development.
    <br />
    <a href="https://codeberg.org/dasnacl/toadml/issues">Report bug or request a feature</a>
    ·
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>


## About the project

(NOTE: As of now, this section only describes the aim of the project. It does not imply that this stuff is implemented already! Please refer to the <a href="#roadmap">Roadmap</a>.)

There is a plethora of programming languages available today and more and more arise.
Out of all these languages, few employ formal methods as means of verification, both in the implementation of the compiler of that language as well as within the language itself.
ToadML is one of the few candidates whose compiler is formally verified.

Here's why ToadML is interesting:

* Secure: Programs in ToadML that satisfy a property also satisfy it after compilation. 
* Quantitative types for safe resource management.
* Multi-language paradigm to sidestep the [fire-triangle](https://dl.acm.org/doi/10.1145/3371126).
* Algebraic effects.
* Bootstrapped and verified.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

Right now, ToadML is built with both [Ocaml](https://ocaml.org) and [Coq](https://coq.inria.fr/).

- [coq-record-update](https://github.com/tchajed/coq-record-update-plugin)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Roadmap

The following shows a very rough roadmap of what is planned.

- [ ] Runtime-layer done.
  - [x] Minimal driver code to perform file i/o, then switching to Coq.
  - [x] Verified parser.
  - [x] Implement a secure, verified compiler for very simple runtime language.
  - [ ] Make target language of runtime-layer more ASM like (likely targeting ARM Morello/CHERI).
  - [ ] Enhance source-language with first-class functions.
- [ ] Add verification-layer.
  - [ ] Verify a declarative version of the typechecking rules for a variant of quantitative type theory.
  - [ ] Implement a typechecker and prove soundness.
- [ ] Bootstrap

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contributing

This project is open to contributions.
Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please first create an issue with the tag `enhancement` and describe the what and how.
Then, if your proposal is accepted and you want to implement it, fork the repo and create a pull request.
Thanks again!

1. Create an issue with `enhancement` tag.
2. Wait for approval.
3. Fork the project.
4. Create your feature branch (`git checkout -b feature/MyFeature`).
5. Commit your changes (`git commit -m 'Implement MyFeature'`).
6. Push to the branch (`git push origin feature/MyFeature`).
7. Open a pull request.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## License

Distributed under the Apache 2.0 License.
See `LICENSE-2.0.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Talk to me on discord: `@dasnacl`
Project Link: [https://codeberg.org/dasnacl/toadml](https://codeberg.org/dasnacl/toadml)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Acknowledgments

* [![](https://dcbadge.vercel.app/api/server/4Kjt3ZE)](https://discord.gg/4Kjt3ZE)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

